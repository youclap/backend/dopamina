import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// TODO improve this
// check https://www.lordcodes.com/posts/gradle-dependencies-using-kotlin
val logback_version: String by project
val ktor_version: String by project
val kotlin_version: String by project
val detekt_version: String by project
val koin_version: String by project
val klap_version: String by project

plugins {
    application
    kotlin("jvm") version "1.3.50"
    id("com.github.johnrengelman.shadow") version "5.1.0"

    id("io.gitlab.arturbosch.detekt") version "1.0.0-RC16"
}

group = "tech.youclap"
version = "0.0.1-SNAPSHOT"

application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

tasks.withType<ShadowJar> {
    archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
    mergeServiceFiles()
}

detekt {
    parallel = true
    toolVersion = detekt_version
    input = files("src/main/kotlin")
    config = files("$rootDir/detekt.yml")
    buildUponDefaultConfig = true
}


dependencies {

    // Core
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")

    implementation("com.github.youclap.klap:core:$klap_version")

    // Ktor
    implementation("io.ktor:ktor-server-netty:$ktor_version") // TODO maybe test CIO
    implementation("com.github.youclap.klap:ktor:$klap_version")
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-jackson:$ktor_version")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-joda:2.10.0.pr1")

    // TODO maybe could be used https://github.com/Kotlin/KEEP/blob/master/proposals/stdlib/result.md instead but there
    //  is no support to be used as a return type
    implementation("com.github.kittinunf.result:result:2.2.0")

    // Database
    implementation("com.github.youclap.klap:exposed:$klap_version")
    implementation("org.jetbrains.exposed:exposed:0.17.3")
    implementation("mysql:mysql-connector-java:5.1.48")
    implementation("com.zaxxer:HikariCP:3.3.1")

    // Logging
    implementation("ch.qos.logback:logback-classic:$logback_version")

    // Dependency injection
    implementation("org.koin:koin-ktor:$koin_version")
    implementation("org.koin:koin-logger-slf4j:$koin_version")

    // PubSub
    implementation("com.google.cloud:google-cloud-pubsub:1.98.0")

    // Code style
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:$detekt_version")

    // Tests
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://kotlin.bintray.com/ktor") }
    maven { url = uri("https://plugins.gradle.org/m2/") }
    maven { url = uri("http://kotlin.bintray.com/kotlinx") }
    maven { url = uri("https://jitpack.io") }
}


// TODO try to improve this
val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-XXLanguage:+InlineClasses")
}

val compileTestKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions.jvmTarget = "1.8"
}
