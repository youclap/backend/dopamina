FROM adoptopenjdk/openjdk8:alpine-slim

ENV PORT=80

EXPOSE $PORT
VOLUME /tmp

COPY service-account.json /
COPY build/libs/dopamina.jar /app.jar

ENV GOOGLE_APPLICATION_CREDENTIALS='/service-account.json'

ENTRYPOINT java -server -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:InitialRAMFraction=2 -XX:MinRAMFraction=2 -XX:MaxRAMFraction=2 -XX:+UseG1GC -XX:MaxGCPauseMillis=100 -XX:+UseStringDeduplication -jar /app.jar
