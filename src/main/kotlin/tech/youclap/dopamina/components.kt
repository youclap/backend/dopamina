package tech.youclap.dopamina

import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.jackson.JacksonConverter
import org.koin.Logger.slf4jLogger
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import tech.youclap.dopamina.service.AmericaService
import tech.youclap.dopamina.service.AmericaServiceImpl
import tech.youclap.dopamina.service.AuthorizationService
import tech.youclap.dopamina.service.AuthorizationServiceImpl
import tech.youclap.dopamina.service.ChallengeService
import tech.youclap.dopamina.service.ChallengeServiceImpl
import tech.youclap.dopamina.service.ClapService
import tech.youclap.dopamina.service.ClapServiceImpl
import tech.youclap.dopamina.service.CommentService
import tech.youclap.dopamina.service.CommentServiceImpl
import tech.youclap.dopamina.service.FeedService
import tech.youclap.dopamina.service.FeedServiceImpl
import tech.youclap.dopamina.service.MexicoService
import tech.youclap.dopamina.service.MexicoServiceImpl
import tech.youclap.dopamina.service.PostService
import tech.youclap.dopamina.service.PostServiceImpl
import tech.youclap.dopamina.shared.extension.teatroURL
import tech.youclap.dopamina.store.AmericaStore
import tech.youclap.dopamina.store.AuthorizationStore
import tech.youclap.dopamina.store.ChallengeStore
import tech.youclap.dopamina.store.ClapStore
import tech.youclap.dopamina.store.CommentStore
import tech.youclap.dopamina.store.MexicoStore
import tech.youclap.dopamina.store.MySQLAmericaStore
import tech.youclap.dopamina.store.MySQLAuthorizationStore
import tech.youclap.dopamina.store.MySQLChallengeStore
import tech.youclap.dopamina.store.MySQLClapStore
import tech.youclap.dopamina.store.MySQLCommentStore
import tech.youclap.dopamina.store.MySQLMexicoStore
import tech.youclap.dopamina.store.MySQLPostStore
import tech.youclap.dopamina.store.PostStore
import tech.youclap.dopamina.store.PubSubStore
import tech.youclap.dopamina.store.PubSubStoreImpl

fun Application.installKoin() = install(Koin) {

    slf4jLogger()

    val configuration = ServiceConfiguration(teatroURL)

    val dependenciesModule = module {

        single { configuration }

        single { jacksonObjectMapper() }

        single<AuthorizationService> { AuthorizationServiceImpl(get()) }
        single<AuthorizationStore> { MySQLAuthorizationStore() }

        single<ChallengeService> { ChallengeServiceImpl(get(), get(), get(), get()) }
        single<ChallengeStore> { MySQLChallengeStore() }

        single<PostService> { PostServiceImpl(get(), get(), get(), get(), get()) }
        single<PostStore> { MySQLPostStore() }

        single<ClapService> { ClapServiceImpl(get(), get(), get(), get(), get()) }
        single<ClapStore> { MySQLClapStore() }

        single<CommentService> { CommentServiceImpl(get(), get(), get(), get()) }
        single<CommentStore> { MySQLCommentStore() }

        single<FeedService> { FeedServiceImpl(get(), get(), get()) }

        single<MexicoService> { MexicoServiceImpl(get()) }
        single<MexicoStore> { MySQLMexicoStore() }

        single<AmericaService> { AmericaServiceImpl(get(), get()) }
        single<AmericaStore> { MySQLAmericaStore() }
        single<PubSubStore> { PubSubStoreImpl(get()) }
    }

    modules(dependenciesModule)
}

fun ContentNegotiation.Configuration.jacksonInjected(
    contentType: ContentType = ContentType.Application.Json,
    mapper: ObjectMapper,
    block: ObjectMapper.() -> Unit = {}
) {
    mapper.apply {
        setDefaultPrettyPrinter(DefaultPrettyPrinter().apply {
            indentArraysWith(DefaultPrettyPrinter.FixedSpaceIndenter.instance)
            indentObjectsWith(DefaultIndenter("  ", "\n"))
        })
    }
    mapper.apply(block)
    val converter = JacksonConverter(mapper)
    register(contentType, converter)
}
