package tech.youclap.dopamina

import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.Application
import org.jetbrains.exposed.sql.Database
import tech.youclap.klap.exposed.hikariConfig
import tech.youclap.klap.ktor.extension.dbHost
import tech.youclap.klap.ktor.extension.dbName
import tech.youclap.klap.ktor.extension.dbPassword
import tech.youclap.klap.ktor.extension.dbPort
import tech.youclap.klap.ktor.extension.dbUsername

fun Application.setupDatabase() {

    val config = hikariConfig(dbHost, dbPort, dbName, dbUsername, dbPassword)

    Database.connect(HikariDataSource(config))
}
