package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.store.ChallengeStoreReadResult

interface ChallengeStore {

    suspend fun createChallenge(challenge: Challenge, privacy: Privacy): Result<ChallengeID, Error>
    suspend fun readChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID
    ): Result<ChallengeStoreReadResult, Error>

    suspend fun deleteChallenge(challengeID: ChallengeID): Result<Unit, Error>

    suspend fun readChallenges(
        authenticatedProfileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<List<ChallengeStoreReadResult>, Error>

    suspend fun readChallengesForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID,
        sameProfile: Boolean
    ): Result<List<ChallengeStoreReadResult>, Error>

    suspend fun readChallengesForGroup(
        groupID: GroupID,
        authenticatedProfileID: ProfileID?
    ): Result<List<ChallengeStoreReadResult>, Error>

    suspend fun readChallengeByFirebaseID(firebaseID: String): Result<ChallengeStoreReadResult, Error>

    suspend fun hideChallenge(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        challengeID: ChallengeID
    ): Result<Unit, Error>

    suspend fun unhideChallenge(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<Unit, Error>

    suspend fun isChallengeCreator(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<Boolean, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class ChallengeNotFound(val challengeID: ChallengeID) : Error()
        data class ChallengePrivateGroupsInconsistent(val challengeID: ChallengeID) : Error()
        data class ChallengeNotFoundByFirebaseID(val firebaseID: String) : Error()
        data class GroupNotFound(val groupID: GroupID) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
