package tech.youclap.dopamina.store

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.kittinunf.result.Result
import com.google.api.core.ApiFutureCallback
import com.google.api.core.ApiFutures
import com.google.cloud.ServiceOptions
import com.google.cloud.pubsub.v1.Publisher
import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectTopicName
import com.google.pubsub.v1.PubsubMessage
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.america.AmericaMessage

interface PubSubStore {

    suspend fun sendMessage(message: AmericaMessage): Result<Unit, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}

class PubSubStoreImpl(
    private val objectMapper: ObjectMapper
) : PubSubStore {

    private companion object {
        private val logger = LoggerFactory.getLogger(PubSubStoreImpl::class.java)
    }

    private val publisher: Publisher

    init {
        val projectId = ServiceOptions.getDefaultProjectId()
        logger.info("ℹ️ projectID '$projectId'")

        val topicName = ProjectTopicName.of(projectId, "america")

        publisher = Publisher.newBuilder(topicName).build()
    }

    @SuppressWarnings("TooGenericExceptionCaught")
    override suspend fun sendMessage(message: AmericaMessage): Result<Unit, PubSubStore.Error> {

        logger.info("ℹ️ Sending message '$message'")

        return try {
            val jsonMessage = objectMapper.writeValueAsString(message)

            val data = ByteString.copyFromUtf8(jsonMessage)
            val pubsubMessage = PubsubMessage.newBuilder()
                .setData(data)
                .build()

            val messageFuture = publisher.publish(pubsubMessage)
            ApiFutures.addCallback(messageFuture, object : ApiFutureCallback<String> {
                override fun onSuccess(result: String) {
                    logger.info("ℹ️ sending message '$result'")
                }

                override fun onFailure(throwable: Throwable) {
                    logger.error("❌ error sending message callback '$message'", throwable)
                }
            })

            Result.success(Unit)
        } catch (e: Exception) {

            logger.error("❌ error sending message '$message'", e)

            Result.error(PubSubStore.Error.Unknown(e))
        }
    }
}
