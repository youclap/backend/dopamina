package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.JoinType.INNER
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.andWhere
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.ClapType
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.shared.db.notInSubQuery
import tech.youclap.dopamina.shared.extension.value
import tech.youclap.dopamina.store.ClapStore.Error
import tech.youclap.dopamina.store.extention.save
import tech.youclap.dopamina.store.extention.update
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.ClapTable
import tech.youclap.dopamina.store.table.PostTable

class MySQLClapStore : ClapStore {

    override suspend fun createClap(clap: Clap): Result<Unit, Error> {
        return dbQuery {

            clap.save()

            PostTable.update({ PostTable.id eq clap.postID.value }) {
                with(SqlExpressionBuilder) {
                    it.update(claps, claps + clap.type.value)
                }
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun updateClap(clap: Clap, currentClap: ClapType): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = clap.update()

            if (affectedRows == 0) {
                throw Error.ClapNotFound(clap.postID, clap.profileID)
            }

            PostTable.update({ PostTable.id eq clap.postID.value }) {
                with(SqlExpressionBuilder) {
                    it.update(claps, claps - currentClap.value + clap.type.value)
                }
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readClap(authenticatedProfileID: ProfileID, postID: PostID): Result<Clap, Error> {
        return dbQuery {
            selectClap(authenticatedProfileID, postID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun deleteClap(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error> {
        return dbQuery {

            val currentClap = selectClap(authenticatedProfileID, postID)

            val affectedRows = ClapTable.deleteWhere {
                ClapTable.postID eq postID.value and
                    (ClapTable.profileID eq authenticatedProfileID.value)
            }

            // TODO try to improve this any other way :/
            PostTable.update({ PostTable.id eq postID.value }) {
                with(SqlExpressionBuilder) {
                    it.update(claps, claps - currentClap.type.value)
                }
            }

            // TODO not sure if it makes sense?
            //  It is a bit redundant since`selectClap` throws if the clap doesn't exists
            if (affectedRows == 0) {
                throw Error.ClapNotFound(postID, authenticatedProfileID)
            }
            Unit
        }
            .mapError {
                // TODO maybe try to join this with [read]
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readClapsForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<List<Clap>, Error> {
        return dbQuery {
            selectClapsForPost(authenticatedProfileID, postID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun count(postID: PostID): Result<Pair<Int, Int>, Error> {
        return dbQuery {

            val singleCount = ClapTable
                .select {
                    ClapTable.postID eq postID.value and
                        (ClapTable.type eq ClapType.SINGLE)
                }
                .count()

            val doubleCount = ClapTable
                .select {
                    ClapTable.postID eq postID.value and
                        (ClapTable.type eq ClapType.DOUBLE)
                }
                .count()

            (singleCount to doubleCount)
        }.mapError {
            when (it) {
                is Error -> it
                else -> Error.Unknown(it)
            }
        }
    }

    private fun selectClap(authenticatedProfileID: ProfileID, postID: PostID): Clap {

        return ChallengeTable.join(
            otherTable = PostTable,
            joinType = INNER,
            additionalConstraint = { ChallengeTable.id eq PostTable.challengeID }
        )
            .join(
                otherTable = ClapTable,
                joinType = INNER,
                additionalConstraint = { PostTable.id eq ClapTable.postID }
            )
            .slice(ClapTable.columns)
            .select {
                ChallengeTable.deleted eq false and
                    (PostTable.deleted eq false) and
                    (ClapTable.postID eq postID.value) and
                    (ClapTable.profileID eq authenticatedProfileID.value)
            }
            .firstOrNull()
            ?.toClap()
            ?: throw Error.ClapNotFound(postID, authenticatedProfileID)
    }

    private fun selectClapsForPost(authenticatedProfileID: ProfileID?, postID: PostID): List<Clap> {

        val query = ChallengeTable
            .join(
                otherTable = PostTable,
                joinType = INNER
            )
            .join(
                otherTable = ClapTable,
                joinType = INNER,
                additionalConstraint = { PostTable.id eq ClapTable.postID }
            )
            .slice(ClapTable.columns)
            .select {
                ChallengeTable.deleted eq false and
                    (PostTable.deleted eq false) and
                    (ClapTable.postID eq postID.value)
            }

        authenticatedProfileID?.let {
            query
                .andWhere {
                    ClapTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ClapTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ClapTable.postID notInSubQuery hiddenPosts(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.challengeID notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .map {
                it.toClap()
            }
    }

    private fun ResultRow.toClap(): Clap {
        return Clap(
            postID = this[ClapTable.postID].toPostID(),
            profileID = this[ClapTable.profileID].toProfileID(),
            userID = this[ClapTable.userID].toUserID(),
            type = this[ClapTable.type],
            createdDate = this[ClapTable.createdDate]
        )
    }
}
