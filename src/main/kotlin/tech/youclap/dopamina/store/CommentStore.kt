package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

interface CommentStore {

    suspend fun createComment(comment: Comment): Result<CommentID, Error>
    suspend fun readComment(authenticatedProfileID: ProfileID?, commentID: CommentID): Result<Comment, Error>
    suspend fun deleteComment(commentID: CommentID): Result<Unit, Error>

    suspend fun readCommentsForPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<List<Comment>, Error>
    suspend fun count(postID: PostID): Result<Int, Error>

    suspend fun hideComment(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        commentID: CommentID
    ): Result<Unit, Error>

    suspend fun unhideComment(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Unit, Error>

    suspend fun isCommentCreator(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Boolean, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class CommentNotFound(val commentID: CommentID) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
