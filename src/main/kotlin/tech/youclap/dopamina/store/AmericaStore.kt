package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.select
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.CommentPostTable
import tech.youclap.dopamina.store.table.CommentTable
import tech.youclap.dopamina.store.table.GroupTable
import tech.youclap.dopamina.store.table.PostTable
import tech.youclap.dopamina.store.table.ProfileFirebaseTable

interface AmericaStore {

    suspend fun readFirebaseID(profileID: ProfileID): Result<String, Error>
    suspend fun readFirebaseID(groupID: GroupID): Result<String, Error>
    suspend fun readFirebaseID(challengeID: ChallengeID): Result<String, Error>
    suspend fun readFirebaseID(postID: PostID): Result<String, Error>
    suspend fun readFirebaseID(commentID: CommentID): Result<String, Error>

    suspend fun readPostID(commentID: CommentID): Result<PostID, Error>
    suspend fun readChallengeID(postID: PostID): Result<ChallengeID, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class ProfileNotFound(val profileID: ProfileID) : Error()
        data class GroupNotFound(val groupID: GroupID) : Error()
        data class ChallengeNotFound(val challengeID: ChallengeID) : Error()
        data class PostNotFound(val postID: PostID) : Error()
        data class CommentNotFound(val commentID: CommentID) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}

class MySQLAmericaStore : AmericaStore {

    override suspend fun readFirebaseID(profileID: ProfileID): Result<String, AmericaStore.Error> {
        return dbQuery {
            ProfileFirebaseTable
                .slice(ProfileFirebaseTable.firebaseID)
                .select {
                    ProfileFirebaseTable.profileID eq profileID.value
                }
                .firstOrNull()
                ?.let {
                    it[ProfileFirebaseTable.firebaseID]
                }
                ?: throw AmericaStore.Error.ProfileNotFound(profileID)
        }
            .mapError(mapError)
    }

    override suspend fun readFirebaseID(groupID: GroupID): Result<String, AmericaStore.Error> {
        return dbQuery {
            GroupTable
                .slice(GroupTable.firebaseID)
                .select {
                    GroupTable.id eq groupID.value
                }
                .firstOrNull()
                ?.let {
                    it[GroupTable.firebaseID]
                }
                ?: throw AmericaStore.Error.GroupNotFound(groupID)
        }
            .mapError(mapError)
    }

    override suspend fun readFirebaseID(challengeID: ChallengeID): Result<String, AmericaStore.Error> {
        return dbQuery {
            ChallengeTable
                .slice(ChallengeTable.firebaseID)
                .select {
                    ChallengeTable.id eq challengeID.value
                }
                .firstOrNull()
                ?.let {
                    it[ChallengeTable.firebaseID]
                }
                ?: throw AmericaStore.Error.ChallengeNotFound(challengeID)
        }
            .mapError(mapError)
    }

    override suspend fun readFirebaseID(postID: PostID): Result<String, AmericaStore.Error> {
        return dbQuery {
            PostTable
                .slice(PostTable.firebaseID)
                .select {
                    PostTable.id eq postID.value
                }
                .firstOrNull()
                ?.let {
                    it[PostTable.firebaseID]
                }
                ?: throw AmericaStore.Error.PostNotFound(postID)
        }
            .mapError(mapError)
    }

    override suspend fun readFirebaseID(commentID: CommentID): Result<String, AmericaStore.Error> {
        return dbQuery {
            CommentTable
                .slice(CommentTable.firebaseID)
                .select {
                    CommentTable.id eq commentID.value
                }
                .firstOrNull()
                ?.let {
                    it[CommentTable.firebaseID]
                }
                ?: throw AmericaStore.Error.CommentNotFound(commentID)
        }
            .mapError(mapError)
    }

    override suspend fun readPostID(commentID: CommentID): Result<PostID, AmericaStore.Error> {
        return dbQuery {
            CommentPostTable
                .slice(CommentPostTable.postID)
                .select {
                    CommentPostTable.commentID eq commentID.value
                }
                .firstOrNull()
                ?.let {
                    it[CommentPostTable.postID].toPostID()
                }
                ?: throw AmericaStore.Error.CommentNotFound(commentID)
        }
            .mapError(mapError)
    }

    override suspend fun readChallengeID(postID: PostID): Result<ChallengeID, AmericaStore.Error> {
        return dbQuery {
            PostTable
                .slice(PostTable.challengeID)
                .select {
                    PostTable.id eq postID.value
                }
                .firstOrNull()
                ?.let {
                    it[PostTable.challengeID].toChallengeID()
                }
                ?: throw AmericaStore.Error.PostNotFound(postID)
        }
            .mapError(mapError)
    }

    private val mapError: (Exception) -> AmericaStore.Error = {
        when (it) {
            is AmericaStore.Error -> it
            else -> AmericaStore.Error.Unknown(it)
        }
    }
}
