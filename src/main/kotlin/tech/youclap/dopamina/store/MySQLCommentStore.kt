@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.Join
import org.jetbrains.exposed.sql.JoinType.INNER
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SortOrder.DESC
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.andWhere
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.extension.toCommentID
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.shared.db.notInSubQuery
import tech.youclap.dopamina.store.CommentStore.Error
import tech.youclap.dopamina.store.extention.save
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.CommentPostTable
import tech.youclap.dopamina.store.table.CommentTable
import tech.youclap.dopamina.store.table.HiddenComment
import tech.youclap.dopamina.store.table.PostTable

class MySQLCommentStore : CommentStore {

    override suspend fun createComment(comment: Comment): Result<CommentID, Error> {
        return dbQuery {

            val commentID = comment.save()

            commentID.value.toCommentID()
        }
            .mapError {
                // TODO maybe check for exception here org.jetbrains.exposed.exceptions
                Error.Unknown(it)
            }
    }

    override suspend fun readComment(authenticatedProfileID: ProfileID?, commentID: CommentID): Result<Comment, Error> {
        return dbQuery {

            val comment = selectComment(authenticatedProfileID, commentID)

            comment
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun deleteComment(commentID: CommentID): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = CommentTable.update({
                CommentTable.id eq commentID.value and
                    (CommentTable.deleted eq false)
            }) {
                it[deleted] = true
            }

            if (affectedRows == 0) {
                throw Error.CommentNotFound(commentID)
            }

            Unit
        }
            .mapError {
                // TODO maybe try to join this with [read]
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readCommentsForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<List<Comment>, Error> {
        return dbQuery {
            selectCommentsForPost(authenticatedProfileID, postID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun count(postID: PostID): Result<Int, Error> {
        return dbQuery {

            commentsJoinQuery()
                .slice(PostTable.id)
                .select {
                    ChallengeTable.deleted eq false and
                        (PostTable.deleted eq false) and
                        (CommentPostTable.postID eq postID.value) and
                        (CommentTable.deleted eq false)
                }
                .count()
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun hideComment(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        commentID: CommentID
    ): Result<Unit, Error> {

        return dbQuery {
            HiddenComment.insert {
                it[HiddenComment.profileID] = authenticatedProfileID.value
                it[HiddenComment.userID] = authenticatedUserID.value
                it[HiddenComment.commentID] = commentID.value
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun unhideComment(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = HiddenComment.deleteWhere {
                HiddenComment.profileID eq authenticatedProfileID.value and
                    (HiddenComment.commentID eq commentID.value)
            }

            if (affectedRows == 0) {
                throw Error.CommentNotFound(commentID)
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun isCommentCreator(
        authenticatedProfileID: ProfileID,
        commentID: CommentID
    ): Result<Boolean, Error> {

        return dbQuery {
            CommentTable
                .slice(CommentTable.profileID)
                .select {
                    CommentTable.id eq commentID.value and
                        (CommentTable.profileID eq authenticatedProfileID.value)
                }
                .firstOrNull()
                ?.let {
                    it[CommentTable.profileID] == authenticatedProfileID.value
                }
                ?: throw Error.CommentNotFound(commentID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    private fun selectComment(authenticatedProfileID: ProfileID?, commentID: CommentID): Comment {

        val query = commentsJoinQuery()
            .slice(CommentTable.columns + CommentPostTable.postID)
            .select {
                ChallengeTable.deleted eq false and
                    (PostTable.deleted eq false) and
                    (CommentTable.id eq commentID.value) and
                    (CommentTable.deleted eq false)
            }

        authenticatedProfileID?.let {
            query
                .andWhere {
                    CommentTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    CommentTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    CommentTable.id notInSubQuery hiddenComments(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.id notInSubQuery hiddenPosts(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.id notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .firstOrNull()
            ?.toComment()
            ?: throw Error.CommentNotFound(commentID)
    }

    private fun selectCommentsForPost(authenticatedProfileID: ProfileID?, postID: PostID): List<Comment> {

        val query = commentsJoinQuery()
            .slice(CommentTable.columns + CommentPostTable.postID)
            .select {
                ChallengeTable.deleted eq false and
                    (PostTable.deleted eq false) and
                    (CommentPostTable.postID eq postID.value) and
                    (CommentTable.deleted eq false)
            }

        authenticatedProfileID?.let {
            query
                .andWhere {
                    CommentTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    CommentTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    CommentTable.id notInSubQuery hiddenComments(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.id notInSubQuery hiddenPosts(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.id notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .orderBy(CommentTable.createdDate, DESC)
            .map { it.toComment() }
    }

    private fun ResultRow.toComment(): Comment {

        return Comment(
            id = this[CommentTable.id].value,
            postID = this[CommentPostTable.postID].toPostID(),
            profileID = this[CommentTable.profileID].toProfileID(),
            userID = this[CommentTable.userID].toUserID(),
            message = this[CommentTable.message],
            createdDate = this[CommentTable.createdDate],
            deleted = this[CommentTable.deleted],
            firebaseID = this[CommentTable.firebaseID]
        )
    }

    private fun commentsJoinQuery(): Join {
        return ChallengeTable.join(
            otherTable = PostTable,
            joinType = INNER,
            additionalConstraint = { ChallengeTable.id eq PostTable.challengeID }
        )
            .join(
                otherTable = CommentPostTable,
                joinType = INNER,
                additionalConstraint = { PostTable.id eq CommentPostTable.postID }
            )
            .join(
                otherTable = CommentTable,
                joinType = INNER,
                additionalConstraint = { CommentPostTable.commentID eq CommentTable.id }
            )
    }
}
