package tech.youclap.dopamina.store.extention

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.insertAndGetId
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.store.table.ChallengeTable

fun Challenge.save(): EntityID<Long> {

    val challenge = this // TODO improve this 💩

    return ChallengeTable.insertAndGetId {
        it[userID] = challenge.userID.value
        it[profileID] = challenge.profileID.value
        it[privacyType] = challenge.privacy
        it[mediaType] = challenge.mediaType
        challenge.createdDate?.run { it[createdDate] = this }
        it[beginDate] = challenge.beginDate
        it[endDate] = challenge.endDate
        it[title] = challenge.title
        it[description] = challenge.description
        it[allowImage] = challenge.allowImage
        it[allowVideo] = challenge.allowVideo
        it[allowText] = challenge.allowText
        challenge.deleted?.run { it[deleted] = this }
        it[firebaseID] = challenge.firebaseID
    }
}
