package tech.youclap.dopamina.store.extention

import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.store.table.ClapTable

fun Clap.save() {

    val clap = this

    ClapTable.insert {
        it[postID] = clap.postID.value
        it[profileID] = clap.profileID.value
        it[userID] = clap.userID.value
        it[type] = clap.type
        clap.createdDate?.run { it[createdDate] = this }
    }
}

fun Clap.update(): Int {

    val clap = this

    return ClapTable.update({
        ClapTable.profileID eq clap.profileID.value and
            (ClapTable.postID eq clap.postID.value)
    }) {
        it[type] = clap.type
        it[userID] = clap.userID.value
        it[createdDate] = DateTime.now()
    }
}
