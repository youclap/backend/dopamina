package tech.youclap.dopamina.store.extention

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.store.table.PostTable
import tech.youclap.dopamina.store.table.PostTextTable

fun Post.save(): EntityID<Long> {

    val post = this

    val newPostID = PostTable.insertAndGetId {
        it[profileID] = post.profileID.value
        it[userID] = post.userID.value
        it[challengeID] = post.challengeID.value
        it[type] = post.type
        post.createdDate?.run { it[createdDate] = this }
        post.deleted?.run { it[deleted] = this }
        post.firebaseID.run { it[firebaseID] = this }
    }

    if (post is Post.Text) {
        PostTextTable.insert {
            it[postID] = newPostID.value
            it[startColor] = post.startColor
            it[endColor] = post.endColor
            it[message] = post.message
        }
    }

    return newPostID
}
