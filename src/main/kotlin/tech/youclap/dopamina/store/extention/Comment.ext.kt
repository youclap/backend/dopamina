package tech.youclap.dopamina.store.extention

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.store.table.CommentPostTable
import tech.youclap.dopamina.store.table.CommentTable

fun Comment.save(): EntityID<Long> {

    val comment = this

    val commentID = CommentTable.insertAndGetId {
        it[profileID] = comment.profileID.value
        it[userID] = comment.userID.value
        it[message] = comment.message
        comment.createdDate?.run { it[createdDate] = this }
        comment.deleted?.run { it[deleted] = this }
        comment.firebaseID.run { it[firebaseID] = this }
    }

    CommentPostTable.insert {
        it[this.commentID] = commentID.value
        it[this.postID] = comment.postID.value
    }

    return commentID
}
