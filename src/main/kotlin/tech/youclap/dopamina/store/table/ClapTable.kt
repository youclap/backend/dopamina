package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table
import tech.youclap.dopamina.model.ClapType

object ClapTable : Table("Clap") {

    val postID = long("postID").primaryKey(0)

    val profileID = long("profileID").primaryKey(1)

    val userID = long("userID")

    val type = enumeration("typeID", ClapType::class)

    val createdDate = datetime("createdDate")
}
