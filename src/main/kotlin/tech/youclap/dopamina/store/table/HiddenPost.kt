package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object HiddenPost : Table("HiddenPost") {

    val profileID = long("profileID")
    val userID = long("userID")
    val postID = long("postID")
    val createdDate = datetime("createdDate")
}
