package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object CommentPostTable : Table("CommentPost") {

    val commentID = long("commentID").primaryKey(0)

    val postID = long("postID").primaryKey(1)
}
