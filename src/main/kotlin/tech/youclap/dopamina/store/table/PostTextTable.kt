package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object PostTextTable : Table("PostText") {
    val postID = long("postID").references(PostTable.id).primaryKey(0)
    val startColor = integer("startColor")
    val endColor = integer("endColor")
    val message = text("message")
}
