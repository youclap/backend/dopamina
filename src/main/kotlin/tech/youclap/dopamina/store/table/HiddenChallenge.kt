package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object HiddenChallenge : Table("HiddenChallenge") {

    val profileID = long("profileID")
    val userID = long("userID")
    val challengeID = long("challengeID")
    val createdDate = datetime("createdDate")
}
