package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.dao.LongIdTable
import tech.youclap.dopamina.model.ChallengeMediaType
import tech.youclap.dopamina.model.PrivacyType

object ChallengeTable : LongIdTable("Challenge") {
    val userID = long("userID")
    val profileID = long("profileID")
    val privacyType = enumeration("privacyID", PrivacyType::class)
    val mediaType = enumeration("mediaTypeID", ChallengeMediaType::class)
    val createdDate = datetime("createdDate")
    val beginDate = datetime("beginDate")
    val endDate = datetime("endDate").nullable()
    val title = varchar("title", TITLE_COLUMN_SIZE)
    val description = text("description").nullable()
    val allowImage = bool("allowImage")
    val allowVideo = bool("allowVideo")
    val allowText = bool("allowText")
    val deleted = bool("deleted")
    val firebaseID = varchar("firebaseID", FIREBASEID_COLUMN_SIZE)
}
