package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object ChallengePrivateForGroupsTable : Table("ChallengePrivateForGroups") {

    val challengeID = long("challengeID").entityId().references(ChallengeTable.id).primaryKey()

    val groupID = long("groupID").references(GroupTable.id)
}
