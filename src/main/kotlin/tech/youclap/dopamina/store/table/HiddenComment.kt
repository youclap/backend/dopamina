package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object HiddenComment : Table("HiddenComment") {

    val profileID = long("profileID")
    val userID = long("userID")
    val commentID = long("commentID")
    val createdDate = datetime("createdDate")
}
