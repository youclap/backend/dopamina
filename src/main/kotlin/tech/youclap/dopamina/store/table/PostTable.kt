package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.dao.LongIdTable
import tech.youclap.dopamina.model.PostType

object PostTable : LongIdTable("Post") {
    val profileID = long("profileID").references(ProfileTable.id)
    val userID = long("userID")
    val challengeID = long("challengeID").references(ChallengeTable.id)
    val type = enumeration("typeID", PostType::class)
    val createdDate = datetime("createdDate")
    val deleted = bool("deleted")
    val firebaseID = varchar("firebaseID", FIREBASEID_COLUMN_SIZE)
    val claps = integer("claps")
}
