package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object BlockedProfile : Table("BlockedProfile") {

    val profileID = long("profileID")

    val blockedProfileID = long("blockedProfileID")
}
