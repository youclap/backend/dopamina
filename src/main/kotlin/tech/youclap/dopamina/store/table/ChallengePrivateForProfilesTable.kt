package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object ChallengePrivateForProfilesTable : Table("ChallengePrivateForProfiles") {

    val challengeID = long("challengeID").references(ChallengeTable.id).primaryKey(0)

    val profileID = long("profileID").references(ProfileTable.id).primaryKey(1)
}
