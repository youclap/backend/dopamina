package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.dao.LongIdTable

// To be used by Mexico
object UserTable : LongIdTable("User")
