package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.dao.LongIdTable

object GroupTable : LongIdTable("Group") {
    val deleted = bool("deleted")
    val firebaseID = varchar("firebaseID", FIREBASEID_COLUMN_SIZE)
}
