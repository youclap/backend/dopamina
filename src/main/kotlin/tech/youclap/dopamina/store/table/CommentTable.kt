package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.dao.LongIdTable

object CommentTable : LongIdTable("Comment") {
    val profileID = long("profileID")
    val userID = long("userID")
    val message = text("message")
    val createdDate = datetime("createdDate")
    val deleted = bool("deleted")
    val firebaseID = varchar("firebaseID", FIREBASEID_COLUMN_SIZE)
}
