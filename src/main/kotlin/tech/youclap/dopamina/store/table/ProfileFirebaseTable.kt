package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

// To be used by Mexico
object ProfileFirebaseTable : Table("FirebaseIdentifier") {
    val profileID = long("profileID").references(ProfileTable.id)
    val firebaseID = varchar("firebaseID", FIREBASEID_COLUMN_SIZE)
}
