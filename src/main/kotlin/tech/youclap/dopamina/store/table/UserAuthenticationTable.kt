package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

// To be used by Mexico
object UserAuthenticationTable : Table("AuthenticationIdentifier") {
    val userID = long("userID").references(UserTable.id)
    val authenticationID = varchar("authenticationID", FIREBASEID_COLUMN_SIZE)
}
