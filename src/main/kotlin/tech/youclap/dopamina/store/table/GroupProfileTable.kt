package tech.youclap.dopamina.store.table

import org.jetbrains.exposed.sql.Table

object GroupProfileTable : Table("Group_Profile") {
    val groupID = long("groupID").references(GroupTable.id)
    val profileID = long("profileID").references(ProfileTable.id)
    val deleted = bool("deleted")
}
