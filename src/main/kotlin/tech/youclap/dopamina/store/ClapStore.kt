package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.ClapType
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID

interface ClapStore {

    suspend fun createClap(clap: Clap): Result<Unit, Error>
    suspend fun updateClap(clap: Clap, currentClap: ClapType): Result<Unit, Error>
    suspend fun readClap(authenticatedProfileID: ProfileID, postID: PostID): Result<Clap, Error>
    suspend fun deleteClap(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error>

    suspend fun readClapsForPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<List<Clap>, Error>
    suspend fun count(postID: PostID): Result<Pair<Int, Int>, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class ClapNotFound(val postID: PostID, val profileID: ProfileID) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
