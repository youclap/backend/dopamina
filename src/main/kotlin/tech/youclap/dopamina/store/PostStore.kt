@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.PostSort
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

interface PostStore {

    suspend fun createPost(post: Post): Result<PostID, Error>
    suspend fun readPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<Post, Error>
    suspend fun deletePost(postID: PostID): Result<Unit, Error>

    suspend fun readPostsForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID,
        sameProfile: Boolean
    ): Result<List<Post>, Error>

    suspend fun readPostsForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID,
        sort: PostSort
    ): Result<List<Post>, Error>

    suspend fun readPostCountForChallenge(challengeID: ChallengeID): Result<Int, Error>

    suspend fun readPostByFirebaseID(firebasaseID: String): Result<Post, Error>

    suspend fun readUserPostForChallenge(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Post, Error>

    suspend fun hidePost(authenticatedProfileID: ProfileID, userID: UserID, postID: PostID): Result<Unit, Error>
    suspend fun unhidePost(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error>

    suspend fun alreadyPosted(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<Boolean, Error>
    suspend fun isPostCreator(authenticatedProfileID: ProfileID, postID: PostID): Result<Boolean, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class PostNotFound(val postID: PostID) : Error()
        data class PostNotFoundByFirebaseID(val firebasaseID: String) : Error()
        data class UserPostNotFoundForChallenge(val challengeID: ChallengeID) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
