@file:SuppressWarnings("LongMethod")

package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.JoinType.INNER
import org.jetbrains.exposed.sql.JoinType.LEFT
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import tech.youclap.dopamina.model.PrivacyType
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.store.table.ChallengePrivateForGroupsTable
import tech.youclap.dopamina.store.table.ChallengePrivateForProfilesTable
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.CommentPostTable
import tech.youclap.dopamina.store.table.CommentTable
import tech.youclap.dopamina.store.table.GroupProfileTable
import tech.youclap.dopamina.store.table.GroupTable
import tech.youclap.dopamina.store.table.PostTable

class MySQLAuthorizationStore : AuthorizationStore {

    override suspend fun canAccessChallenges(
        profileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<Map<ChallengeID, Boolean>, AuthorizationStore.Error> {

        // TODO improve this to have blocked stuff

        return dbQuery {
            if (profileID == null) {
                ChallengeTable
                    .slice(ChallengeTable.id, ChallengeTable.privacyType)
            } else {
                ChallengeTable
                    .join(
                        otherTable = ChallengePrivateForProfilesTable,
                        joinType = LEFT,
                        additionalConstraint = {
                            ChallengePrivateForProfilesTable.challengeID eq ChallengeTable.id and
                                (ChallengePrivateForProfilesTable.profileID eq profileID.value)
                        }
                    )
                    .join(
                        otherTable = ChallengePrivateForGroupsTable,
                        joinType = LEFT,
                        additionalConstraint = { ChallengePrivateForGroupsTable.challengeID eq ChallengeTable.id }
                    )
                    .join(
                        otherTable = GroupTable,
                        joinType = LEFT,
                        additionalConstraint = { GroupTable.id eq ChallengePrivateForGroupsTable.groupID }
                        // TODO not deleted?
                    )
                    .join(
                        otherTable = GroupProfileTable,
                        joinType = LEFT,
                        additionalConstraint = {
                            GroupProfileTable.groupID eq GroupTable.id and
                                (GroupProfileTable.profileID eq profileID.value)
                        }
                    )
                    .slice(
                        ChallengeTable.id,
                        ChallengeTable.privacyType,
                        GroupProfileTable.profileID,
                        ChallengePrivateForProfilesTable.profileID
                    )
            }
                .select {
                    ChallengeTable.id inList challengeIDs.map { it.value } and
                        (ChallengeTable.deleted eq false)
                }
                .map {

                    val challengeID = it[ChallengeTable.id].value.toChallengeID()

                    val canAccess = when (it[ChallengeTable.privacyType]) {

                        PrivacyType.PUBLIC -> true

                        PrivacyType.PRIVATE_PROFILES -> it.getOrNull(ChallengePrivateForProfilesTable.profileID) != null

                        PrivacyType.PRIVATE_GROUPS -> it.getOrNull(GroupProfileTable.profileID) != null
                    }

                    challengeID to canAccess
                }
                .toMap()
        }
            .mapError {
                when (it) {
                    is AuthorizationStore.Error -> it
                    else -> AuthorizationStore.Error.Unknown(it)
                }
            }
    }

    override suspend fun canAccessPost(
        profileID: ProfileID?,
        postID: PostID
    ): Result<Boolean, AuthorizationStore.Error> {

        return dbQuery {
            if (profileID == null) {
                ChallengeTable
                    .join(
                        otherTable = PostTable,
                        joinType = INNER
                    )
                    .slice(ChallengeTable.privacyType)
            } else {
                ChallengeTable
                    .join(
                        otherTable = PostTable,
                        joinType = INNER
                    )
                    .join(
                        otherTable = ChallengePrivateForProfilesTable,
                        joinType = LEFT,
                        additionalConstraint = {
                            ChallengePrivateForProfilesTable.challengeID eq ChallengeTable.id and
                                (ChallengePrivateForProfilesTable.profileID eq profileID.value)
                        }
                    )
                    .join(
                        otherTable = ChallengePrivateForGroupsTable,
                        joinType = LEFT,
                        additionalConstraint = { ChallengePrivateForGroupsTable.challengeID eq ChallengeTable.id }
                    )
                    .join(
                        otherTable = GroupTable,
                        joinType = LEFT,
                        additionalConstraint = { GroupTable.id eq ChallengePrivateForGroupsTable.groupID }
                        // TODO not deleted?
                    )
                    .join(
                        otherTable = GroupProfileTable,
                        joinType = LEFT,
                        additionalConstraint = {
                            GroupProfileTable.groupID eq GroupTable.id and
                                (GroupProfileTable.profileID eq profileID.value)
                        }
                    )
                    .slice(
                        ChallengeTable.privacyType,
                        GroupProfileTable.profileID,
                        ChallengePrivateForProfilesTable.profileID
                    )
            }
                .select {
                    PostTable.id eq postID.value and
                        (ChallengeTable.deleted eq false)
                }
                .firstOrNull()
                ?.let {
                    when (it[ChallengeTable.privacyType]) {

                        PrivacyType.PUBLIC -> true

                        PrivacyType.PRIVATE_PROFILES -> it.getOrNull(ChallengePrivateForProfilesTable.profileID) != null

                        PrivacyType.PRIVATE_GROUPS -> it.getOrNull(GroupProfileTable.profileID) != null
                    }
                }
                ?: throw AuthorizationStore.Error.PostNotFound(postID)
        }
            .mapError {
                when (it) {
                    is AuthorizationStore.Error -> it
                    else -> AuthorizationStore.Error.Unknown(it)
                }
            }
    }

    override suspend fun canAccessComment(
        profileID: ProfileID?,
        commentID: CommentID
    ): Result<Boolean, AuthorizationStore.Error> {
        return dbQuery {
            if (profileID == null) {
                ChallengeTable
                    .join(
                        otherTable = PostTable,
                        joinType = INNER
                    )
                    .join(
                        otherTable = CommentTable,
                        joinType = INNER
                    )
                    .slice(ChallengeTable.privacyType)
            } else {
                ChallengeTable
                    .join(
                        otherTable = PostTable,
                        joinType = INNER
                    )
                    .join(otherTable = CommentPostTable,
                        joinType = INNER,
                        additionalConstraint = {
                            CommentPostTable.postID eq PostTable.id
                        }
                    )
                    .join(
                        otherTable = CommentTable,
                        joinType = INNER,
                        additionalConstraint = {
                            CommentTable.id eq CommentPostTable.commentID
                        }
                    )
                    .join(
                        otherTable = ChallengePrivateForProfilesTable,
                        joinType = LEFT,
                        additionalConstraint = {
                            ChallengePrivateForProfilesTable.challengeID eq ChallengeTable.id and
                                (ChallengePrivateForProfilesTable.profileID eq profileID.value)
                        }
                    )
                    .join(
                        otherTable = ChallengePrivateForGroupsTable,
                        joinType = LEFT,
                        additionalConstraint = { ChallengePrivateForGroupsTable.challengeID eq ChallengeTable.id }
                    )
                    .join(
                        otherTable = GroupTable,
                        joinType = LEFT,
                        additionalConstraint = { GroupTable.id eq ChallengePrivateForGroupsTable.groupID }
                        // TODO not deleted?
                    )
                    .join(
                        otherTable = GroupProfileTable,
                        joinType = LEFT,
                        additionalConstraint = {
                            GroupProfileTable.groupID eq GroupTable.id and
                                (GroupProfileTable.profileID eq profileID.value)
                        }
                    )
                    .slice(
                        ChallengeTable.privacyType,
                        GroupProfileTable.profileID,
                        ChallengePrivateForProfilesTable.profileID
                    )
            }
                .select {
                    CommentTable.id eq commentID.value and
                        (ChallengeTable.deleted eq false)
                }
                .firstOrNull()
                ?.let {
                    when (it[ChallengeTable.privacyType]) {

                        PrivacyType.PUBLIC -> true

                        PrivacyType.PRIVATE_PROFILES -> it.getOrNull(ChallengePrivateForProfilesTable.profileID) != null

                        PrivacyType.PRIVATE_GROUPS -> it.getOrNull(GroupProfileTable.profileID) != null
                    }
                }
                ?: throw AuthorizationStore.Error.CommentNotFound(commentID)
        }
            .mapError {
                when (it) {
                    is AuthorizationStore.Error -> it
                    else -> AuthorizationStore.Error.Unknown(it)
                }
            }
    }

    override suspend fun canAccessGroup(
        profileID: ProfileID,
        groupID: GroupID
    ): Result<Boolean, AuthorizationStore.Error> {
        return dbQuery {
            GroupProfileTable
                .select {
                    GroupProfileTable.groupID eq groupID.value and
                        (GroupProfileTable.profileID eq profileID.value)
                }
                .firstOrNull() != null
        }
            .mapError {
                when (it) {
                    is AuthorizationStore.Error -> it
                    else -> AuthorizationStore.Error.Unknown(it)
                }
            }
    }
}
