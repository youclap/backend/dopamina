@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.JoinType.INNER
import org.jetbrains.exposed.sql.JoinType.LEFT
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SortOrder.ASC
import org.jetbrains.exposed.sql.SortOrder.DESC
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.andWhere
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.PostSort
import tech.youclap.dopamina.model.PostSort.NEWEST
import tech.youclap.dopamina.model.PostSort.OLDEST
import tech.youclap.dopamina.model.PostSort.RANDOM
import tech.youclap.dopamina.model.PostSort.TOP
import tech.youclap.dopamina.model.PostType
import tech.youclap.dopamina.model.PrivacyType
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.shared.db.notInSubQuery
import tech.youclap.dopamina.store.PostStore.Error
import tech.youclap.dopamina.store.extention.save
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.HiddenPost
import tech.youclap.dopamina.store.table.PostTable
import tech.youclap.dopamina.store.table.PostTextTable

class MySQLPostStore : PostStore {

    override suspend fun createPost(post: Post): Result<PostID, Error> {
        return dbQuery {

            val postID = post.save()

            postID.value.toPostID()
        }
            .mapError {
                // TODO maybe check for exception here org.jetbrains.exposed.exceptions
                Error.Unknown(it)
            }
    }

    override suspend fun readPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<Post, Error> {
        return dbQuery {

            val post = selectPost(authenticatedProfileID, postID)

            post
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun deletePost(postID: PostID): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = PostTable.update({
                PostTable.id eq postID.value and
                    (PostTable.deleted eq false)
            }) {
                it[deleted] = true
            }

            if (affectedRows == 0) {
                throw Error.PostNotFound(postID)
            }

            Unit
        }
            .mapError {
                // TODO maybe try to join this with [read]
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readPostsForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID,
        sameProfile: Boolean
    ): Result<List<Post>, Error> {
        return dbQuery {
            selectPostsForProfile(authenticatedProfileID, profileID, sameProfile)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readPostsForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID,
        sort: PostSort
    ): Result<List<Post>, Error> {
        return dbQuery {
            selectPostsForChallenge(authenticatedProfileID, challengeID, sort)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readPostCountForChallenge(challengeID: ChallengeID): Result<Int, Error> {
        return dbQuery {
            selectPostCountForChallenge(challengeID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readPostByFirebaseID(firebasaseID: String): Result<Post, Error> {
        return dbQuery {
            selectPostByFirebaseID(firebasaseID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readUserPostForChallenge(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Post, Error> {
        return dbQuery {
            selectUserPostForChallenge(authenticatedProfileID, challengeID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun hidePost(
        authenticatedProfileID: ProfileID,
        userID: UserID,
        postID: PostID
    ): Result<Unit, Error> {

        return dbQuery {
            HiddenPost.insert {
                it[HiddenPost.profileID] = authenticatedProfileID.value
                it[HiddenPost.userID] = userID.value
                it[HiddenPost.postID] = postID.value
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun unhidePost(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = HiddenPost.deleteWhere {
                HiddenPost.profileID eq authenticatedProfileID.value and
                    (HiddenPost.postID eq postID.value)
            }

            if (affectedRows == 0) {
                throw Error.PostNotFound(postID)
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun alreadyPosted(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Boolean, Error> {
        return dbQuery {
            PostTable
                .select {
                    PostTable.challengeID eq challengeID.value and
                        (PostTable.profileID eq authenticatedProfileID.value) and
                        (PostTable.deleted eq false)
                }
                .firstOrNull() != null
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun isPostCreator(authenticatedProfileID: ProfileID, postID: PostID): Result<Boolean, Error> {
        return dbQuery {
            PostTable
                .slice(PostTable.profileID)
                .select {
                    PostTable.id eq postID.value and
                        (PostTable.profileID eq authenticatedProfileID.value)
                }
                .firstOrNull()
                ?.let {
                    it[PostTable.profileID] == authenticatedProfileID.value
                }
                ?: throw Error.PostNotFound(postID)
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    private fun selectPost(authenticatedProfileID: ProfileID?, postID: PostID): Post {

        val query = PostTable
            .join(
                otherTable = PostTextTable,
                joinType = LEFT
            )
            .join(
                otherTable = ChallengeTable,
                joinType = INNER,
                additionalConstraint = { PostTable.challengeID eq ChallengeTable.id }
            )
            .slice(PostTable.columns + PostTextTable.columns + ChallengeTable.firebaseID)
            .select {
                PostTable.id eq postID.value and
                    (PostTable.deleted eq false) and
                    (ChallengeTable.deleted eq false)
            }

        authenticatedProfileID?.let {
            query
                .andWhere {
                    PostTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.id notInSubQuery hiddenPosts(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.challengeID notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .firstOrNull()
            ?.toPost()
            ?: throw Error.PostNotFound(postID)
    }

    private fun selectPostsForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID,
        sameProfile: Boolean
    ): List<Post> {

        val query = PostTable
            .join(
                otherTable = PostTextTable,
                joinType = LEFT,
                additionalConstraint = { PostTable.id eq PostTextTable.postID })
            .join(
                otherTable = ChallengeTable,
                joinType = INNER,
                additionalConstraint = { PostTable.challengeID eq ChallengeTable.id }
            )
            .slice(PostTable.columns + PostTextTable.columns + ChallengeTable.firebaseID)
            .select {
                PostTable.profileID eq profileID.value and
                    (PostTable.deleted eq false) and
                    (ChallengeTable.deleted eq false)
            }
            .orderBy(PostTable.createdDate, DESC)

        if (authenticatedProfileID != null && !sameProfile) {
            query
                .andWhere {
                    // TODO improve this with correct proivacy, groups and private users
                    ChallengeTable.privacyType eq PrivacyType.PUBLIC
                }
                .andWhere {
                    PostTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.id notInSubQuery hiddenPosts(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.challengeID notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query.map {
            it.toPost()
        }
    }

    private fun selectPostsForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID,
        sort: PostSort
    ): List<Post> {

        val query = PostTable.join(
            otherTable = PostTextTable,
            joinType = LEFT,
            additionalConstraint = { PostTable.id eq PostTextTable.postID }
        )
            .join(
                otherTable = ChallengeTable,
                joinType = INNER,
                additionalConstraint = { PostTable.challengeID eq ChallengeTable.id }
            )
            .slice(PostTable.columns + PostTextTable.columns + ChallengeTable.firebaseID)
            .select {
                PostTable.challengeID eq challengeID.value and
                    (PostTable.deleted eq false) and
                    (ChallengeTable.deleted eq false)
            }

        authenticatedProfileID?.let {
            query
                .andWhere {
                    PostTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.id notInSubQuery hiddenPosts(authenticatedProfileID)
                }
                .andWhere {
                    PostTable.challengeID notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        when (sort) {
            RANDOM ->
                query.orderBy(PostTable.firebaseID) // TODO change this

            TOP ->
                query.orderBy(PostTable.claps, DESC)

            NEWEST ->
                query.orderBy(PostTable.createdDate, DESC)

            OLDEST ->
                query.orderBy(PostTable.createdDate, ASC)
        }

        return query.map {
            it.toPost()
        }
    }

    private fun selectPostCountForChallenge(challengeID: ChallengeID): Int {

        return PostTable.join(
            otherTable = ChallengeTable,
            joinType = INNER,
            additionalConstraint = { PostTable.challengeID eq ChallengeTable.id }
        )
            .slice(PostTable.id)
            .select {
                PostTable.challengeID eq challengeID.value and
                    (PostTable.deleted eq false) and
                    (ChallengeTable.deleted eq false)
            }
            .count()
    }

    private fun selectPostByFirebaseID(firebasaseID: String): Post {
        return PostTable.join(
            otherTable = PostTextTable,
            joinType = LEFT,
            additionalConstraint = { PostTable.id eq PostTextTable.postID }
        )
            .join(
                otherTable = ChallengeTable,
                joinType = INNER,
                additionalConstraint = { PostTable.challengeID eq ChallengeTable.id }
            )
            .slice(PostTable.columns + PostTextTable.columns + ChallengeTable.firebaseID)
            .select {
                PostTable.firebaseID eq firebasaseID and
                    (PostTable.deleted eq false) and
                    (ChallengeTable.deleted eq false)
            }
            .firstOrNull()
            ?.toPost()
            ?: throw Error.PostNotFoundByFirebaseID(firebasaseID)
    }

    private fun selectUserPostForChallenge(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Post {

        val query = PostTable
            .join(
                otherTable = PostTextTable,
                joinType = LEFT
            )
            .join(
                otherTable = ChallengeTable,
                joinType = INNER,
                additionalConstraint = { PostTable.challengeID eq ChallengeTable.id }
            )
            .slice(PostTable.columns + PostTextTable.columns + ChallengeTable.firebaseID)
            .select {
                PostTable.challengeID eq challengeID.value and
                    (PostTable.profileID eq authenticatedProfileID.value) and
                    (PostTable.deleted eq false) and
                    (ChallengeTable.deleted eq false)
            }

        return query
            .firstOrNull()
            ?.toPost()
            ?: throw Error.UserPostNotFoundForChallenge(challengeID)
    }

    private fun ResultRow.toPost(): Post {
        return when (this[PostTable.type]) {
            PostType.IMAGE ->
                Post.Image(
                    id = this[PostTable.id].value.toPostID(),
                    challengeID = this[PostTable.challengeID].toChallengeID(),
                    profileID = this[PostTable.profileID].toProfileID(),
                    userID = this[PostTable.userID].toUserID(),
                    createdDate = this[PostTable.createdDate],
                    deleted = this[PostTable.deleted],
                    firebaseID = this[PostTable.firebaseID],
                    challengeFirebaseID = this[ChallengeTable.firebaseID],
                    claps = this[PostTable.claps]
                )
            PostType.VIDEO ->
                Post.Video(
                    id = this[PostTable.id].value.toPostID(),
                    challengeID = this[PostTable.challengeID].toChallengeID(),
                    profileID = this[PostTable.profileID].toProfileID(),
                    userID = this[PostTable.userID].toUserID(),
                    createdDate = this[PostTable.createdDate],
                    deleted = this[PostTable.deleted],
                    firebaseID = this[PostTable.firebaseID],
                    challengeFirebaseID = this[ChallengeTable.firebaseID],
                    claps = this[PostTable.claps]
                )
            PostType.TEXT ->
                Post.Text(
                    id = this[PostTable.id].value.toPostID(),
                    challengeID = this[PostTable.challengeID].toChallengeID(),
                    profileID = this[PostTable.profileID].toProfileID(),
                    userID = this[PostTable.userID].toUserID(),
                    createdDate = this[PostTable.createdDate],
                    deleted = this[PostTable.deleted],
                    firebaseID = this[PostTable.firebaseID],
                    claps = this[PostTable.claps],
                    startColor = this[PostTextTable.startColor],
                    endColor = this[PostTextTable.endColor],
                    message = this[PostTextTable.message],
                    challengeFirebaseID = this[ChallengeTable.firebaseID]
                )
        }
    }
}
