package tech.youclap.dopamina.store

import org.jetbrains.exposed.sql.Query
import org.jetbrains.exposed.sql.select
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.store.table.BlockedProfile
import tech.youclap.dopamina.store.table.HiddenChallenge
import tech.youclap.dopamina.store.table.HiddenComment
import tech.youclap.dopamina.store.table.HiddenPost

// TODO try to improve this with union https://github.com/JetBrains/Exposed/issues/402
fun blockedByAuthenticatedProfiles(profileID: ProfileID): Query {
    return BlockedProfile
        .slice(BlockedProfile.blockedProfileID)
        .select {
            BlockedProfile.profileID eq profileID.value
        }
}

fun blockedAuthenticatedProfiles(profileID: ProfileID): Query {
    return BlockedProfile
        .slice(BlockedProfile.profileID)
        .select {
            BlockedProfile.blockedProfileID eq profileID.value
        }
}

fun hiddenChallenges(profileID: ProfileID): Query {
    return HiddenChallenge
        .slice(HiddenChallenge.challengeID)
        .select {
            HiddenChallenge.profileID eq profileID.value
        }
}

fun hiddenPosts(profileID: ProfileID): Query {
    return HiddenPost
        .slice(HiddenPost.postID)
        .select {
            HiddenPost.profileID eq profileID.value
        }
}

fun hiddenComments(profileID: ProfileID): Query {
    return HiddenComment
        .slice(HiddenComment.commentID)
        .select {
            HiddenComment.profileID eq profileID.value
        }
}
