@file:SuppressWarnings("TooManyFunctions", "TooGenericExceptionThrown")

package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toCommentID
import tech.youclap.dopamina.model.extension.toGroupID
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.shared.extension.value
import tech.youclap.dopamina.store.extention.save
import tech.youclap.dopamina.store.extention.update
import tech.youclap.dopamina.store.table.ChallengePrivateForGroupsTable
import tech.youclap.dopamina.store.table.ChallengePrivateForProfilesTable
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.ClapTable
import tech.youclap.dopamina.store.table.CommentTable
import tech.youclap.dopamina.store.table.GroupTable
import tech.youclap.dopamina.store.table.HiddenChallenge
import tech.youclap.dopamina.store.table.HiddenComment
import tech.youclap.dopamina.store.table.HiddenPost
import tech.youclap.dopamina.store.table.PostTable
import tech.youclap.dopamina.store.table.ProfileFirebaseTable
import tech.youclap.dopamina.store.table.UserAuthenticationTable

interface MexicoStore {

    suspend fun readUserID(firebaseUserID: String): Result<UserID, Error>
    suspend fun readProfileID(firebaseUserID: String): Result<ProfileID, Error>
    suspend fun readGroupID(firebaseGroupID: String): Result<GroupID, Error>
    suspend fun readChallengeID(firebaseChallengeID: String): Result<ChallengeID, Error>
    suspend fun readPostID(firebasePostID: String): Result<PostID, Error>
    suspend fun readCommentID(firebaseCommentID: String): Result<CommentID, Error>

    suspend fun createChallenge(challenge: Challenge, privacy: Privacy): Result<Long, Error>
    suspend fun deleteChallenge(challengeID: ChallengeID): Result<Unit, Error>
    suspend fun createBlockedChallenge(
        challengeID: ChallengeID,
        profileID: ProfileID,
        userID: UserID,
        timestamp: DateTime
    ): Result<Unit, Error>

    suspend fun deleteBlockedChallenge(challengeID: ChallengeID, profileID: ProfileID): Result<Unit, Error>

    suspend fun createPost(post: Post): Result<PostID, Error>
    suspend fun deletePost(postID: PostID): Result<Unit, Error>
    suspend fun createBlockedPost(
        postID: PostID,
        profileID: ProfileID,
        userID: UserID,
        timestamp: DateTime
    ): Result<Unit, Error>

    suspend fun deleteBlockedPost(postID: PostID, profileID: ProfileID): Result<Unit, Error>

    suspend fun createClap(clap: Clap): Result<Unit, Error>
    suspend fun updateClap(clap: Clap): Result<Unit, Error>
    suspend fun deleteClap(postID: PostID, profileID: ProfileID): Result<Unit, Error>

    suspend fun createComment(comment: Comment): Result<Unit, Error>
    suspend fun deleteComment(commentID: CommentID): Result<Unit, Error>
    suspend fun createBlockedComment(
        commentID: CommentID,
        profileID: ProfileID,
        userID: UserID,
        timestamp: DateTime
    ): Result<Unit, Error>

    suspend fun deleteBlockedComment(commentID: CommentID, profileID: ProfileID): Result<Unit, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class FirebaseUserNotFound(val firebaseUserID: String) : Error()
        data class FirebaseProfileNotFound(val firebaseProfileID: String) : Error()
        data class FirebaseGroupNotFound(val firebaseGroupID: String) : Error()
        data class FirebaseChallengeNotFound(val firebaseChallengeID: String) : Error()
        data class FirebasePostNotFound(val firebasePostID: String) : Error()
        data class FirebaseCommentNotFound(val firebaseCommentID: String) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}

class MySQLMexicoStore : MexicoStore {

    override suspend fun readUserID(firebaseUserID: String): Result<UserID, MexicoStore.Error> {
        return dbQuery {
            UserAuthenticationTable
                .slice(UserAuthenticationTable.userID)
                .select {
                    UserAuthenticationTable.authenticationID eq firebaseUserID
                }
                .firstOrNull()
                ?.let {
                    it[UserAuthenticationTable.userID].toUserID()
                }
                ?: throw MexicoStore.Error.FirebaseUserNotFound(firebaseUserID)
        }
            .mapError(mapError)
    }

    override suspend fun readProfileID(firebaseUserID: String): Result<ProfileID, MexicoStore.Error> {
        return dbQuery {
            ProfileFirebaseTable
                .slice(ProfileFirebaseTable.profileID)
                .select {
                    ProfileFirebaseTable.firebaseID eq firebaseUserID
                }
                .firstOrNull()
                ?.let {
                    it[ProfileFirebaseTable.profileID].toProfileID()
                }
                ?: throw MexicoStore.Error.FirebaseProfileNotFound(firebaseUserID)
        }
            .mapError(mapError)
    }

    override suspend fun readGroupID(firebaseGroupID: String): Result<GroupID, MexicoStore.Error> {

        return dbQuery {
            GroupTable
                .slice(GroupTable.id)
                .select {
                    GroupTable.firebaseID eq firebaseGroupID
                }
                .firstOrNull()
                ?.let {
                    it[GroupTable.id].value.toGroupID()
                }
                ?: throw MexicoStore.Error.FirebaseGroupNotFound(firebaseGroupID)
        }

            .mapError(mapError)
    }

    override suspend fun readChallengeID(firebaseChallengeID: String): Result<ChallengeID, MexicoStore.Error> {
        return dbQuery {
            ChallengeTable
                .slice(ChallengeTable.id)
                .select {
                    ChallengeTable.firebaseID eq firebaseChallengeID
                }
                .firstOrNull()
                ?.let {
                    it[ChallengeTable.id].value.toChallengeID()
                }
                ?: throw MexicoStore.Error.FirebaseChallengeNotFound(firebaseChallengeID)
        }

            .mapError(mapError)
    }

    override suspend fun readPostID(firebasePostID: String): Result<PostID, MexicoStore.Error> {
        return dbQuery {
            PostTable
                .slice(PostTable.id)
                .select {
                    PostTable.firebaseID eq firebasePostID
                }
                .firstOrNull()
                ?.let {
                    it[PostTable.id].value.toPostID()
                }
                ?: throw MexicoStore.Error.FirebasePostNotFound(firebasePostID)
        }

            .mapError(mapError)
    }

    override suspend fun readCommentID(firebaseCommentID: String): Result<CommentID, MexicoStore.Error> {
        return dbQuery {
            CommentTable
                .slice(CommentTable.id)
                .select {
                    CommentTable.firebaseID eq firebaseCommentID
                }
                .firstOrNull()
                ?.let {
                    it[CommentTable.id].value.toCommentID()
                }
                ?: throw MexicoStore.Error.FirebaseCommentNotFound(firebaseCommentID)
        }

            .mapError(mapError)
    }

    override suspend fun createChallenge(challenge: Challenge, privacy: Privacy): Result<Long, MexicoStore.Error> {
        return dbQuery {

            val challengeID = challenge.save()

            when (privacy) {
                is Privacy.PrivateProfiles ->
                    privacy.profileIDs.forEach { privacyProfileID ->
                        ChallengePrivateForProfilesTable.insert {
                            it[this.challengeID] = challengeID.value
                            it[this.profileID] = privacyProfileID.value
                        }
                    }

                is Privacy.PrivateGroups ->
                    ChallengePrivateForGroupsTable.insert {
                        it[this.challengeID] = challengeID
                        it[this.groupID] = privacy.groupID.value
                    }
            }

            challengeID.value
        }
            .mapError(mapError)
    }

    override suspend fun deleteChallenge(challengeID: ChallengeID): Result<Unit, MexicoStore.Error> {
        return dbQuery {
            val affectedRows = ChallengeTable.update({
                ChallengeTable.id eq challengeID.value
            }) {
                it[deleted] = true
            }

            if (affectedRows == 0) {
                throw Error("ChallengeNotFound $challengeID")
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun createBlockedChallenge(
        challengeID: ChallengeID,
        profileID: ProfileID,
        userID: UserID,
        timestamp: DateTime
    ): Result<Unit, MexicoStore.Error> {

        return dbQuery {
            HiddenChallenge.insert {
                it[HiddenChallenge.profileID] = profileID.value
                it[HiddenChallenge.userID] = userID.value
                it[HiddenChallenge.challengeID] = challengeID.value
                it[HiddenChallenge.createdDate] = timestamp
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteBlockedChallenge(
        challengeID: ChallengeID,
        profileID: ProfileID
    ): Result<Unit, MexicoStore.Error> {
        return dbQuery {
            HiddenChallenge.deleteWhere {
                HiddenChallenge.challengeID eq challengeID.value and
                    (HiddenChallenge.profileID eq profileID.value)
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun createPost(post: Post): Result<PostID, MexicoStore.Error> {
        return dbQuery {

            val postID = post.save()

            postID.value.toPostID()
        }
            .mapError(mapError)
    }

    override suspend fun deletePost(postID: PostID): Result<Unit, MexicoStore.Error> {
        return dbQuery {
            val affectedRows = PostTable.update({
                PostTable.id eq postID.value
            }) {
                it[deleted] = true
            }

            if (affectedRows == 0) {
                throw Error("PostNotFound $postID")
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun createBlockedPost(
        postID: PostID,
        profileID: ProfileID,
        userID: UserID,
        timestamp: DateTime
    ): Result<Unit, MexicoStore.Error> {

        return dbQuery {
            HiddenPost.insert {
                it[HiddenPost.profileID] = profileID.value
                it[HiddenPost.userID] = userID.value
                it[HiddenPost.postID] = postID.value
                it[HiddenPost.createdDate] = timestamp
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteBlockedPost(postID: PostID, profileID: ProfileID): Result<Unit, MexicoStore.Error> {
        return dbQuery {
            HiddenPost.deleteWhere {
                HiddenPost.postID eq postID.value and
                    (HiddenPost.profileID eq profileID.value)
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun createClap(clap: Clap): Result<Unit, MexicoStore.Error> {
        return dbQuery {

            // TODO try to improve this any other way :/
            PostTable.update({ PostTable.id eq clap.postID.value }) {
                with(SqlExpressionBuilder) {
                    it.update(claps, claps + clap.type.value)
                }
            }

            clap.save()

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun updateClap(clap: Clap): Result<Unit, MexicoStore.Error> {
        return dbQuery {

            val currentClap = ClapTable
                .select {
                    (ClapTable.postID eq clap.postID.value) and
                        (ClapTable.profileID eq clap.profileID.value)
                }
                .firstOrNull()
                ?.toClap()

            if (currentClap == null) {
                // TODO try to improve this any other way :/
                PostTable.update({ PostTable.id eq clap.postID.value }) {
                    with(SqlExpressionBuilder) {
                        it.update(claps, claps + clap.type.value)
                    }
                }

                clap.save()

                return@dbQuery
            }

            // TODO try to improve this any other way :/
            PostTable.update({ PostTable.id eq clap.postID.value }) {
                with(SqlExpressionBuilder) {
                    it.update(claps, claps - currentClap.type.value + clap.type.value)
                }
            }

            clap.update()

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteClap(postID: PostID, profileID: ProfileID): Result<Unit, MexicoStore.Error> {
        return dbQuery {

            val affectedRows = ClapTable.deleteWhere {
                ClapTable.postID eq postID.value and
                    (ClapTable.profileID eq profileID.value)
            }

            if (affectedRows == 0) {
                throw Error("ClapNotFound $postID $profileID")
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun createComment(comment: Comment): Result<Unit, MexicoStore.Error> {
        return dbQuery {

            comment.save()

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteComment(commentID: CommentID): Result<Unit, MexicoStore.Error> {
        return dbQuery {
            val affectedRows = CommentTable.update({
                CommentTable.id eq commentID.value
            }) {
                it[deleted] = true
            }

            if (affectedRows == 0) {
                throw Error("CommentNotFound $commentID")
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun createBlockedComment(
        commentID: CommentID,
        profileID: ProfileID,
        userID: UserID,
        timestamp: DateTime
    ): Result<Unit, MexicoStore.Error> {

        return dbQuery {
            HiddenComment.insert {
                it[HiddenComment.profileID] = profileID.value
                it[HiddenComment.userID] = userID.value
                it[HiddenComment.commentID] = commentID.value
                it[HiddenComment.createdDate] = timestamp
            }

            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteBlockedComment(
        commentID: CommentID,
        profileID: ProfileID
    ): Result<Unit, MexicoStore.Error> {
        return dbQuery {
            HiddenComment.deleteWhere {
                HiddenComment.commentID eq commentID.value and
                    (HiddenComment.profileID eq profileID.value)
            }

            Unit
        }
            .mapError(mapError)
    }

    private val mapError: (Exception) -> MexicoStore.Error = {
        when (it) {
            is MexicoStore.Error -> it
            else -> MexicoStore.Error.Unknown(it)
        }
    }

    private fun ResultRow.toClap(): Clap {
        return Clap(
            postID = this[ClapTable.postID].toPostID(),
            profileID = this[ClapTable.profileID].toProfileID(),
            userID = this[ClapTable.userID].toUserID(),
            type = this[ClapTable.type],
            createdDate = this[ClapTable.createdDate]
        )
    }
}
