@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dopamina.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SortOrder.DESC
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.andWhere
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.PrivacyType
import tech.youclap.dopamina.model.PrivacyType.PRIVATE_GROUPS
import tech.youclap.dopamina.model.PrivacyType.PRIVATE_PROFILES
import tech.youclap.dopamina.model.PrivacyType.PUBLIC
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toGroupID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.store.ChallengeStoreReadResult
import tech.youclap.dopamina.shared.db.dbQuery
import tech.youclap.dopamina.shared.db.notInSubQuery
import tech.youclap.dopamina.store.ChallengeStore.Error
import tech.youclap.dopamina.store.extention.save
import tech.youclap.dopamina.store.table.ChallengePrivateForGroupsTable
import tech.youclap.dopamina.store.table.ChallengePrivateForProfilesTable
import tech.youclap.dopamina.store.table.ChallengeTable
import tech.youclap.dopamina.store.table.GroupTable
import tech.youclap.dopamina.store.table.HiddenChallenge

class MySQLChallengeStore : ChallengeStore {

    override suspend fun createChallenge(
        challenge: Challenge,
        privacy: Privacy
    ): Result<ChallengeID, Error> {
        return dbQuery {

            val challengeID = challenge.save()

            when (privacy) {
                is Privacy.PrivateProfiles ->
                    (privacy.profileIDs + challenge.profileID).forEach { privacyProfileID ->
                        ChallengePrivateForProfilesTable.insert {
                            it[this.challengeID] = challengeID.value
                            it[this.profileID] = privacyProfileID.value
                        }
                    }

                is Privacy.PrivateGroups ->
                    ChallengePrivateForGroupsTable.insert {
                        it[this.challengeID] = challengeID
                        it[this.groupID] = privacy.groupID.value
                    }
            }

            challengeID.value.toChallengeID()
        }
            .mapError {
                // TODO maybe check for exception here org.jetbrains.exposed.exceptions
                Error.Unknown(it)
            }
    }

    override suspend fun readChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID
    ): Result<ChallengeStoreReadResult, Error> {
        return dbQuery {

            val challenge = selectChallenge(authenticatedProfileID, challengeID)

            val privacy = selectPrivacy(challengeID, challenge.privacy)

            ChallengeStoreReadResult(
                challenge = challenge,
                privacy = privacy
            )
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun deleteChallenge(challengeID: ChallengeID): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = ChallengeTable.update({
                ChallengeTable.id eq challengeID.value and
                    (ChallengeTable.deleted eq false)
            }) {
                it[deleted] = true
            }

            if (affectedRows == 0) {
                throw Error.ChallengeNotFound(challengeID)
            }

            Unit
        }
            .mapError {
                // TODO maybe try to join this with [read]
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readChallenges(
        authenticatedProfileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<List<ChallengeStoreReadResult>, Error> {
        return dbQuery {
            val challenges = selectChallenges(authenticatedProfileID, challengeIDs)

            challengeIDs
                .mapNotNull { challengeID ->
                    // TODO improve this.  🔨for keeping order in query 🤮
                    challenges.firstOrNull { it.id == challengeID }
                }
                .map { challenge ->

                    val privacy = selectPrivacy(challenge.id!!, challenge.privacy)

                    ChallengeStoreReadResult(
                        challenge = challenge,
                        privacy = privacy
                    )
                }
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readChallengesForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID,
        sameProfile: Boolean
    ): Result<List<ChallengeStoreReadResult>, Error> {
        return dbQuery {
            val challenges = selectChallengesForProfile(authenticatedProfileID, profileID, sameProfile)

            challenges.map { challenge ->

                val privacy = selectPrivacy(challenge.id!!, challenge.privacy)

                ChallengeStoreReadResult(
                    challenge = challenge,
                    privacy = privacy
                )
            }
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readChallengesForGroup(
        groupID: GroupID,
        authenticatedProfileID: ProfileID?
    ): Result<List<ChallengeStoreReadResult>, Error> {
        return dbQuery {

            // check if group is not deleted
            GroupTable.select {
                GroupTable.id eq groupID.value and
                    (GroupTable.deleted eq false)
            }
                .firstOrNull() ?: throw Error.GroupNotFound(groupID)

            selectChallengesForGroup(groupID, authenticatedProfileID).map { challenge ->

                val privacy = selectPrivacy(challenge.id!!, challenge.privacy)

                ChallengeStoreReadResult(
                    challenge = challenge,
                    privacy = privacy
                )
            }
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun readChallengeByFirebaseID(firebaseID: String): Result<ChallengeStoreReadResult, Error> {
        return dbQuery {

            val challenge = selectChallengeByFirebaseID(firebaseID)

            val privacy = selectPrivacy(challenge.id!!, challenge.privacy)

            ChallengeStoreReadResult(
                challenge = challenge,
                privacy = privacy
            )
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun hideChallenge(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        challengeID: ChallengeID
    ): Result<Unit, Error> {

        return dbQuery {
            HiddenChallenge.insert {
                it[HiddenChallenge.profileID] = authenticatedProfileID.value
                it[HiddenChallenge.userID] = authenticatedUserID.value
                it[HiddenChallenge.challengeID] = challengeID.value
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun unhideChallenge(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Unit, Error> {
        return dbQuery {

            val affectedRows = HiddenChallenge.deleteWhere {
                HiddenChallenge.profileID eq authenticatedProfileID.value and
                    (HiddenChallenge.challengeID eq challengeID.value)
            }

            if (affectedRows == 0) {
                throw Error.ChallengeNotFound(challengeID)
            }

            Unit
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    override suspend fun isChallengeCreator(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Boolean, Error> {
        return dbQuery {
            val resultRow = ChallengeTable
                .slice(ChallengeTable.profileID)
                .select {
                    ChallengeTable.id eq challengeID.value and
                        (ChallengeTable.deleted eq false)
                }
                .firstOrNull() ?: throw Error.ChallengeNotFound(challengeID)

            resultRow[ChallengeTable.profileID] == authenticatedProfileID.value
        }
            .mapError {
                when (it) {
                    is Error -> it
                    else -> Error.Unknown(it)
                }
            }
    }

    private fun selectChallenge(authenticatedProfileID: ProfileID?, challengeID: ChallengeID): Challenge {

        val query = ChallengeTable
            .select {
                ChallengeTable.id eq challengeID.value and
                    (ChallengeTable.deleted eq false)
            }

        // Maybe this should be divided in another function for authenticated profiles?
        authenticatedProfileID?.let {
            query
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.id notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .firstOrNull()
            ?.toChallenge()
            ?: throw Error.ChallengeNotFound(challengeID)
    }

    private fun selectChallenges(authenticatedProfileID: ProfileID?, challengeIDs: List<ChallengeID>): List<Challenge> {

        val query = ChallengeTable
            .select {
                ChallengeTable.id inList challengeIDs.map { it.value } and
                    (ChallengeTable.deleted eq false)
            }

        // Maybe this should be divided in another function for authenticated profiles?
        authenticatedProfileID?.let {
            query
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.id notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .map {
                it.toChallenge()
            }
    }

    private fun selectChallengesForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID,
        sameProfile: Boolean
    ): List<Challenge> {

        val query = ChallengeTable
            .select {
                ChallengeTable.profileID eq profileID.value and
                    (ChallengeTable.deleted eq false)
            }

        if (authenticatedProfileID != null && !sameProfile) {
            query
                .andWhere {
                    ChallengeTable.privacyType eq PUBLIC
                }
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.id notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .orderBy(ChallengeTable.createdDate, DESC)
            .map {
                it.toChallenge()
            }
    }

    private fun selectChallengesForGroup(
        groupID: GroupID,
        authenticatedProfileID: ProfileID?
    ): List<Challenge> {

        val query = ChallengePrivateForGroupsTable
            .join(otherTable = ChallengeTable,
                joinType = JoinType.INNER,
                additionalConstraint = { ChallengeTable.id eq ChallengePrivateForGroupsTable.challengeID })
            .slice(ChallengeTable.columns)
            .select {
                ChallengePrivateForGroupsTable.groupID eq groupID.value and
                    (ChallengeTable.deleted eq false)
            }

        authenticatedProfileID?.let {
            query
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedByAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.profileID notInSubQuery blockedAuthenticatedProfiles(authenticatedProfileID)
                }
                .andWhere {
                    ChallengeTable.id notInSubQuery hiddenChallenges(authenticatedProfileID)
                }
        }

        return query
            .orderBy(ChallengeTable.createdDate, DESC)
            .map {
                it.toChallenge()
            }
    }

    private fun selectPrivacy(challengeID: ChallengeID, privacyType: PrivacyType): Privacy {
        return when (privacyType) {

            PUBLIC ->
                Privacy.Public

            PRIVATE_PROFILES ->
                ChallengePrivateForProfilesTable
                    .slice(ChallengePrivateForProfilesTable.profileID)
                    .select { ChallengePrivateForProfilesTable.challengeID eq challengeID.value }
                    .map {
                        it[ChallengePrivateForProfilesTable.profileID].toProfileID()
                    }
                    .toList()
                    .let {
                        // TODO should check if $it is > than 1?
                        Privacy.PrivateProfiles(it)
                    }

            PRIVATE_GROUPS ->
                ChallengePrivateForGroupsTable
                    .slice(ChallengePrivateForGroupsTable.groupID)
                    .select { ChallengePrivateForGroupsTable.challengeID eq challengeID.value }
                    .firstOrNull()
                    ?.let {
                        Privacy.PrivateGroups(it[ChallengePrivateForGroupsTable.groupID].toGroupID())
                    }
                    ?: throw Error.ChallengePrivateGroupsInconsistent(challengeID)
        }
    }

    private fun selectChallengeByFirebaseID(firebaseID: String): Challenge {
        return ChallengeTable
            .select {
                ChallengeTable.firebaseID eq firebaseID and
                    (ChallengeTable.deleted eq false)
            }
            .firstOrNull()
            ?.toChallenge()
            ?: throw Error.ChallengeNotFoundByFirebaseID(firebaseID)
    }

    private fun ResultRow.toChallenge(): Challenge {
        return Challenge(
            id = this[ChallengeTable.id].value.toChallengeID(),
            userID = this[ChallengeTable.userID].toUserID(),
            profileID = this[ChallengeTable.profileID].toProfileID(),
            privacy = this[ChallengeTable.privacyType],
            createdDate = this[ChallengeTable.createdDate],
            beginDate = this[ChallengeTable.beginDate],
            endDate = this[ChallengeTable.endDate],
            mediaType = this[ChallengeTable.mediaType],
            title = this[ChallengeTable.title],
            description = this[ChallengeTable.description],
            allowImage = this[ChallengeTable.allowImage],
            allowVideo = this[ChallengeTable.allowVideo],
            allowText = this[ChallengeTable.allowText],
            deleted = this[ChallengeTable.deleted],
            firebaseID = this[ChallengeTable.firebaseID]
        )
    }
}
