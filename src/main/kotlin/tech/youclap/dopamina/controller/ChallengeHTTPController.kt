package tech.youclap.dopamina.controller

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toGroupID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.service.ChallengeService
import tech.youclap.dopamina.shared.ErrorCode.AMERICA
import tech.youclap.dopamina.shared.ErrorCode.BADREQUEST
import tech.youclap.dopamina.shared.ErrorCode.CHALLENGE_INCONSISTENT
import tech.youclap.dopamina.shared.ErrorCode.CHALLENGE_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.FORBIDDEN
import tech.youclap.dopamina.shared.ErrorCode.UNKNOWN
import tech.youclap.dopamina.shared.PROFILEID_HEADER
import tech.youclap.dopamina.shared.USERID_HEADER
import tech.youclap.dopamina.shared.extension.authenticatedProfileID
import tech.youclap.dopamina.shared.extension.authenticatedUserID
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.model.HttpError

// TODO look into this https://github.com/soywiz/ktor-springer ?
class ChallengeHTTPController(
    private val challengeService: ChallengeService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(ChallengeHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {

        route("challenge") {
            route("{challengeID}") {
                get {
                    readChallenge()
                }

                route("hide") {
                    post {
                        createHiddenChallenge()
                    }

                    delete {
                        deleteHiddenChallenge()
                    }
                }
            }

            post {
                createChallenge()
            }

            delete("{challengeID}") {
                deleteChallenge()
            }
        }

        route("challenges") {
            get {
                readChallenges()
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readChallenge() {

        val challengeID = call.challengeID
        val authenticatedProfileID = call.authenticatedProfileID

        val result = challengeService.read(authenticatedProfileID, challengeID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    @Suppress("ThrowsCount")
    private suspend fun PipelineContext<Unit, ApplicationCall>.readChallenges() {

        val authenticatedProfileID = call.authenticatedProfileID

        val ids = call.request.queryParameters.getAll("id[]")
            ?.map { it.toLong().toChallengeID() }

        val profileID = call.request.queryParameters["profileID"]?.toLong()?.toProfileID()

        val groupID = call.request.queryParameters["groupID"]?.toLong()?.toGroupID()

        val result: Result<*, HttpError> = when {

            ids.isNullOrEmpty() && profileID == null && groupID == null ->
                throw Error.InvalidParametersRequest("'id[]' and 'profile' and 'group' can't be both null")
            !ids.isNullOrEmpty() && profileID != null ->
                throw Error.InvalidParametersRequest("'id[]' and 'profile' can't be both requested")
            !ids.isNullOrEmpty() && groupID != null ->
                throw Error.InvalidParametersRequest("'id[]' and 'group' can't be both requested")
            profileID != null && groupID != null ->
                throw Error.InvalidParametersRequest("'profile' and 'group' can't be both requested")

            !ids.isNullOrEmpty() ->
                challengeService.readChallenges(authenticatedProfileID, ids)
                    .mapError(mapServiceError)

            profileID != null ->
                challengeService.readChallengesForProfile(authenticatedProfileID, profileID)
                    .mapError(mapServiceError)

            groupID != null && authenticatedProfileID != null ->
                challengeService.readChallengesForGroup(authenticatedProfileID, groupID)
                    .mapError(mapServiceError)

            else ->
                throw Error.InvalidParametersRequest("Invalid parameters")
        }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createChallenge() {

        val challengeCreate = call.receive<Challenge.Create>()
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val authenticatedUserID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = challengeService.create(authenticatedProfileID, authenticatedUserID, challengeCreate)
            .mapError(mapServiceError)

        call.respondResult(result, Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteChallenge() {

        val challengeID = call.challengeID
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = challengeService.delete(authenticatedProfileID, challengeID)
            .mapError(mapServiceError)
            .map { NoContent }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createHiddenChallenge() {

        val challengeID = call.challengeID
        val profileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val userID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = challengeService.hideChallenge(profileID, userID, challengeID)
            .mapError(mapServiceError)
            .map { Created }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteHiddenChallenge() {

        val challengeID = call.challengeID
        val profileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = challengeService.unhideChallenge(profileID, challengeID)
            .mapError(mapServiceError)
            .map { NoContent }

        call.respondResult(result)
    }

    private inline val ApplicationCall.challengeID: ChallengeID
        get() = parameters["challengeID"]?.toLong()?.toChallengeID()
            ?: throw Error.InvalidChallengeParameter(
                "Invalid challengeID " +
                    "parameter '${this.parameters["challengeID"]}'"
            )

    // TODO improve this
    private val mapServiceError: (ChallengeService.Error) -> HttpError = {
        when (it) {
            is ChallengeService.Error.ChallengeNotFound ->
                HttpError(NotFound, CHALLENGE_NOT_FOUND, "Challenge '${it.challengeID}' not found")

            is ChallengeService.Error.ChallengeNotFoundByFirebaseID ->
                HttpError(NotFound, CHALLENGE_NOT_FOUND, "Challenge with firebaseID '${it.firebaseID}' not found")

            is ChallengeService.Error.PrivateGroupsChallengeInconsistent ->
                HttpError(
                    InternalServerError,
                    CHALLENGE_INCONSISTENT,
                    "Challenge '${it.challengeID}' data is inconsistent for group privacy"
                )

            is ChallengeService.Error.Forbidden ->
                HttpError(Forbidden, FORBIDDEN, "Can't access challenge '${it.challengeID}' ")

            is ChallengeService.Error.NotAuthorizedGroup ->
                HttpError(Forbidden, FORBIDDEN, "Can't access group '${it.groupID}' ")

            is ChallengeService.Error.BadRequest ->
                HttpError(BadRequest, BADREQUEST, it.message)

            is ChallengeService.Error.America ->
                HttpError(InternalServerError, AMERICA, "America error: ${it.cause?.localizedMessage}")

            is ChallengeService.Error.Unknown ->
                HttpError(InternalServerError, UNKNOWN, "Unknown error: ${it.cause?.localizedMessage}")
        }
    }

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class InvalidChallengeParameter(override val message: String) : Error()
        data class InvalidMultipleChallengeRequest(override val message: String) : Error()
        data class InvalidParametersRequest(override val message: String) : Error()
        data class HeaderNotFound(val header: String) : Error(null)
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
