@file:SuppressWarnings("TooGenericExceptionThrown", "TooManyFunctions")

package tech.youclap.dopamina.controller

import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.NoContent
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.patch
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.mexico.MexicoBlockedChallenge
import tech.youclap.dopamina.model.mexico.MexicoBlockedComment
import tech.youclap.dopamina.model.mexico.MexicoBlockedPost
import tech.youclap.dopamina.model.mexico.MexicoChallenge
import tech.youclap.dopamina.model.mexico.MexicoClap
import tech.youclap.dopamina.model.mexico.MexicoComment
import tech.youclap.dopamina.model.mexico.MexicoDeleteClap
import tech.youclap.dopamina.model.mexico.MexicoPost
import tech.youclap.dopamina.service.MexicoService
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.extension.toHttpError
import tech.youclap.klap.ktor.model.HttpError

class MexicoHTTPController(
    private val service: MexicoService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(MexicoHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {
        route("dopamina/mexico") {

            route("challenge") {
                post {
                    createChallenge()
                }

                delete("{firebaseID}") {
                    deleteChallenge()
                }

                post("blocked") {
                    createBlockedChallenge()
                }

                delete("{firebaseID}/user/{userID}") {
                    deleteBlockedChallenge()
                }
            }

            route("post") {
                post {
                    createPost()
                }

                delete("{firebaseID}") {
                    deletePost()
                }

                post("blocked") {
                    createBlockedPost()
                }

                delete("{firebaseID}/user/{userID}") {
                    deleteBlockedPost()
                }
            }

            route("clap") {
                post {
                    createClap()
                }

                patch {
                    updateClap()
                }

                delete {
                    deleteClap()
                }
            }

            route("comment") {
                post {
                    createComment()
                }

                delete("{firebaseID}") {
                    deleteComment()
                }

                post("blocked") {
                    createBlockedComment()
                }

                delete("{firebaseID}/user/{userID}") {
                    deleteBlockedComment()
                }
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createChallenge() {

        val mexicoChallenge = call.receive<MexicoChallenge>()

        logger.info("New Challenge: $mexicoChallenge")

        val result = service.createChallenge(mexicoChallenge)
            .mapError(mapServiceError)

        call.respondResult(result, Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteChallenge() {

        val challengeFirebaseID = call.firebaseID

        logger.info("Delete Challenge: $challengeFirebaseID")

        val result = service.deleteChallenge(challengeFirebaseID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createBlockedChallenge() {

        val mexicoBlockedChallenge = call.receive<MexicoBlockedChallenge>()

        logger.info("New BlockedChallenge: $mexicoBlockedChallenge")

        val result = service.createBlockedChallenge(mexicoBlockedChallenge)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteBlockedChallenge() {
        val challengeFirebaseID = call.firebaseID
        val userID = call.parameters["userID"] ?: throw Error("Invalid userID parameter '${call.parameters["userID"]}'")

        logger.info("Delete Blocked Challenge: $challengeFirebaseID userID $userID")

        val result = service.deleteBlockedChallenge(userID, challengeFirebaseID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createPost() {

        val mexicoPost = call.receive<MexicoPost>()

        logger.info("New Post: $mexicoPost")

        val result = service.createPost(mexicoPost)
            .mapError(mapServiceError)

        call.respondResult(result, Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deletePost() {

        val postFirebaseID = call.firebaseID

        logger.info("Delete post: $postFirebaseID")

        val result = service.deletePost(postFirebaseID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createBlockedPost() {

        val mexicoBlockedPost = call.receive<MexicoBlockedPost>()

        logger.info("New BlockedPost: $mexicoBlockedPost")

        val result = service.createBlockedPost(mexicoBlockedPost)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteBlockedPost() {
        val postFirebaseID = call.firebaseID
        val userID = call.parameters["userID"] ?: throw Error("Invalid userID parameter '${call.parameters["userID"]}'")

        logger.info("Delete Blocked Post: $postFirebaseID userID $userID")

        val result = service.deleteBlockedPost(userID, postFirebaseID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createClap() {

        val mexicoClap = call.receive<MexicoClap>()

        logger.info("New clap: $mexicoClap")

        val result = service.createClap(mexicoClap)
            .mapError(mapServiceError)

        call.respondResult(result, Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.updateClap() {

        val mexicoClap = call.receive<MexicoClap>()

        logger.info("Update clap: $mexicoClap")

        val result = service.updateClap(mexicoClap)
            .mapError(mapServiceError)

        call.respondResult(result, Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteClap() {

        val mexicoDeleteClap = call.receive<MexicoDeleteClap>()

        logger.info("Delete clap : $mexicoDeleteClap")

        val result = service.deleteClap(mexicoDeleteClap.postID, mexicoDeleteClap.userID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createComment() {

        val mexicoComment = call.receive<MexicoComment>()

        logger.info("New comemnt: $mexicoComment")

        val result = service.createComment(mexicoComment)
            .mapError(mapServiceError)

        call.respondResult(result, Created)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteComment() {

        val commentFirebaseID = call.firebaseID

        logger.info("Delete comment: $commentFirebaseID")

        val result = service.deleteComment(commentFirebaseID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createBlockedComment() {

        val mexicoBlockedComment = call.receive<MexicoBlockedComment>()

        logger.info("New BlockedComment: $mexicoBlockedComment")

        val result = service.createBlockedComment(mexicoBlockedComment)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteBlockedComment() {
        val commentFirebaseID = call.firebaseID
        val userID = call.parameters["userID"] ?: throw Error("Invalid userID parameter '${call.parameters["userID"]}'")

        logger.info("Delete Blocked Comment: $commentFirebaseID userID $userID")

        val result = service.deleteBlockedComment(userID, commentFirebaseID)
            .mapError(mapServiceError)

        call.respondResult(result, NoContent)
    }

    private inline val ApplicationCall.firebaseID: String
        get() = parameters["firebaseID"]
            ?: throw Error("Invalid challenge parameter '${this.parameters["firebaseID"]}'")

    // TODO improve this
    private val mapServiceError: (Exception) -> HttpError = {
        it.toHttpError()
    }
}
