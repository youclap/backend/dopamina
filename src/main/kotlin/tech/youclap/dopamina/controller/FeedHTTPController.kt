package tech.youclap.dopamina.controller

import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.FeedItem
import tech.youclap.dopamina.service.FeedService
import tech.youclap.dopamina.shared.ErrorCode
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.model.HttpError

class FeedHTTPController(
    private val service: FeedService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(FeedHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {
        route("dopamina/feed") {
            post {
                readFeed()
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readFeed() {

        val feed = call.receive<Array<FeedItem>>()

        logger.info("New feed request: $feed")

        val result = service.readFeed(feed.toList())
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private val mapServiceError: (FeedService.Error) -> HttpError = {
        when (it) {
            is FeedService.Error.Unknown ->
                HttpError(
                    HttpStatusCode.InternalServerError,
                    ErrorCode.UNKNOWN,
                    "Unknown error: ${it.cause?.localizedMessage}"
                )
        }
    }
}
