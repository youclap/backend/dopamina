package tech.youclap.dopamina.controller

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.PostSort
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.service.PostService
import tech.youclap.dopamina.shared.ErrorCode.ALREADY_POSTED
import tech.youclap.dopamina.shared.ErrorCode.BADREQUEST
import tech.youclap.dopamina.shared.ErrorCode.CHALLENGE_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.CHALLENGE_NOT_ONGOING
import tech.youclap.dopamina.shared.ErrorCode.FORBIDDEN
import tech.youclap.dopamina.shared.ErrorCode.INVALID_POST
import tech.youclap.dopamina.shared.ErrorCode.POST_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.UNKNOWN
import tech.youclap.dopamina.shared.PROFILEID_HEADER
import tech.youclap.dopamina.shared.USERID_HEADER
import tech.youclap.dopamina.shared.extension.authenticatedProfileID
import tech.youclap.dopamina.shared.extension.authenticatedUserID
import tech.youclap.klap.core.extension.valueOrNull
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.model.HttpError

class PostHTTPController(
    private val postService: PostService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(PostHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {
        route("post") {
            route("{postID}") {
                get {
                    readPost()
                }

                route("hide") {
                    post {
                        createHiddenPost()
                    }

                    delete {
                        deleteHiddenPost()
                    }
                }
            }

            get {
                readUserPost()
            }

            post {
                createPost()
            }

            delete("{postID}") {
                deletePost()
            }
        }

        route("posts") {
            get {
                readPosts()
            }

            get("count") {
                readPostCount()
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readPost() {

        val postID = call.postID
        val authenticatedProfileID = call.authenticatedProfileID

        val result = postService.read(authenticatedProfileID, postID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readUserPost() {
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val challengeID = call.request.queryParameters["challengeID"]?.toLong()?.toChallengeID()
            ?: throw Error.InvalidQueryParameters("'challengeID' can't be both null")

        val result = postService.readUserPost(authenticatedProfileID, challengeID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createPost() {

        val postCreate = call.receive<Post.Create>()
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val authenticatedUserID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = postService.create(authenticatedProfileID, authenticatedUserID, postCreate)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deletePost() {

        val postID = call.postID
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = postService.delete(authenticatedProfileID, postID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.NoContent }

        call.respondResult(result)
    }

    @Suppress("ThrowsCount")
    private suspend fun PipelineContext<Unit, ApplicationCall>.readPosts() {

        val authenticatedProfileID = call.authenticatedProfileID

        val profileID = call.request.queryParameters["profileID"]?.toLong()?.toProfileID()
        val challengeID = call.request.queryParameters["challengeID"]?.toLong()?.toChallengeID()

        val result: Result<*, HttpError> = when {

            profileID == null && challengeID == null ->
                throw Error.InvalidQueryParameters("'profileID' and 'challengeID' can't be both null")

            profileID != null && challengeID != null ->
                throw Error.InvalidQueryParameters("'profileID' and 'challengeID' can't be both requested")

            profileID != null ->
                postService.readPostsForProfile(authenticatedProfileID, profileID)
                    .mapError(mapServiceError)

            challengeID != null -> {

                val sort = call.request.queryParameters["sort"]?.let { valueOrNull<PostSort>(it) }
                    ?: throw Error.InvalidQueryParameters("Invalid sort parameter")

                postService.readPostsForChallenge(authenticatedProfileID, challengeID, sort)
                    .mapError(mapServiceError)
            }

            else ->
                throw Error.InvalidQueryParameters("Invalid parameters")
        }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readPostCount() {

        val authenticatedProfileID = call.authenticatedProfileID

        val challengeID = call.request.queryParameters["challengeID"]?.toLong()?.toChallengeID()

        val result: Result<*, HttpError> = when {

            challengeID != null ->
                postService.countForChallenge(authenticatedProfileID, challengeID)
                    .mapError(mapServiceError)

            else ->
                throw Error.InvalidQueryParameters("Invalid parameters")
        }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createHiddenPost() {

        val postID = call.postID
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val authenticatedUserID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = postService.hidePost(authenticatedProfileID, authenticatedUserID, postID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.Created }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteHiddenPost() {

        val postID = call.postID
        val profileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = postService.unhidePost(profileID, postID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.NoContent }

        call.respondResult(result)
    }

    private inline val ApplicationCall.postID: PostID
        get() = parameters["postID"]?.toLong()?.toPostID()
            ?: throw Error.InvalidPostParameter("Invalid post parameter '${this.parameters["postID"]}'")

    // TODO improve this
    private val mapServiceError: (PostService.Error) -> HttpError = {
        when (it) {
            is PostService.Error.InvalidPost ->
                HttpError(BadRequest, INVALID_POST, "Invalid Post '${it.message}'")

            is PostService.Error.PostNotFound ->
                HttpError(NotFound, POST_NOT_FOUND, "Post '${it.postID}' not found")

            is PostService.Error.PostNotFoundByFirebaseID ->
                HttpError(NotFound, POST_NOT_FOUND, "Post with firebaseID '${it.firebaseID}' not found")

            is PostService.Error.UserPostNotFoundForChallenge ->
                HttpError(NotFound, POST_NOT_FOUND, "User Post for challenge '${it.challengeID}' not found")

            is PostService.Error.Forbidden ->
                HttpError(Forbidden, FORBIDDEN, "Can't access post ${it.postID}")

            is PostService.Error.Unknown ->
                HttpError(InternalServerError, UNKNOWN, "Unknown error: ${it.cause?.localizedMessage}")

            is PostService.Error.ChallengeNotFound ->
                HttpError(NotFound, CHALLENGE_NOT_FOUND, "Challenge not found '${it.challengeID}'")

            is PostService.Error.ForbiddenChallenge ->
                HttpError(Forbidden, FORBIDDEN, "Can't access challenge ${it.challengeID}")

            is PostService.Error.ForbiddenGroup ->
                HttpError(Forbidden, FORBIDDEN, "Can't access group ${it.groupID}")

            is PostService.Error.ChallengeNotOngoing ->
                HttpError(BadRequest, CHALLENGE_NOT_ONGOING, "Challenge not ongoing ${it.challengeID}")

            is PostService.Error.BadRequest ->
                HttpError(BadRequest, BADREQUEST, it.message)

            is PostService.Error.AlreadyPosted ->
                HttpError(BadRequest, ALREADY_POSTED, "Already posted in challenge ${it.challengeID}")
        }
    }

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class InvalidPostParameter(override val message: String) : Error()
        data class InvalidQueryParameters(override val message: String) : Error(null)
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
        data class HeaderNotFound(val header: String) : Error(null)
    }
}
