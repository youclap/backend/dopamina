package tech.youclap.dopamina.controller

import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.patch
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.service.ClapService
import tech.youclap.dopamina.shared.ErrorCode.BADREQUEST
import tech.youclap.dopamina.shared.ErrorCode.CHALLENGE_NOT_ONGOING
import tech.youclap.dopamina.shared.ErrorCode.CLAP_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.FORBIDDEN
import tech.youclap.dopamina.shared.ErrorCode.POST_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.UNKNOWN
import tech.youclap.dopamina.shared.PROFILEID_HEADER
import tech.youclap.dopamina.shared.USERID_HEADER
import tech.youclap.dopamina.shared.extension.authenticatedProfileID
import tech.youclap.dopamina.shared.extension.authenticatedUserID
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.model.HttpError

class ClapHTTPController(
    private val clapService: ClapService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(ClapHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {
        route("clap") {

            get {
                read()
            }

            post {
                createOrUpdate()
            }

            patch {
                createOrUpdate()
            }

            delete {
                delete()
            }
        }

        route("claps") {
            get("count") {
                readCount()
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.read() {

        val postID = call.queryPostID.toPostID()
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = clapService.read(authenticatedProfileID, postID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createOrUpdate() {

        val clapCreate = call.receive<Clap.Create>()
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val authenticatedUserID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = clapService.createOrUpdate(authenticatedProfileID, authenticatedUserID, clapCreate)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.delete() {

        val postID = call.queryPostID.toPostID()
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = clapService.delete(authenticatedProfileID, postID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.NoContent }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readCount() {

        val postID = call.queryPostID.toPostID()
        val authenticatedProfileID = call.authenticatedProfileID

        val result = clapService.countForPost(authenticatedProfileID, postID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private inline val ApplicationCall.queryPostID: Long
        get() = request.queryParameters["postID"]?.toLong()
            ?: throw Error.InvalidQueryParameters("Invalid parameter '${this.parameters["postID"]}'")

    // TODO improve this
    private val mapServiceError: (ClapService.Error) -> HttpError = {
        when (it) {
            is ClapService.Error.ClapNotFound ->
                HttpError(
                    NotFound,
                    CLAP_NOT_FOUND,
                    "Clap postID='${it.postID}' profileID='${it.profileID}' not found"
                )

            is ClapService.Error.ChallengeNotOngoing ->
                HttpError(BadRequest, CHALLENGE_NOT_ONGOING, "Challenge not ongoing ${it.challengeID}")

            is ClapService.Error.PostNotFound ->
                HttpError(NotFound, POST_NOT_FOUND, "Post '${it.postID}' not found")

            is ClapService.Error.BadRequest ->
                HttpError(BadRequest, BADREQUEST, it.message)

            is ClapService.Error.ForbiddenPost ->
                HttpError(Forbidden, FORBIDDEN, "Can't access post ${it.postID}")

            is ClapService.Error.ForbiddenGroup ->
                HttpError(Forbidden, FORBIDDEN, "Can't access group ${it.groupID}")

            is ClapService.Error.Unknown ->
                HttpError(InternalServerError, UNKNOWN, "Unknown error: ${it.cause?.localizedMessage}")
        }
    }

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class InvalidQueryParameters(override val message: String) : Error(null)
        data class HeaderNotFound(val header: String) : Error(null)
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
