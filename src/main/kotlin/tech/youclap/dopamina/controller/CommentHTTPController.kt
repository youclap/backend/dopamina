package tech.youclap.dopamina.controller

import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.extension.toCommentID
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.service.CommentService
import tech.youclap.dopamina.shared.ErrorCode.BADREQUEST
import tech.youclap.dopamina.shared.ErrorCode.COMMENT_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.FORBIDDEN
import tech.youclap.dopamina.shared.ErrorCode.INVALID_COMMENT
import tech.youclap.dopamina.shared.ErrorCode.POST_NOT_FOUND
import tech.youclap.dopamina.shared.ErrorCode.UNKNOWN
import tech.youclap.dopamina.shared.PROFILEID_HEADER
import tech.youclap.dopamina.shared.USERID_HEADER
import tech.youclap.dopamina.shared.extension.authenticatedProfileID
import tech.youclap.dopamina.shared.extension.authenticatedUserID
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.model.HttpError

class CommentHTTPController(
    private val commentService: CommentService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(CommentHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {
        route("comment") {
            route("{commentID}") {
                get {
                    readComment()
                }

                route("hide") {
                    post {
                        createHiddenComment()
                    }

                    delete {
                        deleteHiddenComment()
                    }
                }
            }

            post {
                createComment()
            }

            delete("{commentID}") {
                deleteComment()
            }
        }

        route("comments") {
            get {
                readComments()
            }

            get("count") {
                readCount()
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readComment() {

        val commentID = call.commentID
        val authenticatedProfileID = call.authenticatedProfileID

        val result = commentService.read(authenticatedProfileID, commentID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createComment() {

        val commentCreate = call.receive<Comment.Create>()
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val authenticatedUserID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = commentService.create(authenticatedProfileID, authenticatedUserID, commentCreate)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteComment() {

        val commentID = call.commentID
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = commentService.delete(authenticatedProfileID, commentID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.NoContent }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readComments() {

        val authenticatedProfileID = call.authenticatedProfileID

        val postID = call.queryPostID.toPostID()

        val result = commentService.readCommentsForPost(authenticatedProfileID, postID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.readCount() {

        val postID = call.queryPostID.toPostID()
        val authenticatedProfileID = call.authenticatedProfileID

        val result = commentService.countForPost(authenticatedProfileID, postID)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createHiddenComment() {

        val commentID = call.commentID
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)
        val authenticatedUserID = call.authenticatedUserID ?: throw Error.HeaderNotFound(USERID_HEADER)

        val result = commentService.hideComment(authenticatedProfileID, authenticatedUserID, commentID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.Created }

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteHiddenComment() {

        val commentID = call.commentID
        val authenticatedProfileID = call.authenticatedProfileID ?: throw Error.HeaderNotFound(PROFILEID_HEADER)

        val result = commentService.unhideComment(authenticatedProfileID, commentID)
            .mapError(mapServiceError)
            .map { HttpStatusCode.NoContent }

        call.respondResult(result)
    }

    private inline val ApplicationCall.commentID: CommentID
        get() = parameters["commentID"]?.toLong()?.toCommentID()
            ?: throw Error.InvalidCommentParameter("Invalid comment parameter '${this.parameters["commentID"]}'")

    private inline val ApplicationCall.queryPostID: Long
        get() = request.queryParameters["postID"]?.toLong()
            ?: throw ClapHTTPController.Error.InvalidQueryParameters("Invalid parameter '${this.parameters["postID"]}'")

    // TODO improve this
    private val mapServiceError: (CommentService.Error) -> HttpError = {
        when (it) {
            is CommentService.Error.CommentNotFound ->
                HttpError(NotFound, COMMENT_NOT_FOUND, "Comment '${it.commentID}' not found")

            is CommentService.Error.PostNotFound ->
                HttpError(NotFound, POST_NOT_FOUND, "Post '${it.postID}' not found")

            is CommentService.Error.Forbidden ->
                HttpError(Forbidden, FORBIDDEN, "Can't access comment ${it.commentID}")

            is CommentService.Error.ForbiddenPost ->
                HttpError(Forbidden, FORBIDDEN, "Can't access post ${it.postID}")

            is CommentService.Error.ForbiddenChallenge ->
                HttpError(Forbidden, FORBIDDEN, "Can't access challenge ${it.challengeID}")

            is CommentService.Error.ForbiddenGroup ->
                HttpError(Forbidden, FORBIDDEN, "Can't access group ${it.groupID}")

            is CommentService.Error.InvalidComment ->
                HttpError(BadRequest, INVALID_COMMENT, "Invalid Comment '${it.message}'")

            is CommentService.Error.BadRequest ->
                HttpError(BadRequest, BADREQUEST, it.message)

            is CommentService.Error.Unknown ->
                HttpError(InternalServerError, UNKNOWN, "Unknown error: ${it.cause?.localizedMessage}")
        }
    }

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class InvalidCommentParameter(override val message: String) : Error()
        data class HeaderNotFound(val header: String) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
