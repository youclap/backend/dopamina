package tech.youclap.dopamina

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.github.kittinunf.result.Result
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.InternalServerError
import org.koin.ktor.ext.inject
import tech.youclap.dopamina.controller.ChallengeHTTPController
import tech.youclap.dopamina.controller.ClapHTTPController
import tech.youclap.dopamina.controller.CommentHTTPController
import tech.youclap.dopamina.controller.PostHTTPController
import tech.youclap.dopamina.shared.ErrorCode.INVALID_CHALLENGE_PARAMETER
import tech.youclap.dopamina.shared.ErrorCode.INVALID_MULTIPLE_CHALLENGE_PARAMETER
import tech.youclap.dopamina.shared.ErrorCode.INVALID_PARAMETERS
import tech.youclap.dopamina.shared.ErrorCode.INVALID_POST_PARAMETER
import tech.youclap.dopamina.shared.ErrorCode.UNKNOWN
import tech.youclap.dopamina.shared.PROFILEID_HEADER
import tech.youclap.klap.ktor.extension.isDev
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.extension.toHttpError
import tech.youclap.klap.ktor.model.HttpError

@Suppress("LongMethod", "ComplexMethod")
fun Application.main() {

    installKoin()

    install(ContentNegotiation) {
        val objectMapper: ObjectMapper by inject()

        jacksonInjected(mapper = objectMapper) {
            registerModule(JodaModule())
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            if (isDev) enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    if (isDev) install(CallLogging)

    install(CORS) {
        header("X-Requested-With")
        header(PROFILEID_HEADER)
        anyHost()
        allowNonSimpleContentTypes = true
    }

    // Generic exception handling
    install(StatusPages) {
        // TODO improve this. maybe try to use controller to handle this
        exception<PostHTTPController.Error> { exception ->
            log.error("❌ unhandled exception", exception)

            val httpError = when (exception) {
                is PostHTTPController.Error.InvalidPostParameter ->
                    HttpError(BadRequest, INVALID_POST_PARAMETER, exception.message)

                is PostHTTPController.Error.InvalidQueryParameters ->
                    HttpError(BadRequest, INVALID_POST_PARAMETER, exception.message)

                is PostHTTPController.Error.HeaderNotFound ->
                    HttpError(BadRequest, INVALID_PARAMETERS, "Header '${exception.header}' not found")

                is PostHTTPController.Error.Unknown ->
                    HttpError(InternalServerError, UNKNOWN, "Unknown error: ${exception.cause?.localizedMessage}")
            }

            call.respondResult(Result.error(httpError))
        }

        exception<ChallengeHTTPController.Error> { exception ->
            log.error("❌ unhandled exception", exception)

            val httpError = when (exception) {
                is ChallengeHTTPController.Error.InvalidChallengeParameter ->
                    HttpError(BadRequest, INVALID_CHALLENGE_PARAMETER, exception.message)

                is ChallengeHTTPController.Error.InvalidMultipleChallengeRequest ->
                    HttpError(BadRequest, INVALID_MULTIPLE_CHALLENGE_PARAMETER, exception.message)

                is ChallengeHTTPController.Error.InvalidParametersRequest ->
                    HttpError(BadRequest, INVALID_PARAMETERS, exception.message)

                is ChallengeHTTPController.Error.HeaderNotFound ->
                    HttpError(BadRequest, INVALID_PARAMETERS, "Header '${exception.header}' not found")

                is ChallengeHTTPController.Error.Unknown ->
                    HttpError(InternalServerError, UNKNOWN, "Unknown error: ${exception.cause?.localizedMessage}")
            }

            call.respondResult(Result.error(httpError))
        }

        exception<ClapHTTPController.Error> { exception ->
            log.error("❌ unhandled exception", exception)

            val httpError = when (exception) {

                is ClapHTTPController.Error.InvalidQueryParameters ->
                    HttpError(BadRequest, INVALID_PARAMETERS, exception.message)

                is ClapHTTPController.Error.HeaderNotFound ->
                    HttpError(BadRequest, INVALID_PARAMETERS, "Header '${exception.header}' not found")

                is ClapHTTPController.Error.Unknown ->
                    HttpError(InternalServerError, UNKNOWN, "Unknown error: ${exception.cause?.localizedMessage}")
            }

            call.respondResult(Result.error(httpError))
        }

        exception<CommentHTTPController.Error> { exception ->
            log.error("❌ unhandled exception", exception)

            val httpError = when (exception) {

                is CommentHTTPController.Error.HeaderNotFound ->
                    HttpError(BadRequest, INVALID_PARAMETERS, "Header '${exception.header}' not found")

                is CommentHTTPController.Error.InvalidCommentParameter ->
                    HttpError(BadRequest, INVALID_PARAMETERS, exception.message)

                is CommentHTTPController.Error.Unknown ->
                    HttpError(InternalServerError, UNKNOWN, "Unknown error: ${exception.cause?.localizedMessage}")
            }

            call.respondResult(Result.error(httpError))
        }

        exception<Throwable> { exception ->
            log.error("❌ unhandled exception", exception)
            call.respondResult(Result.error(exception.toHttpError()))
        }
    }

    routes()

    setupDatabase()
}
