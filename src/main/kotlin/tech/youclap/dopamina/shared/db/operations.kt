@file:SuppressWarnings("MatchingDeclarationName")
package tech.youclap.dopamina.shared.db

import org.jetbrains.exposed.sql.Expression
import org.jetbrains.exposed.sql.ExpressionWithColumnType
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.Query
import org.jetbrains.exposed.sql.QueryBuilder

class NotInSubQueryOp<T>(private val expr: Expression<T>, private val query: Query) : Op<Boolean>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) = queryBuilder {
        append(expr, " NOT IN (")
        query.prepareSQL(this)
        +")"
    }
}

infix fun <T> ExpressionWithColumnType<T>.notInSubQuery(query: Query): Op<Boolean> = NotInSubQueryOp(this, query)
