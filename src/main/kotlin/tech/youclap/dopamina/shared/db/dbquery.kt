@file:Suppress("MatchingDeclarationName", "TooGenericExceptionCaught")

package tech.youclap.dopamina.shared.db

import com.github.kittinunf.result.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.statements.BatchInsertStatement
import org.jetbrains.exposed.sql.transactions.transaction

// TODO maybe find a better name?
suspend fun <T : Any> dbQuery(block: () -> T): Result<T, Exception> {
    return try {
        withContext(Dispatchers.IO) {
            transaction { block() }
        }.run {
            Result.success(this)
        }
    } catch (e: Exception) {
        // TODO maybe we could map this exceptions org.jetbrains.exposed.exceptions
        Result.error(e)
    }
}

class BatchInsertUpdateOnDuplicate(table: Table, private val onDupUpdate: List<Column<*>>) :
    BatchInsertStatement(table, false) {
    override fun prepareSQL(transaction: Transaction): String {
        val onUpdateSQL = if (onDupUpdate.isNotEmpty()) {
            " ON DUPLICATE KEY UPDATE " + onDupUpdate.joinToString {
                "${transaction.identity(it)}=VALUES(${transaction.identity(
                    it
                )})"
            }
        } else ""
        return super.prepareSQL(transaction) + onUpdateSQL
    }
}
