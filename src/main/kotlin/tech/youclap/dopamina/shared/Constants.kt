package tech.youclap.dopamina.shared

const val PROFILEID_HEADER = "X-Profile-ID"
const val USERID_HEADER = "X-User-ID"
