package tech.youclap.dopamina.shared.extension

import tech.youclap.dopamina.model.ClapType

inline val ClapType.value: Int
    get() = when (this) {
        ClapType.SINGLE -> 1
        ClapType.DOUBLE -> 2
    }
