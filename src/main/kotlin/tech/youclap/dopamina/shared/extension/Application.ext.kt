package tech.youclap.dopamina.shared.extension

import io.ktor.application.Application

inline val Application.teatroURL
    get() = environment.config.property("endpoints.teatro").getString()
