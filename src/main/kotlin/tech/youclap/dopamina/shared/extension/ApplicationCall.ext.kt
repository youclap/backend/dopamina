package tech.youclap.dopamina.shared.extension

import io.ktor.application.ApplicationCall
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.shared.PROFILEID_HEADER
import tech.youclap.dopamina.shared.USERID_HEADER
import tech.youclap.klap.ktor.extension.requestHeader

val ApplicationCall.authenticatedUserID: UserID?
    get() = requestHeader(USERID_HEADER)?.toLong()?.toUserID()

val ApplicationCall.authenticatedProfileID: ProfileID?
    get() = requestHeader(PROFILEID_HEADER)?.toLong()?.toProfileID()
