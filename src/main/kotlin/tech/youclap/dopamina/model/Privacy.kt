package tech.youclap.dopamina.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.ProfileID

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = Privacy.Public::class, name = "PUBLIC"),
    JsonSubTypes.Type(value = Privacy.PrivateProfiles::class, name = "PRIVATE_PROFILES"),
    JsonSubTypes.Type(value = Privacy.PrivateGroups::class, name = "PRIVATE_GROUPS")
)
sealed class Privacy {

    object Public : Privacy()

    data class PrivateProfiles(val profileIDs: List<ProfileID>) : Privacy()

    data class PrivateGroups(val groupID: GroupID) : Privacy()
}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = PrivacyCreate.Public::class, name = "PUBLIC"),
    JsonSubTypes.Type(value = PrivacyCreate.PrivateProfiles::class, name = "PRIVATE_PROFILES"),
    JsonSubTypes.Type(value = PrivacyCreate.PrivateGroups::class, name = "PRIVATE_GROUPS")
)
sealed class PrivacyCreate {

    object Public : PrivacyCreate()

    data class PrivateProfiles(val profileIDs: List<Long>) : PrivacyCreate()

    data class PrivateGroups(val groupID: Long) : PrivacyCreate()
}
