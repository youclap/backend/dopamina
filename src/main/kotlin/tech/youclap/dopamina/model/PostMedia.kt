package tech.youclap.dopamina.model

sealed class PostMedia {

    data class Image(val imageURL: String) : PostMedia()

    data class Video(val videoURL: String, val thumbnailURL: String) : PostMedia()

    data class Text(val message: String, val startColor: String, val endColor: String) : PostMedia()
}
