package tech.youclap.dopamina.model

data class FeedItem(
    val firebaseID: String,
    val type: FeedType
)
