package tech.youclap.dopamina.model.extension

import org.joda.time.DateTime
import tech.youclap.dopamina.model.response.ChallengeResponse

fun ChallengeResponse.isOngoing(): Boolean {
    val now = DateTime.now()
    return beginDate <= now && (endDate?.let { it >= now } ?: true)
}
