package tech.youclap.dopamina.model.extension

fun Int.toHexColor(): String {
    return "#%06x".format(this)
}
