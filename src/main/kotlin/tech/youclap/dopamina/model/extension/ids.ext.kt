package tech.youclap.dopamina.model.extension

import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

inline fun Long.toChallengeID() = ChallengeID(this)

inline fun Long.toPostID() = PostID(this)

inline fun Long.toCommentID() = CommentID(this)

inline fun Long.toProfileID() = ProfileID(this)

inline fun Long.toUserID() = UserID(this)

inline fun Long.toGroupID() = GroupID(this)
