package tech.youclap.dopamina.model.extension

import tech.youclap.dopamina.model.PrivacyCreate
import tech.youclap.dopamina.model.PrivacyType

val PrivacyCreate.type
    get() = when (this) {
        is PrivacyCreate.Public -> PrivacyType.PUBLIC
        is PrivacyCreate.PrivateProfiles -> PrivacyType.PRIVATE_PROFILES
        is PrivacyCreate.PrivateGroups -> PrivacyType.PRIVATE_GROUPS
    }
