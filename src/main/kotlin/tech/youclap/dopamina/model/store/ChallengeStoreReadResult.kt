package tech.youclap.dopamina.model.store

import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.Privacy

// TODO review this name 🤮
data class ChallengeStoreReadResult(
    val challenge: Challenge,
    val privacy: Privacy
)
