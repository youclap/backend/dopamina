package tech.youclap.dopamina.model.id

import com.fasterxml.jackson.annotation.JsonValue

inline class ProfileID(@JsonValue val value: Long)
