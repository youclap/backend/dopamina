package tech.youclap.dopamina.model.id

import com.fasterxml.jackson.annotation.JsonValue

inline class CommentID(@JsonValue val value: Long)
