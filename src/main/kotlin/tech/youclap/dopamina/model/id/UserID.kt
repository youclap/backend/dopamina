package tech.youclap.dopamina.model.id

import com.fasterxml.jackson.annotation.JsonValue

inline class UserID(@JsonValue val value: Long)
