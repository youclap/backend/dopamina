package tech.youclap.dopamina.model.id

import com.fasterxml.jackson.annotation.JsonValue

inline class PostID(@JsonValue val value: Long)
