package tech.youclap.dopamina.model.id

import com.fasterxml.jackson.annotation.JsonValue

inline class GroupID(@JsonValue val value: Long)
