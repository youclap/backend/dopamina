package tech.youclap.dopamina.model.id

import com.fasterxml.jackson.annotation.JsonValue

inline class ChallengeID(@JsonValue val value: Long)
