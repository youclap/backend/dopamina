package tech.youclap.dopamina.model

enum class ChallengeMediaType {
    IMAGE,
    VIDEO
}
