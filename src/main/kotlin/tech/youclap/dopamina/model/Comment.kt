package tech.youclap.dopamina.model

import org.joda.time.DateTime
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

data class Comment(
    val id: Long?,
    val postID: PostID,
    val profileID: ProfileID,
    val userID: UserID,
    val message: String,
    val createdDate: DateTime?,
    val deleted: Boolean?,
    val firebaseID: String
) {

    data class Create(
        val postID: Long,
        val profileID: Long,
        val userID: Long,
        val message: String
    )
}
