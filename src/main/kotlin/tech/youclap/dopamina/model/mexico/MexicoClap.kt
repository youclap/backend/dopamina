package tech.youclap.dopamina.model.mexico

import org.joda.time.DateTime
import tech.youclap.dopamina.model.ClapType

data class MexicoClap(
    val challengeID: String? = null,
    val postID: String,
    val timestamp: DateTime,
    val type: ClapType,
    val userID: String
)

data class MexicoDeleteClap(
    val postID: String,
    val userID: String
)
