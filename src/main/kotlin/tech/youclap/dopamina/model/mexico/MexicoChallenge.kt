package tech.youclap.dopamina.model.mexico

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.joda.time.DateTime
import tech.youclap.dopamina.model.PrivacyType

data class MexicoChallenge(
    val deleted: Boolean,
    val endDate: DateTime,
    val media: MexicoChallengeMediaTypeContainer,
    val participations: List<MexicoChallengeParticipations>,
    val privacy: MexicoChallengePrivacy,
    val startDate: DateTime,
    val timestamp: DateTime,
    val title: String,
    val description: String?,
    val uid: String,
    val userID: String
)

enum class MexicoChallengeMediaType {
    PHOTO,
    VIDEO
}

data class MexicoChallengeMediaTypeContainer(
    val type: MexicoChallengeMediaType
)

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = MexicoChallengePrivacy.Public::class, name = "PUBLIC"),
    JsonSubTypes.Type(value = MexicoChallengePrivacy.PrivateProfiles::class, name = "PRIVATE_USERS"),
    JsonSubTypes.Type(value = MexicoChallengePrivacy.PrivateGroups::class, name = "PRIVATE_GROUP")
)
sealed class MexicoChallengePrivacy(@JsonIgnore val type: PrivacyType) {

    object Public : MexicoChallengePrivacy(PrivacyType.PUBLIC)

    data class PrivateProfiles(@JsonProperty("value") val userIDs: List<String>) :
        MexicoChallengePrivacy(PrivacyType.PRIVATE_PROFILES)

    data class PrivateGroups(@JsonProperty("value") val groupID: String) :
        MexicoChallengePrivacy(PrivacyType.PRIVATE_GROUPS)
}

enum class MexicoChallengeParticipations {
    VIDEO,
    PHOTO,
    TEXT
}

data class MexicoBlockedChallenge(
    val challengeID: String,
    val timestamp: DateTime,
    val userID: String
)
