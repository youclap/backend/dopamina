package tech.youclap.dopamina.model.mexico

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.joda.time.DateTime

data class MexicoPost(
    val challengeID: String,
    val deleted: Boolean,
    val media: MexicoPostMedia,
    val timestamp: DateTime,
    val uid: String,
    val userID: String,
    val privacy: MexicoChallengePrivacy? = null
)

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = MexicoPostMedia.Photo::class, name = "PHOTO"),
    JsonSubTypes.Type(value = MexicoPostMedia.Video::class, name = "VIDEO"),
    JsonSubTypes.Type(value = MexicoPostMedia.Text::class, name = "TEXT")
)
sealed class MexicoPostMedia(@JsonIgnore val type: MexicoPostMediaType) {

    object Photo : MexicoPostMedia(MexicoPostMediaType.PHOTO)

    object Video : MexicoPostMedia(MexicoPostMediaType.VIDEO)

    data class Text(val colors: List<String>, @JsonProperty("value") val message: String) :
        MexicoPostMedia(MexicoPostMediaType.TEXT)
}

enum class MexicoPostMediaType {
    VIDEO,
    PHOTO,
    TEXT
}

data class MexicoBlockedPost(
    val postID: String,
    val timestamp: DateTime,
    val userID: String
)
