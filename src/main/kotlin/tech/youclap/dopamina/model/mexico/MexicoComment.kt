package tech.youclap.dopamina.model.mexico

import org.joda.time.DateTime

data class MexicoComment(
    val deleted: Boolean,
    val postID: String,
    val timestamp: DateTime,
    val uid: String,
    val userID: String,
    val value: String
)

data class MexicoBlockedComment(
    val commentID: String,
    val timestamp: DateTime,
    val userID: String,
    val postID: String? = null
)
