package tech.youclap.dopamina.model

enum class PostType {
    IMAGE,
    VIDEO,
    TEXT
}
