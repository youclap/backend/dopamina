package tech.youclap.dopamina.model

import org.joda.time.DateTime
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

data class Clap(
    val postID: PostID,
    val profileID: ProfileID,
    val userID: UserID,
    val type: ClapType,
    val createdDate: DateTime?
) {

    data class Create(
        val postID: Long,
        val profileID: Long,
        val userID: Long,
        val type: ClapType
    )
}
