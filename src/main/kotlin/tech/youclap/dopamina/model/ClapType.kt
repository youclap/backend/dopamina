package tech.youclap.dopamina.model

enum class ClapType {
    SINGLE,
    DOUBLE
}
