package tech.youclap.dopamina.model

import tech.youclap.dopamina.model.ChallengeMediaType.IMAGE
import tech.youclap.dopamina.model.ChallengeMediaType.VIDEO

sealed class ChallengeMedia(val type: ChallengeMediaType) {

    data class Image(val imageURL: String) : ChallengeMedia(IMAGE)

    data class Video(val videoURL: String, val thumbnailURL: String) : ChallengeMedia(VIDEO)
}
