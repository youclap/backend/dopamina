package tech.youclap.dopamina.model.america

import com.fasterxml.jackson.annotation.JsonValue

data class AmericaMessage(
    val collection: AmericaCollection,
    val action: Action,
    val documentID: String,
    val data: Any? = null
)

enum class AmericaCollection(@JsonValue val value: String) {
    Challenge("challenge"),
    BlockedChallenge("blocked-challenge"),
    Post("post"),
    BlockedPost("blocked-post"),
    Comment("comment"),
    BlockedComment("blocked-comment"),
    Clap("clap"),
}

enum class Action {
    CREATE,
    UPDATE,
    MARK_DELETED,
    DELETE
}
