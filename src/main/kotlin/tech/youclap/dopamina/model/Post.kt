package tech.youclap.dopamina.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.joda.time.DateTime
import tech.youclap.dopamina.model.PostType.IMAGE
import tech.youclap.dopamina.model.PostType.TEXT
import tech.youclap.dopamina.model.PostType.VIDEO
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

sealed class Post(val type: PostType) {
    abstract val id: PostID?
    abstract val challengeID: ChallengeID
    abstract val profileID: ProfileID
    abstract val userID: UserID
    abstract val createdDate: DateTime?
    abstract val deleted: Boolean?
    abstract val firebaseID: String
    abstract val challengeFirebaseID: String?
    abstract val claps: Int

    data class Image(
        override val id: PostID? = null,
        override val challengeID: ChallengeID,
        override val profileID: ProfileID,
        override val userID: UserID,
        override val createdDate: DateTime? = null,
        override val deleted: Boolean? = null,
        override val firebaseID: String,
        override val challengeFirebaseID: String?,
        override val claps: Int
    ) : Post(IMAGE)

    data class Video(
        override val id: PostID? = null,
        override val challengeID: ChallengeID,
        override val profileID: ProfileID,
        override val userID: UserID,
        override val createdDate: DateTime? = null,
        override val deleted: Boolean? = null,
        override val firebaseID: String,
        override val challengeFirebaseID: String?,
        override val claps: Int
    ) : Post(VIDEO)

    data class Text(
        override val id: PostID? = null,
        override val challengeID: ChallengeID,
        override val profileID: ProfileID,
        override val userID: UserID,
        override val createdDate: DateTime? = null,
        override val deleted: Boolean? = null,
        override val firebaseID: String,
        override val challengeFirebaseID: String?,
        override val claps: Int,
        val startColor: Int,
        val endColor: Int,
        val message: String
    ) : Post(TEXT)

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
    @JsonSubTypes(
        JsonSubTypes.Type(value = Create.Image::class, name = "IMAGE"),
        JsonSubTypes.Type(value = Create.Video::class, name = "VIDEO"),
        JsonSubTypes.Type(value = Create.Text::class, name = "TEXT")
    )
    sealed class Create(@JsonIgnore val type: PostType) {
        abstract val userID: Long
        abstract val profileID: Long
        abstract val challengeID: Long

        data class Image(
            override val userID: Long,
            override val profileID: Long,
            override val challengeID: Long
        ) : Create(IMAGE)

        data class Video(
            override val userID: Long,
            override val profileID: Long,
            override val challengeID: Long
        ) : Create(VIDEO)

        data class Text(
            override val userID: Long,
            override val profileID: Long,
            override val challengeID: Long,
            val startColor: String,
            val endColor: String,
            val message: String
        ) : Create(TEXT)
    }
}
