package tech.youclap.dopamina.model

enum class FeedType {
    CHALLENGE,
    POST
}
