package tech.youclap.dopamina.model

enum class PrivacyType {
    PUBLIC,
    PRIVATE_PROFILES,
    PRIVATE_GROUPS
}
