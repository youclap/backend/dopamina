package tech.youclap.dopamina.model

enum class PostSort {
    RANDOM,
    TOP,
    NEWEST,
    OLDEST
}
