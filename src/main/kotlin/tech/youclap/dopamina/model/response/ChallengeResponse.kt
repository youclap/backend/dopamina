package tech.youclap.dopamina.model.response

import org.joda.time.DateTime
import tech.youclap.dopamina.model.ChallengeMedia
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

data class ChallengeResponse(
    val id: ChallengeID,
    val userID: UserID,
    val profileID: ProfileID,
    val privacy: Privacy,
    val createdDate: DateTime,
    val beginDate: DateTime,
    val endDate: DateTime?,
    val media: ChallengeMedia,
    val title: String,
    val description: String?,
    val allowImage: Boolean,
    val allowVideo: Boolean,
    val allowText: Boolean,
    val firebaseID: String
)
