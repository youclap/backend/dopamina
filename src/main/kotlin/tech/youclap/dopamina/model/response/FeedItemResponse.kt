package tech.youclap.dopamina.model.response

import tech.youclap.dopamina.model.FeedType
import tech.youclap.dopamina.model.FeedType.CHALLENGE
import tech.youclap.dopamina.model.FeedType.POST

sealed class FeedItemResponse(val type: FeedType) {

    data class Challenge(val item: ChallengeResponse) : FeedItemResponse(CHALLENGE)

    data class Post(val item: PostResponse) : FeedItemResponse(POST)
}
