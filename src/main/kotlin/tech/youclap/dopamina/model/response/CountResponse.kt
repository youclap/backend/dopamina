package tech.youclap.dopamina.model.response

data class CountResponse(val count: Int)
