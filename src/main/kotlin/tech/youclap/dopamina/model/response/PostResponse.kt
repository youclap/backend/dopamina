package tech.youclap.dopamina.model.response

import org.joda.time.DateTime
import tech.youclap.dopamina.model.PostMedia
import tech.youclap.dopamina.model.PostType
import tech.youclap.dopamina.model.PostType.IMAGE
import tech.youclap.dopamina.model.PostType.TEXT
import tech.youclap.dopamina.model.PostType.VIDEO
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

// TODO maybe this could be simplified ???
sealed class PostResponse(val type: PostType) {
    abstract val id: PostID
    abstract val userID: UserID
    abstract val profileID: ProfileID
    abstract val challengeID: ChallengeID
    abstract val createdDate: DateTime
    abstract val media: PostMedia
    abstract val firebaseID: String

    data class Image(
        override val id: PostID,
        override val userID: UserID,
        override val profileID: ProfileID,
        override val challengeID: ChallengeID,
        override val createdDate: DateTime,
        override val media: PostMedia,
        override val firebaseID: String
    ) : PostResponse(IMAGE)

    data class Video(
        override val id: PostID,
        override val userID: UserID,
        override val profileID: ProfileID,
        override val challengeID: ChallengeID,
        override val createdDate: DateTime,
        override val media: PostMedia,
        override val firebaseID: String
    ) : PostResponse(VIDEO)

    data class Text(
        override val id: PostID,
        override val userID: UserID,
        override val profileID: ProfileID,
        override val challengeID: ChallengeID,
        override val createdDate: DateTime,
        override val media: PostMedia,
        override val firebaseID: String
    ) : PostResponse(TEXT)
}
