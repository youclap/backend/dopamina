package tech.youclap.dopamina.model.response

import org.joda.time.DateTime
import tech.youclap.dopamina.model.ClapType
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

data class ClapResponse(
    val postID: PostID,
    val profileID: ProfileID,
    val userID: UserID,
    val type: ClapType,
    val createdDate: DateTime
)
