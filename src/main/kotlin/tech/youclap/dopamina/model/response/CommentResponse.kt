package tech.youclap.dopamina.model.response

import org.joda.time.DateTime
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

data class CommentResponse(
    val id: Long,
    val postID: PostID,
    val userID: UserID,
    val profileID: ProfileID,
    val message: String,
    val createdDate: DateTime,
    val firebaseID: String
)
