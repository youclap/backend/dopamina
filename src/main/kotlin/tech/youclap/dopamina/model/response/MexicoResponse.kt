package tech.youclap.dopamina.model.response

import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID

data class MexicoChallengeResponse(val challengeID: ChallengeID)

data class MexicoPostResponse(val postID: PostID)
