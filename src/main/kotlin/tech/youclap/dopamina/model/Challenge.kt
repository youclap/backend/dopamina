package tech.youclap.dopamina.model

import org.joda.time.DateTime
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID

data class Challenge(
    val id: ChallengeID?,
    val userID: UserID,
    val profileID: ProfileID,
    val privacy: PrivacyType,
    val createdDate: DateTime?,
    val beginDate: DateTime,
    val endDate: DateTime?,
    val mediaType: ChallengeMediaType,
    val title: String,
    val description: String?,
    val allowImage: Boolean,
    val allowVideo: Boolean,
    val allowText: Boolean,
    val deleted: Boolean?,
    val firebaseID: String
) {

    data class Create(
        val userID: Long,
        val profileID: Long,
        val privacy: PrivacyCreate,
        val beginDate: DateTime,
        val endDate: DateTime? = null,
        val mediaType: ChallengeMediaType,
        val title: String,
        val description: String? = null,
        val allowImage: Boolean,
        val allowVideo: Boolean,
        val allowText: Boolean
    )
}
