package tech.youclap.dopamina

import io.ktor.application.Application
import io.ktor.routing.routing
import org.koin.ktor.ext.inject
import tech.youclap.dopamina.controller.ChallengeHTTPController
import tech.youclap.dopamina.controller.ClapHTTPController
import tech.youclap.dopamina.controller.CommentHTTPController
import tech.youclap.dopamina.controller.FeedHTTPController
import tech.youclap.dopamina.controller.MexicoHTTPController
import tech.youclap.dopamina.controller.PostHTTPController
import tech.youclap.dopamina.service.ChallengeService
import tech.youclap.dopamina.service.ClapService
import tech.youclap.dopamina.service.CommentService
import tech.youclap.dopamina.service.FeedService
import tech.youclap.dopamina.service.MexicoService
import tech.youclap.dopamina.service.PostService
import tech.youclap.klap.ktor.controller.HealthCheckController
import tech.youclap.klap.ktor.extension.route

fun Application.routes() {

    val challengeService: ChallengeService by inject()
    val postService: PostService by inject()
    val clapService: ClapService by inject()
    val commentService: CommentService by inject()

    val feedService: FeedService by inject()

    val mexicoService: MexicoService by inject()

    routing {

        route(HealthCheckController())

        route(ChallengeHTTPController(challengeService))
        route(PostHTTPController(postService))
        route(ClapHTTPController(clapService))
        route(CommentHTTPController(commentService))

        route(FeedHTTPController(feedService))

        route(MexicoHTTPController(mexicoService))
    }
}
