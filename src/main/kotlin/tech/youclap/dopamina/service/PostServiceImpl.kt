@file:SuppressWarnings("TooManyFunctions", "ChainWrapping")

package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.flatMap
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import tech.youclap.dopamina.ServiceConfiguration
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.PostSort
import tech.youclap.dopamina.model.extension.isOngoing
import tech.youclap.dopamina.model.extension.toChallengeID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.CountResponse
import tech.youclap.dopamina.model.response.PostResponse
import tech.youclap.dopamina.service.PostService.Error
import tech.youclap.dopamina.service.extension.toResponse
import tech.youclap.dopamina.store.PostStore
import tech.youclap.klap.core.utils.generateFirebaseID

class PostServiceImpl(
    private val configurations: ServiceConfiguration,
    private val store: PostStore,
    private val authorizationService: AuthorizationService,
    private val challengeService: ChallengeService,
    private val americaService: AmericaService
) : PostService {

    private val regex: Regex by lazy {
        Regex("[a-fA-F0-9]{6}")
    }

    @SuppressWarnings("ComplexCondition")
    override suspend fun create(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        postCreate: Post.Create
    ): Result<PostResponse, Error> {

        if (postCreate.profileID != authenticatedProfileID.value || postCreate.userID != authenticatedUserID.value) {
            return Result.error(Error.BadRequest("Invalid create post data"))
        }

        return challengeService.read(authenticatedProfileID, postCreate.challengeID.toChallengeID())
            .mapError(errorChallengeHandler)
            .flatMap { challenge ->

                if (!challenge.isOngoing()) {
                    return@flatMap Result.error(Error.ChallengeNotOngoing(postCreate.challengeID.toChallengeID()))
                }

                if (!(postCreate is Post.Create.Image && challenge.allowImage)
                    && !(postCreate is Post.Create.Text && challenge.allowText)
                    && !(postCreate is Post.Create.Video && challenge.allowVideo)
                ) {
                    return@flatMap Result.error(Error.InvalidPost("Type '${postCreate.type}' not allowed"))
                }

                if (postCreate is Post.Create.Text && postCreate.message.isBlank()) {
                    return@flatMap Result.error(Error.InvalidPost("Post text can't be empty"))
                }

                store.alreadyPosted(authenticatedProfileID, postCreate.challengeID.toChallengeID())
                    .map { challenge to it }
            }
            .flatMap { (challenge, alreadyPosted) ->

                if (alreadyPosted) {
                    return@flatMap Result.error(Error.AlreadyPosted(postCreate.challengeID.toChallengeID()))
                }

                val postResult = postCreate.toPost()

                postResult.flatMap { store.createPost(it) }
                    .flatMap { read(authenticatedProfileID, it) }
                    .map { challenge to it }
            }
            .flatMap { (challenge, post) ->
                americaService.createPost(post, challenge.privacy)
                    .map { post }
            }
            .mapError(errorHandler)
    }

    override suspend fun read(authenticatedProfileID: ProfileID?, postID: PostID): Result<PostResponse, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.Forbidden(postID))
                }

                store.readPost(authenticatedProfileID, postID)
                    .map { it.toResponse(configurations.teatroURL) }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun delete(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error> {

        return store.isPostCreator(authenticatedProfileID, postID)
            .mapError(errorStoreHandler)
            .flatMap { isCreator ->

                if (!isCreator) {
                    return@flatMap Result.error(Error.Forbidden(postID))
                }

                return@flatMap store.deletePost(postID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.removePost(postID)
            }
            .mapError(errorHandler)
    }

    override suspend fun readPostsForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID
    ): Result<List<PostResponse>, Error> {

        return store.readPostsForProfile(authenticatedProfileID, profileID, authenticatedProfileID == profileID)
            .map { list ->
                list.map { it.toResponse(configurations.teatroURL) }
            }
            .mapError(errorStoreHandler)
    }

    override suspend fun readPostsForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID,
        sort: PostSort
    ): Result<List<PostResponse>, Error> {

        return authorizationService.canAccessChallenges(authenticatedProfileID, listOf(challengeID))
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessMap ->
                if (!canAccessMap.getOrDefault(challengeID, false)) {
                    return@flatMap Result.error(Error.ForbiddenChallenge(challengeID))
                }

                store.readPostsForChallenge(authenticatedProfileID, challengeID, sort)
                    .map { list ->
                        list.map { it.toResponse(configurations.teatroURL) }
                    }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun readUserPost(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<PostResponse, Error> {
        return store.readUserPostForChallenge(authenticatedProfileID, challengeID)
            .map { it.toResponse(configurations.teatroURL) }
            .mapError(errorStoreHandler)
    }

    override suspend fun countForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID
    ): Result<CountResponse, Error> {

        return authorizationService.canAccessChallenges(authenticatedProfileID, listOf(challengeID))
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessMap ->
                if (!canAccessMap.getOrDefault(challengeID, false)) {
                    return@flatMap Result.error(Error.ForbiddenChallenge(challengeID))
                }

                store.readPostCountForChallenge(challengeID)
                    .map { CountResponse(it) }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun hidePost(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        postID: PostID
    ): Result<Unit, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.Forbidden(postID))
                }

                store.hidePost(authenticatedProfileID, authenticatedUserID, postID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.createHiddenPost(authenticatedProfileID, postID)
            }
            .mapError(errorHandler)
    }

    override suspend fun unhidePost(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.Forbidden(postID))
                }

                store.unhidePost(authenticatedProfileID, postID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.removeHiddenPost(authenticatedProfileID, postID)
            }
            .mapError(errorHandler)
    }

    private val errorStoreHandler: (PostStore.Error) -> Error = {
        when (it) {
            is PostStore.Error.PostNotFound ->
                Error.PostNotFound(it.postID, it)

            is PostStore.Error.PostNotFoundByFirebaseID ->
                Error.PostNotFoundByFirebaseID(it.firebasaseID)

            is PostStore.Error.UserPostNotFoundForChallenge ->
                Error.UserPostNotFoundForChallenge(it.challengeID)

            is PostStore.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorHandler: (Exception) -> Error = {
        when (it) {
            is Error ->
                it

            is PostStore.Error.PostNotFound ->
                Error.PostNotFound(it.postID, it)

            else ->
                Error.Unknown(it)
        }
    }

    private val errorAuthorizationHandler: (AuthorizationService.Error) -> Error = {
        when (it) {
            is AuthorizationService.Error.PostNotFound ->
                Error.PostNotFound(it.postID, it)

            is AuthorizationService.Error.CommentNotFound,
            is AuthorizationService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorChallengeHandler: (ChallengeService.Error) -> Error = {
        when (it) {

            is ChallengeService.Error.ChallengeNotFound ->
                Error.ChallengeNotFound(it.challengeID)

            is ChallengeService.Error.Forbidden ->
                Error.ForbiddenChallenge(it.challengeID)

            is ChallengeService.Error.NotAuthorizedGroup ->
                Error.ForbiddenGroup(it.groupID)

            // Should not happen here
            is ChallengeService.Error.BadRequest,
            is ChallengeService.Error.PrivateGroupsChallengeInconsistent,
            is ChallengeService.Error.ChallengeNotFoundByFirebaseID,
            is ChallengeService.Error.America,
            is ChallengeService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    @Suppress("TooGenericExceptionCaught")
    private fun Post.Create.toPost(): Result<Post, Error> {
        return try {
            val post = when (this) {
                is Post.Create.Image -> Post.Image(
                    userID = userID.toUserID(),
                    profileID = profileID.toProfileID(),
                    challengeID = challengeID.toChallengeID(),
                    firebaseID = generateFirebaseID(),
                    challengeFirebaseID = null,
                    claps = 0
                )

                is Post.Create.Video -> Post.Video(
                    userID = userID.toUserID(),
                    profileID = profileID.toProfileID(),
                    challengeID = challengeID.toChallengeID(),
                    firebaseID = generateFirebaseID(),
                    challengeFirebaseID = null,
                    claps = 0
                )

                is Post.Create.Text -> {
                    Post.Text(
                        userID = userID.toUserID(),
                        profileID = profileID.toProfileID(),
                        challengeID = challengeID.toChallengeID(),
                        firebaseID = generateFirebaseID(),
                        challengeFirebaseID = null,
                        claps = 0,
                        startColor = startColor.parseColor(),
                        endColor = endColor.parseColor(),
                        message = message.trim()
                    )
                }
            }

            Result.success(post)
        } catch (e: Error) {
            Result.error(e)
        } catch (e: Throwable) {
            Result.error(Error.Unknown(e))
        }
    }

    private fun String.parseColor(): Int {
        return regex.find(this)?.value
            ?.toInt(radix = 16)
            ?: throw Error.InvalidPost("Invalid color '$this'")
    }
}
