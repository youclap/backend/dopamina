package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import tech.youclap.dopamina.ServiceConfiguration
import tech.youclap.dopamina.model.FeedItem
import tech.youclap.dopamina.model.FeedType
import tech.youclap.dopamina.model.response.FeedItemResponse
import tech.youclap.dopamina.service.extension.toResponse
import tech.youclap.dopamina.store.ChallengeStore
import tech.youclap.dopamina.store.PostStore

class FeedServiceImpl(
    private val configurations: ServiceConfiguration,
    private val challengeStore: ChallengeStore,
    private val postStore: PostStore
) : FeedService {

    override suspend fun readFeed(feed: List<FeedItem>): Result<List<FeedItemResponse>, FeedService.Error> {
        // TODO 🔨🔨🔨🔨🔨🔨🔨🔨🔨🔨🔨
        return feed.mapNotNull { feedItem ->
            try {
                when (feedItem.type) {
                    FeedType.CHALLENGE ->
                        challengeStore.readChallengeByFirebaseID(feedItem.firebaseID)
                            .map {
                                FeedItemResponse.Challenge(it.toResponse(configurations.teatroURL))
                            }
                            .get()

                    FeedType.POST ->
                        postStore.readPostByFirebaseID(feedItem.firebaseID)
                            .map {
                                FeedItemResponse.Post(it.toResponse(configurations.teatroURL))
                            }
                            .get()
                }
            } catch (_: Exception) {
                // TODO 🔨🔨🔨🔨🔨🔨🔨🔨🔨🔨🔨
                null
            }
        }
            .let {
                Result.success(it)
            }
            .mapError(errorHandler)
    }

    private val errorHandler: (Exception) -> FeedService.Error = {
        when (it) {
            is FeedService.Error ->
                it

            else ->
                FeedService.Error.Unknown(it)
        }
    }
}
