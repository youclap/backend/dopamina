package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.store.AuthorizationStore

class AuthorizationServiceImpl(
    private val authorizationStore: AuthorizationStore
) : AuthorizationService {

    override suspend fun canAccessChallenges(
        profileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<Map<ChallengeID, Boolean>, AuthorizationService.Error> {

        return authorizationStore.canAccessChallenges(profileID, challengeIDs)
            .mapError(errorStoreHandler)
    }

    override suspend fun canAccessPost(
        profileID: ProfileID?,
        postID: PostID
    ): Result<Boolean, AuthorizationService.Error> {
        return authorizationStore.canAccessPost(profileID, postID)
            .mapError(errorStoreHandler)
    }

    override suspend fun canAccessComment(
        profileID: ProfileID?,
        commentID: CommentID
    ): Result<Boolean, AuthorizationService.Error> {
        return authorizationStore.canAccessComment(profileID, commentID)
            .mapError(errorStoreHandler)
    }

    override suspend fun canAccessGroup(
        profileID: ProfileID,
        groupID: GroupID
    ): Result<Boolean, AuthorizationService.Error> {
        return authorizationStore.canAccessGroup(profileID, groupID)
            .mapError(errorStoreHandler)
    }

    private val errorStoreHandler: (AuthorizationStore.Error) -> AuthorizationService.Error = {
        when (it) {
            is AuthorizationStore.Error.PostNotFound ->
                AuthorizationService.Error.PostNotFound(it.postID)

            is AuthorizationStore.Error.CommentNotFound ->
                AuthorizationService.Error.CommentNotFound(it.commentID)

            is AuthorizationStore.Error.Unknown ->
                AuthorizationService.Error.Unknown(it)
        }
    }
}
