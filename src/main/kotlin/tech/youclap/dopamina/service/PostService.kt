package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.PostSort
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.CountResponse
import tech.youclap.dopamina.model.response.PostResponse

interface PostService {

    suspend fun read(authenticatedProfileID: ProfileID?, postID: PostID): Result<PostResponse, Error>
    suspend fun create(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        postCreate: Post.Create
    ): Result<PostResponse, Error>

    suspend fun delete(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error>

    suspend fun readPostsForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID
    ): Result<List<PostResponse>, Error>

    suspend fun readPostsForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID,
        sort: PostSort
    ): Result<List<PostResponse>, Error>

    suspend fun readUserPost(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<PostResponse, Error>

    suspend fun countForChallenge(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID
    ): Result<CountResponse, Error>

    suspend fun hidePost(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        postID: PostID
    ): Result<Unit, Error>

    suspend fun unhidePost(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {

        data class PostNotFound(val postID: PostID, override val cause: Throwable? = null) : Error(cause)

        data class ChallengeNotFound(val challengeID: ChallengeID) : Error(null)

        data class PostNotFoundByFirebaseID(val firebaseID: String) : Error(null)

        data class UserPostNotFoundForChallenge(val challengeID: ChallengeID) : Error(null)

        data class Forbidden(val postID: PostID) : Error(null)

        data class ForbiddenChallenge(val challengeID: ChallengeID) : Error(null)

        data class ForbiddenGroup(val groupID: GroupID) : Error(null)

        data class ChallengeNotOngoing(val challengeID: ChallengeID) : Error(null)

        data class AlreadyPosted(val challengeID: ChallengeID) : Error(null)

        data class InvalidPost(override val message: String) : Error(null)

        data class BadRequest(override val message: String) : Error(null)

        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
