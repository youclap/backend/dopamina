package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.ClapResponse
import tech.youclap.dopamina.model.response.CountResponse

interface ClapService {

    suspend fun createOrUpdate(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        clapCreate: Clap.Create
    ): Result<ClapResponse, Error>

    suspend fun read(authenticatedProfileID: ProfileID, postID: PostID): Result<ClapResponse, Error>
    suspend fun delete(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error>

    suspend fun readClapsForPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<List<ClapResponse>, Error>
    suspend fun countForPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<CountResponse, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class ChallengeNotOngoing(val challengeID: ChallengeID) : Error(null)
        data class ClapNotFound(val postID: PostID, val profileID: ProfileID) : ClapService.Error(null)
        data class PostNotFound(val postID: PostID) : Error(null)
        data class BadRequest(override val message: String) : Error(null)
        data class ForbiddenPost(val postID: PostID) : Error(null)
        data class ForbiddenGroup(val groupID: GroupID) : Error(null)
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
