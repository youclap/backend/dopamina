package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.CommentResponse
import tech.youclap.dopamina.model.response.CountResponse

interface CommentService {

    suspend fun read(authenticatedProfileID: ProfileID?, commentID: CommentID): Result<CommentResponse, Error>
    suspend fun create(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        commentCreate: Comment.Create
    ): Result<CommentResponse, Error>

    suspend fun delete(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Unit, Error>

    suspend fun readCommentsForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<List<CommentResponse>, Error>

    suspend fun countForPost(authenticatedProfileID: ProfileID?, postID: PostID): Result<CountResponse, Error>

    suspend fun hideComment(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        commentID: CommentID
    ): Result<Unit, Error>

    suspend fun unhideComment(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Unit, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {

        data class CommentNotFound(val commentID: CommentID) : Error(null)

        data class PostNotFound(val postID: PostID) : Error(null)

        data class Forbidden(val commentID: CommentID) : Error(null)

        data class ForbiddenPost(val postID: PostID) : Error(null)

        data class ForbiddenChallenge(val challengeID: ChallengeID) : Error(null)

        data class ForbiddenGroup(val groupID: GroupID) : Error(null)

        data class InvalidComment(override val message: String) : Error(null)

        data class BadRequest(override val message: String) : Error(null)

        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
