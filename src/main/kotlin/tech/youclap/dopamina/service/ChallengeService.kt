package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.ChallengeResponse

interface ChallengeService {

    suspend fun create(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        challengeCreate: Challenge.Create
    ): Result<ChallengeResponse, Error>

    suspend fun read(authenticatedProfileID: ProfileID?, challengeID: ChallengeID): Result<ChallengeResponse, Error>
    suspend fun delete(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<Unit, Error>

    suspend fun readChallenges(
        authenticatedProfileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<List<ChallengeResponse>, Error>

    suspend fun readChallengesForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID
    ): Result<List<ChallengeResponse>, Error>

    suspend fun readChallengesForGroup(
        authenticatedProfileID: ProfileID,
        groupID: GroupID
    ): Result<List<ChallengeResponse>, Error>

    suspend fun hideChallenge(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        challengeID: ChallengeID
    ): Result<Unit, Error>

    suspend fun unhideChallenge(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<Unit, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class ChallengeNotFound(val challengeID: ChallengeID, override val cause: Throwable? = null) : Error(cause)
        data class ChallengeNotFoundByFirebaseID(val firebaseID: String) : Error(null)
        data class PrivateGroupsChallengeInconsistent(
            val challengeID: ChallengeID,
            override val cause: Throwable? = null
        ) :
            Error(cause)

        data class Forbidden(val challengeID: ChallengeID, override val cause: Throwable? = null) : Error(cause)
        data class NotAuthorizedGroup(val groupID: GroupID, override val cause: Throwable? = null) : Error(cause)
        data class BadRequest(override val message: String) : ChallengeService.Error(null)
        data class America(override val cause: Throwable? = null) : Error(cause)
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
