@file:Suppress("TooManyFunctions")

package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.ChallengeMediaType
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.mexico.MexicoBlockedChallenge
import tech.youclap.dopamina.model.mexico.MexicoBlockedComment
import tech.youclap.dopamina.model.mexico.MexicoBlockedPost
import tech.youclap.dopamina.model.mexico.MexicoChallenge
import tech.youclap.dopamina.model.mexico.MexicoChallengeMediaType
import tech.youclap.dopamina.model.mexico.MexicoChallengeParticipations
import tech.youclap.dopamina.model.mexico.MexicoChallengePrivacy
import tech.youclap.dopamina.model.mexico.MexicoClap
import tech.youclap.dopamina.model.mexico.MexicoComment
import tech.youclap.dopamina.model.mexico.MexicoPost
import tech.youclap.dopamina.model.mexico.MexicoPostMedia
import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.MexicoChallengeResponse
import tech.youclap.dopamina.model.response.MexicoPostResponse
import tech.youclap.dopamina.store.MexicoStore

interface MexicoService {

    suspend fun createChallenge(mexicoChallenge: MexicoChallenge): Result<MexicoChallengeResponse, Exception>
    suspend fun deleteChallenge(challengeFirebaseID: String): Result<Unit, Exception>
    suspend fun createBlockedChallenge(mexicoBlockedChallenge: MexicoBlockedChallenge): Result<Unit, Exception>
    suspend fun deleteBlockedChallenge(userFirebaseID: String, challengeFirebaseID: String): Result<Unit, Exception>

    suspend fun createPost(mexicoPost: MexicoPost): Result<MexicoPostResponse, Exception>
    suspend fun deletePost(postFirebaseID: String): Result<Unit, Exception>
    suspend fun createBlockedPost(mexicoBlockedPost: MexicoBlockedPost): Result<Unit, Exception>
    suspend fun deleteBlockedPost(userFirebaseID: String, postFirebaseID: String): Result<Unit, Exception>

    suspend fun createClap(mexicoClap: MexicoClap): Result<Unit, Exception>
    suspend fun updateClap(mexicoClap: MexicoClap): Result<Unit, Exception>
    suspend fun deleteClap(postFirebaseID: String, userFirebaseID: String): Result<Unit, Exception>

    suspend fun createComment(mexicoComment: MexicoComment): Result<Unit, Exception>
    suspend fun deleteComment(commentFirebaseID: String): Result<Unit, Exception>
    suspend fun createBlockedComment(mexicoBlockedComment: MexicoBlockedComment): Result<Unit, Exception>
    suspend fun deleteBlockedComment(userFirebaseID: String, commentFirebaseID: String): Result<Unit, Exception>
}

class MexicoServiceImpl(private val store: MexicoStore) : MexicoService {

    private val regex: Regex by lazy {
        Regex("[a-fA-F0-9]{6}")
    }

    override suspend fun createChallenge(mexicoChallenge: MexicoChallenge): Result<MexicoChallengeResponse, Exception> {

        val userID = store.readUserID(mexicoChallenge.userID).get()
        val profileID = store.readProfileID(mexicoChallenge.userID).get()

        val challenge = mexicoChallenge.toChallenge(userID, profileID)
        val privacy = mexicoChallenge.privacy.toPrivacy()

        return store.createChallenge(challenge, privacy)
            .map {
                MexicoChallengeResponse(ChallengeID(it))
            }
    }

    override suspend fun deleteChallenge(challengeFirebaseID: String): Result<Unit, Exception> {

        val challengeID = store.readChallengeID(challengeFirebaseID).get()

        return store.deleteChallenge(challengeID)
    }

    override suspend fun createBlockedChallenge(
        mexicoBlockedChallenge: MexicoBlockedChallenge
    ): Result<Unit, Exception> {

        val challengeID = store.readChallengeID(mexicoBlockedChallenge.challengeID).get()
        val profileID = store.readProfileID(mexicoBlockedChallenge.userID).get()
        val userID = store.readUserID(mexicoBlockedChallenge.userID).get()

        return store.createBlockedChallenge(challengeID, profileID, userID, mexicoBlockedChallenge.timestamp)
    }

    override suspend fun deleteBlockedChallenge(
        userFirebaseID: String,
        challengeFirebaseID: String
    ): Result<Unit, Exception> {

        val challengeID = store.readChallengeID(challengeFirebaseID).get()
        val profileID = store.readProfileID(userFirebaseID).get()

        return store.deleteBlockedChallenge(challengeID, profileID)
    }

    override suspend fun createPost(mexicoPost: MexicoPost): Result<MexicoPostResponse, Exception> {

        val userID = store.readUserID(mexicoPost.userID).get()
        val profileID = store.readProfileID(mexicoPost.userID).get()
        val challengeID = store.readChallengeID(mexicoPost.challengeID).get()

        val post = mexicoPost.toPost(userID, profileID, challengeID)

        return store.createPost(post)
            .map {
                MexicoPostResponse(it)
            }
    }

    override suspend fun deletePost(postFirebaseID: String): Result<Unit, Exception> {

        val postID = store.readPostID(postFirebaseID).get()

        return store.deletePost(postID)
    }

    override suspend fun createBlockedPost(mexicoBlockedPost: MexicoBlockedPost): Result<Unit, Exception> {

        val postID = store.readPostID(mexicoBlockedPost.postID).get()
        val profileID = store.readProfileID(mexicoBlockedPost.userID).get()
        val userID = store.readUserID(mexicoBlockedPost.userID).get()

        return store.createBlockedPost(postID, profileID, userID, mexicoBlockedPost.timestamp)
    }

    override suspend fun deleteBlockedPost(userFirebaseID: String, postFirebaseID: String): Result<Unit, Exception> {

        val postID = store.readPostID(postFirebaseID).get()
        val profileID = store.readProfileID(userFirebaseID).get()

        return store.deleteBlockedPost(postID, profileID)
    }

    override suspend fun createClap(mexicoClap: MexicoClap): Result<Unit, Exception> {

        val userID = store.readUserID(mexicoClap.userID).get()
        val profileID = store.readProfileID(mexicoClap.userID).get()
        val postID = store.readPostID(mexicoClap.postID).get()

        val clap = mexicoClap.toClap(userID, profileID, postID)

        return store.createClap(clap)
    }

    override suspend fun updateClap(mexicoClap: MexicoClap): Result<Unit, Exception> {

        val userID = store.readUserID(mexicoClap.userID).get()
        val profileID = store.readProfileID(mexicoClap.userID).get()
        val postID = store.readPostID(mexicoClap.postID).get()

        val clap = mexicoClap.toClap(userID, profileID, postID)

        return store.updateClap(clap)
    }

    override suspend fun deleteClap(postFirebaseID: String, userFirebaseID: String): Result<Unit, Exception> {
        val profileID = store.readProfileID(userFirebaseID).get()
        val postID = store.readPostID(postFirebaseID).get()

        return store.deleteClap(postID, profileID)
    }

    override suspend fun createComment(mexicoComment: MexicoComment): Result<Unit, Exception> {

        val userID = store.readUserID(mexicoComment.userID).get()
        val profileID = store.readProfileID(mexicoComment.userID).get()
        val postID = store.readPostID(mexicoComment.postID).get()

        val comment = mexicoComment.toComment(userID, profileID, postID)

        return store.createComment(comment)
    }

    override suspend fun deleteComment(commentFirebaseID: String): Result<Unit, Exception> {

        val commentID = store.readCommentID(commentFirebaseID).get()

        return store.deleteComment(commentID)
    }

    override suspend fun createBlockedComment(mexicoBlockedComment: MexicoBlockedComment): Result<Unit, Exception> {

        val commentID = store.readCommentID(mexicoBlockedComment.commentID).get()
        val profileID = store.readProfileID(mexicoBlockedComment.userID).get()
        val userID = store.readUserID(mexicoBlockedComment.userID).get()

        return store.createBlockedComment(commentID, profileID, userID, mexicoBlockedComment.timestamp)
    }

    override suspend fun deleteBlockedComment(
        userFirebaseID: String,
        commentFirebaseID: String
    ): Result<Unit, Exception> {

        val commentID = store.readCommentID(commentFirebaseID).get()
        val profileID = store.readProfileID(userFirebaseID).get()

        return store.deleteBlockedComment(commentID, profileID)
    }

    private fun MexicoChallenge.toChallenge(userID: UserID, profileID: ProfileID): Challenge {
        return Challenge(
            id = null,
            userID = userID,
            profileID = profileID,
            privacy = privacy.type,
            createdDate = timestamp,
            beginDate = startDate,
            endDate = endDate,
            mediaType = media.type.toChallengeMediaType(),
            title = title,
            description = description,
            allowImage = participations.contains(MexicoChallengeParticipations.PHOTO),
            allowVideo = participations.contains(MexicoChallengeParticipations.VIDEO),
            allowText = participations.contains(MexicoChallengeParticipations.TEXT),
            deleted = deleted,
            firebaseID = uid
        )
    }

    private fun MexicoChallengeMediaType.toChallengeMediaType(): ChallengeMediaType {
        return when (this) {
            MexicoChallengeMediaType.PHOTO -> ChallengeMediaType.IMAGE
            MexicoChallengeMediaType.VIDEO -> ChallengeMediaType.VIDEO
        }
    }

    private suspend fun MexicoChallengePrivacy.toPrivacy(): Privacy {
        return when (this) {
            is MexicoChallengePrivacy.Public ->
                Privacy.Public

            is MexicoChallengePrivacy.PrivateProfiles ->
                userIDs.map {
                    store.readProfileID(it).get() // TODO maybe not the best way to get?
                }
                    .let {
                        Privacy.PrivateProfiles(it)
                    }

            is MexicoChallengePrivacy.PrivateGroups ->
                store.readGroupID(groupID)
                    .get() // TODO maybe not the best way to get?
                    .let {
                        Privacy.PrivateGroups(it)
                    }
        }
    }

    private fun MexicoPost.toPost(userID: UserID, profileID: ProfileID, challengeID: ChallengeID): Post {

        return when (val media = this.media) {
            is MexicoPostMedia.Photo ->
                Post.Image(
                    id = null,
                    challengeID = challengeID,
                    profileID = profileID,
                    userID = userID,
                    createdDate = timestamp,
                    deleted = deleted,
                    firebaseID = uid,
                    challengeFirebaseID = null,
                    claps = 0
                )

            is MexicoPostMedia.Video ->
                Post.Video(
                    id = null,
                    challengeID = challengeID,
                    profileID = profileID,
                    userID = userID,
                    createdDate = timestamp,
                    deleted = deleted,
                    firebaseID = uid,
                    challengeFirebaseID = null,
                    claps = 0
                )

            is MexicoPostMedia.Text ->
                Post.Text(
                    id = null,
                    challengeID = challengeID,
                    profileID = profileID,
                    userID = userID,
                    createdDate = timestamp,
                    deleted = deleted,
                    firebaseID = uid,
                    challengeFirebaseID = null,
                    claps = 0,
                    message = media.message,
                    startColor = media.colors[0].parseColor(),
                    endColor = media.colors[0].parseColor()
                )
        }
    }

    private fun MexicoClap.toClap(userID: UserID, profileID: ProfileID, postID: PostID): Clap {
        return Clap(
            postID = postID,
            profileID = profileID,
            userID = userID,
            type = type,
            createdDate = timestamp
        )
    }

    private fun MexicoComment.toComment(userID: UserID, profileID: ProfileID, postID: PostID): Comment {
        return Comment(
            id = null,
            postID = postID,
            profileID = profileID,
            userID = userID,
            message = value,
            createdDate = timestamp,
            deleted = deleted,
            firebaseID = uid
        )
    }

    private fun String.parseColor(): Int {
        return regex.find(this)?.value
            ?.toInt(radix = 16)
            ?: throw PostService.Error.InvalidPost(this)
    }
}
