package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.flatMap
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import tech.youclap.dopamina.model.Clap
import tech.youclap.dopamina.model.extension.isOngoing
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.ClapResponse
import tech.youclap.dopamina.model.response.CountResponse
import tech.youclap.dopamina.service.ClapService.Error
import tech.youclap.dopamina.store.ClapStore

class ClapServiceImpl(
    private val store: ClapStore,
    private val authorizationService: AuthorizationService,
    private val postService: PostService,
    private val challengeService: ChallengeService,
    private val americaService: AmericaService
) : ClapService {

    override suspend fun createOrUpdate(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        clapCreate: Clap.Create
    ): Result<ClapResponse, Error> {

        // TODO maybe we could remove this from the create object and just use the header?
        if (clapCreate.profileID != authenticatedProfileID.value || clapCreate.userID != authenticatedUserID.value) {
            return Result.error(Error.BadRequest("Invalid create clap data"))
        }

        return postService.read(authenticatedProfileID, clapCreate.postID.toPostID())
            .mapError(errorPostHandler)
            .flatMap { post ->

                challengeService.read(authenticatedProfileID, post.challengeID)
                    .mapError(errorChallengeHandler)
            }
            .flatMap { challenge ->

                if (!challenge.isOngoing()) {
                    return@flatMap Result.error(Error.ChallengeNotOngoing(challenge.id))
                }

                val currentClap = read(authenticatedProfileID, clapCreate.postID.toPostID())
                val newClap = clapCreate.toResponse()

                when (currentClap) {
                    is Result.Success ->
                        store.updateClap(newClap, currentClap.value.type)

                    is Result.Failure ->
                        store.createClap(newClap)
                }
                    .flatMap { read(authenticatedProfileID, newClap.postID) }
            }
            .flatMap { clapResponse ->
                americaService.createClap(clapResponse)
                    .map { clapResponse }
            }
            .mapError(errorHandler)
    }

    override suspend fun read(authenticatedProfileID: ProfileID, postID: PostID): Result<ClapResponse, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.ForbiddenPost(postID))
                }

                store.readClap(authenticatedProfileID, postID)
                    .map { it.toResponse() }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun delete(authenticatedProfileID: ProfileID, postID: PostID): Result<Unit, Error> {

        return postService.read(authenticatedProfileID, postID)
            .mapError(errorPostHandler)
            .flatMap { post ->

                challengeService.read(authenticatedProfileID, post.challengeID)
                    .mapError(errorChallengeHandler)
            }
            .flatMap { challenge ->

                if (!challenge.isOngoing()) {
                    return@flatMap Result.error(Error.ChallengeNotOngoing(challenge.id))
                }

                store.deleteClap(authenticatedProfileID, postID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.removeClap(authenticatedProfileID, postID)
            }
            .mapError(errorHandler)
    }

    override suspend fun readClapsForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<List<ClapResponse>, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->

                if (!canAccess) {
                    return@flatMap Result.error(Error.ForbiddenPost(postID))
                }

                store.readClapsForPost(authenticatedProfileID, postID)
                    .map { list ->
                        list.map { it.toResponse() }
                    }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun countForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<CountResponse, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->

                if (!canAccess) {
                    return@flatMap Result.error(Error.ForbiddenPost(postID))
                }

                store.count(postID)
                    .map { (singleCount, doubleCount) ->
                        CountResponse(singleCount + doubleCount * 2)
                    }
                    .mapError(errorStoreHandler)
            }
    }

    private val errorStoreHandler: (ClapStore.Error) -> Error = {
        when (it) {
            is ClapStore.Error.ClapNotFound ->
                Error.ClapNotFound(it.postID, it.profileID)

            is ClapStore.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorHandler: (Exception) -> Error = {
        when (it) {
            is Error ->
                it

            is ClapStore.Error.ClapNotFound ->
                Error.ClapNotFound(it.postID, it.profileID)

            else ->
                Error.Unknown(it)
        }
    }

    private val errorAuthorizationHandler: (AuthorizationService.Error) -> Error = {
        when (it) {

            is AuthorizationService.Error.PostNotFound ->
                Error.PostNotFound(it.postID)

            // Should not happen
            is AuthorizationService.Error.CommentNotFound,
            is AuthorizationService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorChallengeHandler: (ChallengeService.Error) -> Error = {
        when (it) {
            is ChallengeService.Error.Forbidden,
            is ChallengeService.Error.NotAuthorizedGroup,
            is ChallengeService.Error.ChallengeNotFound,
            is ChallengeService.Error.ChallengeNotFoundByFirebaseID,
            is ChallengeService.Error.PrivateGroupsChallengeInconsistent,
            is ChallengeService.Error.BadRequest,
            is ChallengeService.Error.America,
            is ChallengeService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorPostHandler: (PostService.Error) -> Error = {
        when (it) {

            is PostService.Error.PostNotFound ->
                Error.PostNotFound(it.postID)

            is PostService.Error.Forbidden ->
                Error.ForbiddenPost(it.postID)

            is PostService.Error.ForbiddenGroup ->
                Error.ForbiddenGroup(it.groupID)

            // Shouldn't happen
            is PostService.Error.BadRequest,
            is PostService.Error.ChallengeNotOngoing,
            is PostService.Error.AlreadyPosted,
            is PostService.Error.InvalidPost,
            is PostService.Error.ForbiddenChallenge,
            is PostService.Error.ChallengeNotFound,
            is PostService.Error.PostNotFoundByFirebaseID,
            is PostService.Error.UserPostNotFoundForChallenge,
            is PostService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private fun Clap.Create.toResponse(): Clap {
        return Clap(
            postID = postID.toPostID(),
            profileID = profileID.toProfileID(),
            userID = userID.toUserID(),
            type = type,
            createdDate = null
        )
    }

    private fun Clap.toResponse(): ClapResponse {

        return ClapResponse(
            postID = postID,
            profileID = profileID,
            userID = userID,
            type = type,
            createdDate = createdDate!!
        )
    }
}
