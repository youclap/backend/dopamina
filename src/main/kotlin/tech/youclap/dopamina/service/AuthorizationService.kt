package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID

interface AuthorizationService {

    suspend fun canAccessChallenges(
        profileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<Map<ChallengeID, Boolean>, Error>

    suspend fun canAccessPost(profileID: ProfileID?, postID: PostID): Result<Boolean, Error>
    suspend fun canAccessComment(profileID: ProfileID?, commentID: CommentID): Result<Boolean, Error>

    suspend fun canAccessGroup(profileID: ProfileID, groupID: GroupID): Result<Boolean, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class PostNotFound(val postID: PostID) : Error(null)
        data class CommentNotFound(val commentID: CommentID) : Error(null)
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
