package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.flatMap
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import tech.youclap.dopamina.model.Comment
import tech.youclap.dopamina.model.extension.toPostID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.CommentResponse
import tech.youclap.dopamina.model.response.CountResponse
import tech.youclap.dopamina.service.CommentService.Error
import tech.youclap.dopamina.store.CommentStore
import tech.youclap.klap.core.utils.generateFirebaseID

class CommentServiceImpl(
    private val store: CommentStore,
    private val authorizationService: AuthorizationService,
    private val postService: PostService,
    private val americaService: AmericaService
) : CommentService {

    @SuppressWarnings("ReturnCount") // TODO try to improve this
    override suspend fun create(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        commentCreate: Comment.Create
    ): Result<CommentResponse, Error> {

        if (commentCreate.profileID != authenticatedProfileID.value ||
            commentCreate.userID != authenticatedUserID.value
        ) {
            return Result.error(Error.BadRequest("Invalid create comment data"))
        }

        return postService.read(authenticatedProfileID, commentCreate.postID.toPostID())
            .mapError(errorPostHandler)
            .flatMap {

                if (commentCreate.message.isBlank()) {
                    return@flatMap Result.error(Error.InvalidComment("Comment text can't be empty"))
                }

                val commentResult = commentCreate.toComment()

                store.createComment(commentResult)
                    .flatMap { read(authenticatedProfileID, it) }
            }
            .flatMap { comment ->
                americaService.createComment(comment)
                    .map { comment }
            }
            .mapError(errorHandler)
    }

    override suspend fun read(
        authenticatedProfileID: ProfileID?,
        commentID: CommentID
    ): Result<CommentResponse, Error> {

        return authorizationService.canAccessComment(authenticatedProfileID, commentID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.Forbidden(commentID))
                }

                store.readComment(authenticatedProfileID, commentID)
                    .map { it.toResponse() }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun delete(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Unit, Error> {

        return store.isCommentCreator(authenticatedProfileID, commentID)
            .mapError(errorStoreHandler)
            .flatMap { isCreator ->

                if (!isCreator) {
                    return@flatMap Result.error(Error.Forbidden(commentID))
                }

                return@flatMap store.deleteComment(commentID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.removeComment(commentID)
            }
            .mapError(errorHandler)
    }

    override suspend fun readCommentsForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<List<CommentResponse>, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessPost ->

                if (!canAccessPost) {
                    return@flatMap Result.error(Error.ForbiddenPost(postID))
                }

                store.readCommentsForPost(authenticatedProfileID, postID)
                    .map { list ->
                        list.map { it.toResponse() }
                    }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun countForPost(
        authenticatedProfileID: ProfileID?,
        postID: PostID
    ): Result<CountResponse, Error> {

        return authorizationService.canAccessPost(authenticatedProfileID, postID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessPost ->

                if (!canAccessPost) {
                    return@flatMap Result.error(Error.ForbiddenPost(postID))
                }

                store.count(postID)
                    .map {
                        CountResponse(it)
                    }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun hideComment(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        commentID: CommentID
    ): Result<Unit, Error> {

        return authorizationService.canAccessComment(authenticatedProfileID, commentID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.Forbidden(commentID))
                }

                store.hideComment(authenticatedProfileID, authenticatedUserID, commentID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.createHiddenComment(authenticatedProfileID, commentID)
            }
            .mapError(errorHandler)
    }

    override suspend fun unhideComment(authenticatedProfileID: ProfileID, commentID: CommentID): Result<Unit, Error> {

        return authorizationService.canAccessComment(authenticatedProfileID, commentID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->
                if (!canAccess) {
                    return@flatMap Result.error(Error.Forbidden(commentID))
                }

                store.unhideComment(authenticatedProfileID, commentID)
                    .mapError(errorStoreHandler)
            }
    }

    private val errorStoreHandler: (CommentStore.Error) -> Error = {
        when (it) {
            is CommentStore.Error.CommentNotFound ->
                Error.CommentNotFound(it.commentID)

            is CommentStore.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorHandler: (Exception) -> Error = {
        when (it) {
            is Error ->
                it

            is CommentStore.Error.CommentNotFound ->
                Error.CommentNotFound(it.commentID)

            else ->
                Error.Unknown(it)
        }
    }

    private val errorPostHandler: (PostService.Error) -> Error = {
        when (it) {

            is PostService.Error.BadRequest ->
                Error.BadRequest(it.message)

            is PostService.Error.PostNotFound ->
                Error.PostNotFound(it.postID)

            is PostService.Error.Forbidden ->
                Error.ForbiddenPost(it.postID)

            is PostService.Error.ForbiddenChallenge ->
                Error.ForbiddenChallenge(it.challengeID)

            is PostService.Error.ForbiddenGroup ->
                Error.ForbiddenGroup(it.groupID)

            // Should not happen here
            is PostService.Error.InvalidPost,
            is PostService.Error.AlreadyPosted,
            is PostService.Error.ChallengeNotOngoing,
            is PostService.Error.UserPostNotFoundForChallenge,
            is PostService.Error.PostNotFoundByFirebaseID,
            is PostService.Error.ChallengeNotFound,
            is PostService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorAuthorizationHandler: (AuthorizationService.Error) -> Error = {
        when (it) {

            is AuthorizationService.Error.PostNotFound ->
                Error.PostNotFound(it.postID)

            is AuthorizationService.Error.CommentNotFound ->
                Error.CommentNotFound(it.commentID)

            is AuthorizationService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private fun Comment.Create.toComment(): Comment {
        return Comment(
            id = null,
            postID = postID.toPostID(),
            userID = userID.toUserID(),
            profileID = profileID.toProfileID(),
            message = message,
            createdDate = null,
            deleted = null,
            firebaseID = generateFirebaseID()
        )
    }

    private fun Comment.toResponse(): CommentResponse {

        return CommentResponse(
            id = id!!,
            postID = postID,
            userID = userID,
            profileID = profileID,
            message = message,
            createdDate = createdDate!!,
            firebaseID = firebaseID
        )
    }
}
