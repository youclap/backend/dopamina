package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.flatMap
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import tech.youclap.dopamina.ServiceConfiguration
import tech.youclap.dopamina.model.Challenge
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.PrivacyCreate
import tech.youclap.dopamina.model.extension.toGroupID
import tech.youclap.dopamina.model.extension.toProfileID
import tech.youclap.dopamina.model.extension.toUserID
import tech.youclap.dopamina.model.extension.type
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.GroupID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.id.UserID
import tech.youclap.dopamina.model.response.ChallengeResponse
import tech.youclap.dopamina.service.ChallengeService.Error
import tech.youclap.dopamina.service.extension.toResponse
import tech.youclap.dopamina.store.AmericaStore
import tech.youclap.dopamina.store.ChallengeStore
import tech.youclap.klap.core.utils.generateFirebaseID

class ChallengeServiceImpl(
    private val configurations: ServiceConfiguration,
    private val store: ChallengeStore,
    private val authorizationService: AuthorizationService,
    private val americaService: AmericaService
) : ChallengeService {

    @SuppressWarnings("ReturnCount") // TODO try to improve this
    override suspend fun create(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        challengeCreate: Challenge.Create
    ): Result<ChallengeResponse, Error> {

        // Validate challenge
        // TODO maybe we could remove this from the create object and just use the header?
        if (challengeCreate.profileID != authenticatedProfileID.value ||
            challengeCreate.userID != authenticatedUserID.value
        ) {
            return Result.error(Error.BadRequest("Invalid create challenge data"))
        }

        // TODO find a way to validate the begin date. because of network lag and 'offline first'

        if (challengeCreate.endDate != null && challengeCreate.beginDate >= challengeCreate.endDate) {
            return Result.error(Error.BadRequest("Challenge 'endDate' has to be after 'beginDate'"))
        }

        if (challengeCreate.title.isBlank()) {
            return Result.error(Error.BadRequest("Challenge 'title' can't be empty"))
        }

        if (!(challengeCreate.allowImage || challengeCreate.allowVideo || challengeCreate.allowText)) {
            return Result.error(Error.BadRequest("Challenge must have at least one allowed media type"))
        }

        // Validate authorization
        return if (challengeCreate.privacy is PrivacyCreate.PrivateGroups) {
            authorizationService.canAccessGroup(authenticatedProfileID, challengeCreate.privacy.groupID.toGroupID())
        } else {
            Result.success(true)
        }
            .flatMap { canAccess ->

                if (!canAccess) {
                    return@flatMap Result.error(
                        Error.NotAuthorizedGroup(
                            (challengeCreate.privacy as PrivacyCreate.PrivateGroups).groupID.toGroupID()
                        )
                    )
                }

                // TODO if privacy is private users, should check if they are friends?? 🤮

                val challenge = challengeCreate.toChallenge()
                val privacy = challengeCreate.privacy.toPrivacy()

                return@flatMap store.createChallenge(challenge, privacy)
                    .flatMap { read(authenticatedProfileID, it) }
            }
            .flatMap { challengeResponse ->
                americaService.createChallenge(challengeResponse)
                    .map { challengeResponse }
            }
            .mapError(errorHandler)
    }

    override suspend fun read(
        authenticatedProfileID: ProfileID?,
        challengeID: ChallengeID
    ): Result<ChallengeResponse, Error> {

        return authorizationService.canAccessChallenges(authenticatedProfileID, listOf(challengeID))
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessMap ->
                if (!canAccessMap.getOrDefault(challengeID, false)) {
                    return@flatMap Result.error(Error.Forbidden(challengeID))
                }

                store.readChallenge(authenticatedProfileID, challengeID)
                    .map { it.toResponse(configurations.teatroURL) }
                    .mapError(errorStoreHandler)
            }
    }

    override suspend fun delete(authenticatedProfileID: ProfileID, challengeID: ChallengeID): Result<Unit, Error> {

        return store.isChallengeCreator(authenticatedProfileID, challengeID)
            .mapError(errorStoreHandler)
            .flatMap { isCreator ->

                if (!isCreator) {
                    return@flatMap Result.error(Error.Forbidden(challengeID))
                }

                return@flatMap store.deleteChallenge(challengeID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.deleteChallenge(challengeID)
                    .mapError(errorHandler)
            }
    }

    @SuppressWarnings("SpreadOperator") // TODO try to remove this
    override suspend fun readChallenges(
        authenticatedProfileID: ProfileID?,
        challengeIDs: List<ChallengeID>
    ): Result<List<ChallengeResponse>, Error> {

        return authorizationService.canAccessChallenges(authenticatedProfileID, challengeIDs)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessMap ->

                val availableChallenges = challengeIDs.filter { canAccessMap.getOrDefault(it, false) }

                store.readChallenges(authenticatedProfileID, availableChallenges)
                    .map { list ->
                        list.map { it.toResponse(configurations.teatroURL) }
                    }
                    .mapError(errorStoreHandler)
            }
    }

    // TODO only public ones for now, or all if same user
    override suspend fun readChallengesForProfile(
        authenticatedProfileID: ProfileID?,
        profileID: ProfileID
    ): Result<List<ChallengeResponse>, Error> {
        return store.readChallengesForProfile(authenticatedProfileID, profileID, authenticatedProfileID == profileID)
            .map { list ->
                list.map { it.toResponse(configurations.teatroURL) }
            }
            .mapError(errorStoreHandler)
    }

    override suspend fun readChallengesForGroup(
        authenticatedProfileID: ProfileID,
        groupID: GroupID
    ): Result<List<ChallengeResponse>, Error> {
        return authorizationService.canAccessGroup(authenticatedProfileID, groupID)
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccess ->

                if (!canAccess) {
                    return@flatMap Result.error(
                        Error.NotAuthorizedGroup(groupID)
                    )
                }

                store.readChallengesForGroup(groupID, authenticatedProfileID)
                    .map { list ->
                        list.map { it.toResponse(configurations.teatroURL) }
                    }
                    .mapError(errorHandler)
            }
    }

    override suspend fun hideChallenge(
        authenticatedProfileID: ProfileID,
        authenticatedUserID: UserID,
        challengeID: ChallengeID
    ): Result<Unit, Error> {

        return authorizationService.canAccessChallenges(authenticatedProfileID, listOf(challengeID))
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessMap ->
                if (!canAccessMap.getOrDefault(challengeID, false)) {
                    return@flatMap Result.error(Error.Forbidden(challengeID))
                }

                store.hideChallenge(authenticatedProfileID, authenticatedUserID, challengeID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.createHiddenChallenge(authenticatedProfileID, challengeID)
                    .mapError(errorHandler)
            }
    }

    override suspend fun unhideChallenge(
        authenticatedProfileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Unit, Error> {

        return authorizationService.canAccessChallenges(authenticatedProfileID, listOf(challengeID))
            .mapError(errorAuthorizationHandler)
            .flatMap { canAccessMap ->
                if (!canAccessMap.getOrDefault(challengeID, false)) {
                    return@flatMap Result.error(Error.Forbidden(challengeID))
                }

                store.unhideChallenge(authenticatedProfileID, challengeID)
                    .mapError(errorStoreHandler)
            }
            .flatMap {
                americaService.deleteHiddenChallenge(authenticatedProfileID, challengeID)
                    .mapError(errorHandler)
            }
    }

    private val errorStoreHandler: (ChallengeStore.Error) -> Error = {
        when (it) {
            is ChallengeStore.Error.ChallengeNotFound ->
                Error.ChallengeNotFound(it.challengeID, it)

            is ChallengeStore.Error.ChallengeNotFoundByFirebaseID ->
                Error.ChallengeNotFoundByFirebaseID(it.firebaseID)

            is ChallengeStore.Error.ChallengePrivateGroupsInconsistent ->
                Error.PrivateGroupsChallengeInconsistent(it.challengeID, it)
            is ChallengeStore.Error.GroupNotFound ->
                Error.NotAuthorizedGroup(it.groupID, it)
            is ChallengeStore.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorAuthorizationHandler: (AuthorizationService.Error) -> Error = {
        when (it) {
            // Shouldn't happen
            is AuthorizationService.Error.CommentNotFound,
            is AuthorizationService.Error.PostNotFound,
            is AuthorizationService.Error.Unknown ->
                Error.Unknown(it)
        }
    }

    private val errorHandler: (Exception) -> Error = {
        when (it) {
            is Error ->
                it

            is AmericaStore.Error ->
                Error.America(it)

            is ChallengeStore.Error.ChallengeNotFound ->
                Error.ChallengeNotFound(it.challengeID, it)

            is ChallengeStore.Error.ChallengePrivateGroupsInconsistent ->
                Error.PrivateGroupsChallengeInconsistent(it.challengeID, it)

            else ->
                Error.Unknown(it)
        }
    }

    private fun Challenge.Create.toChallenge(): Challenge {
        return Challenge(
            id = null,
            userID = userID.toUserID(),
            profileID = profileID.toProfileID(),
            privacy = privacy.type,
            createdDate = null,
            beginDate = beginDate,
            endDate = endDate,
            mediaType = mediaType,
            title = title.trim(),
            description = description?.trim(),
            allowImage = allowImage,
            allowVideo = allowVideo,
            allowText = allowText,
            deleted = null,
            firebaseID = generateFirebaseID()
        )
    }

    private fun PrivacyCreate.toPrivacy(): Privacy {
        return when (this) {
            is PrivacyCreate.Public ->
                Privacy.Public

            is PrivacyCreate.PrivateProfiles ->
                Privacy.PrivateProfiles(this.profileIDs.map { it.toProfileID() })

            is PrivacyCreate.PrivateGroups ->
                Privacy.PrivateGroups(this.groupID.toGroupID())
        }
    }
}
