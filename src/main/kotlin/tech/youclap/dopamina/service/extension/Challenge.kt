package tech.youclap.dopamina.service.extension

import tech.youclap.dopamina.model.ChallengeMedia
import tech.youclap.dopamina.model.ChallengeMediaType
import tech.youclap.dopamina.model.response.ChallengeResponse
import tech.youclap.dopamina.model.store.ChallengeStoreReadResult

fun ChallengeStoreReadResult.toResponse(teatroURL: String): ChallengeResponse {

    return ChallengeResponse(
        id = challenge.id!!,
        userID = challenge.userID,
        profileID = challenge.profileID,
        privacy = privacy,
        createdDate = challenge.createdDate!!,
        beginDate = challenge.beginDate,
        endDate = challenge.endDate,
        media = challenge.mediaType.toChallengeMedia(teatroURL, challenge.firebaseID),
        title = challenge.title,
        description = challenge.description,
        allowImage = challenge.allowImage,
        allowVideo = challenge.allowVideo,
        allowText = challenge.allowText,
        firebaseID = challenge.firebaseID
    )
}

private fun ChallengeMediaType.toChallengeMedia(teatroURL: String, firebaseID: String): ChallengeMedia {

    return when (this) {
        ChallengeMediaType.IMAGE ->
            ChallengeMedia.Image(
                imageURL = "$teatroURL/challenge/$firebaseID/image"
            )

        ChallengeMediaType.VIDEO ->
            ChallengeMedia.Video(
                thumbnailURL = "$teatroURL/challenge/$firebaseID/image",
                videoURL = "$teatroURL/challenge/$firebaseID/video"
            )
    }
}
