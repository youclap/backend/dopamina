package tech.youclap.dopamina.service.extension

import tech.youclap.dopamina.model.Post
import tech.youclap.dopamina.model.PostMedia
import tech.youclap.dopamina.model.extension.toHexColor
import tech.youclap.dopamina.model.response.PostResponse

fun Post.toResponse(teatroURL: String): PostResponse {
    return when (this) {
        is Post.Image -> PostResponse.Image(
            id = id!!,
            userID = userID,
            profileID = profileID,
            challengeID = challengeID,
            createdDate = createdDate!!,
            media = PostMedia.Image(
                imageURL = "$teatroURL/challenge/$challengeFirebaseID/post/$firebaseID/image"
            ),
            firebaseID = firebaseID
        )

        is Post.Video -> PostResponse.Video(
            id = id!!,
            userID = userID,
            profileID = profileID,
            challengeID = challengeID,
            createdDate = createdDate!!,
            media = PostMedia.Video(
                thumbnailURL = "$teatroURL/challenge/$challengeFirebaseID/post/$firebaseID/image",
                videoURL = "$teatroURL/challenge/$challengeFirebaseID/post/$firebaseID/video"
            ),
            firebaseID = firebaseID
        )

        is Post.Text -> PostResponse.Text(
            id = id!!,
            userID = userID,
            profileID = profileID,
            createdDate = createdDate!!,
            challengeID = challengeID,
            media = PostMedia.Text(
                startColor = startColor.toHexColor(),
                endColor = endColor.toHexColor(),
                message = message
            ),
            firebaseID = firebaseID
        )
    }
}
