@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.flatMap
import com.github.kittinunf.result.mapError
import org.joda.time.DateTime
import tech.youclap.dopamina.model.ChallengeMedia
import tech.youclap.dopamina.model.ChallengeMediaType
import tech.youclap.dopamina.model.PostMedia
import tech.youclap.dopamina.model.Privacy
import tech.youclap.dopamina.model.america.Action
import tech.youclap.dopamina.model.america.AmericaCollection
import tech.youclap.dopamina.model.america.AmericaMessage
import tech.youclap.dopamina.model.id.ChallengeID
import tech.youclap.dopamina.model.id.CommentID
import tech.youclap.dopamina.model.id.PostID
import tech.youclap.dopamina.model.id.ProfileID
import tech.youclap.dopamina.model.mexico.MexicoBlockedChallenge
import tech.youclap.dopamina.model.mexico.MexicoBlockedComment
import tech.youclap.dopamina.model.mexico.MexicoBlockedPost
import tech.youclap.dopamina.model.mexico.MexicoChallenge
import tech.youclap.dopamina.model.mexico.MexicoChallengeMediaType
import tech.youclap.dopamina.model.mexico.MexicoChallengeMediaTypeContainer
import tech.youclap.dopamina.model.mexico.MexicoChallengeParticipations
import tech.youclap.dopamina.model.mexico.MexicoChallengePrivacy
import tech.youclap.dopamina.model.mexico.MexicoClap
import tech.youclap.dopamina.model.mexico.MexicoComment
import tech.youclap.dopamina.model.mexico.MexicoPost
import tech.youclap.dopamina.model.mexico.MexicoPostMedia
import tech.youclap.dopamina.model.response.ChallengeResponse
import tech.youclap.dopamina.model.response.ClapResponse
import tech.youclap.dopamina.model.response.CommentResponse
import tech.youclap.dopamina.model.response.PostResponse
import tech.youclap.dopamina.store.AmericaStore
import tech.youclap.dopamina.store.PubSubStore

interface AmericaService {

    suspend fun createChallenge(challengeResponse: ChallengeResponse): Result<Unit, Error>
    suspend fun deleteChallenge(challengeID: ChallengeID): Result<Unit, Error>
    suspend fun createHiddenChallenge(profileID: ProfileID, challengeID: ChallengeID): Result<Unit, Error>
    suspend fun deleteHiddenChallenge(profileID: ProfileID, challengeID: ChallengeID): Result<Unit, Error>

    suspend fun createPost(postResponse: PostResponse, privacy: Privacy): Result<Unit, Error>
    suspend fun removePost(postID: PostID): Result<Unit, Error>
    suspend fun createHiddenPost(profileID: ProfileID, postID: PostID): Result<Unit, Error>
    suspend fun removeHiddenPost(profileID: ProfileID, postID: PostID): Result<Unit, Error>

    suspend fun createComment(commentResponse: CommentResponse): Result<Unit, Error>
    suspend fun removeComment(commentID: CommentID): Result<Unit, Error>
    suspend fun createHiddenComment(profileID: ProfileID, commentID: CommentID): Result<Unit, Error>
    suspend fun removeHiddenComment(profileID: ProfileID, commentID: CommentID): Result<Unit, Error>

    suspend fun createClap(clapResponse: ClapResponse): Result<Unit, Error>
    suspend fun removeClap(profileID: ProfileID, postID: PostID): Result<Unit, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}

class AmericaServiceImpl(
    private val pubSubStore: PubSubStore,
    private val store: AmericaStore
) : AmericaService {

    override suspend fun createChallenge(challengeResponse: ChallengeResponse): Result<Unit, AmericaService.Error> {

        val challenge = challengeResponse.toAmericaChallenge()

        val message = AmericaMessage(
            collection = AmericaCollection.Challenge,
            action = Action.CREATE,
            documentID = challenge.uid,
            data = challenge
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun deleteChallenge(challengeID: ChallengeID): Result<Unit, AmericaService.Error> {

        return store.readFirebaseID(challengeID)
            .mapError(errorStoreHandler)
            .flatMap {
                val message = AmericaMessage(
                    collection = AmericaCollection.Challenge,
                    action = Action.MARK_DELETED,
                    documentID = it
                )

                pubSubStore.sendMessage(message)
                    .mapError(errorPubSubHandler)
            }
    }

    override suspend fun createHiddenChallenge(
        profileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Unit, AmericaService.Error> {

        val profileFirebaseID = store.readFirebaseID(profileID).get()
        val challengeFirebaseID = store.readFirebaseID(challengeID).get()

        val data = MexicoBlockedChallenge(
            challengeID = challengeFirebaseID,
            userID = profileFirebaseID,
            timestamp = DateTime.now() // TODO is this correct?
        )

        val message = AmericaMessage(
            collection = AmericaCollection.BlockedChallenge,
            action = Action.CREATE,
            documentID = "${profileFirebaseID}_$challengeFirebaseID",
            data = data
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun deleteHiddenChallenge(
        profileID: ProfileID,
        challengeID: ChallengeID
    ): Result<Unit, AmericaService.Error> {

        val profileFirebaseID = store.readFirebaseID(profileID).get()
        val challengeFirebaseID = store.readFirebaseID(challengeID).get()

        val message = AmericaMessage(
            collection = AmericaCollection.BlockedChallenge,
            action = Action.DELETE,
            documentID = "${profileFirebaseID}_$challengeFirebaseID"
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun createPost(
        postResponse: PostResponse,
        privacy: Privacy
    ): Result<Unit, AmericaService.Error> {

        val post = postResponse.toAmericaPost(privacy)

        val message = AmericaMessage(
            collection = AmericaCollection.Post,
            action = Action.CREATE,
            documentID = post.uid,
            data = post
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun removePost(postID: PostID): Result<Unit, AmericaService.Error> {

        return store.readFirebaseID(postID)
            .mapError(errorStoreHandler)
            .flatMap {
                val message = AmericaMessage(
                    collection = AmericaCollection.Post,
                    action = Action.MARK_DELETED,
                    documentID = it
                )

                pubSubStore.sendMessage(message)
                    .mapError(errorPubSubHandler)
            }
    }

    override suspend fun createHiddenPost(
        profileID: ProfileID,
        postID: PostID
    ): Result<Unit, AmericaService.Error> {

        val profileFirebaseID = store.readFirebaseID(profileID).get()
        val postFirebaseID = store.readFirebaseID(postID).get()

        val data = MexicoBlockedPost(
            postID = postFirebaseID,
            userID = profileFirebaseID,
            timestamp = DateTime.now() // TODO is this correct?
        )

        val message = AmericaMessage(
            collection = AmericaCollection.BlockedPost,
            action = Action.CREATE,
            documentID = "${profileFirebaseID}_$postFirebaseID",
            data = data
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun removeHiddenPost(
        profileID: ProfileID,
        postID: PostID
    ): Result<Unit, AmericaService.Error> {

        val profileFirebaseID = store.readFirebaseID(profileID).get()
        val postFirebaseID = store.readFirebaseID(postID).get()

        val message = AmericaMessage(
            collection = AmericaCollection.BlockedPost,
            action = Action.DELETE,
            documentID = "${profileFirebaseID}_$postFirebaseID"
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun createComment(commentResponse: CommentResponse): Result<Unit, AmericaService.Error> {

        val comment = commentResponse.toAmericaComment()

        val message = AmericaMessage(
            collection = AmericaCollection.Comment,
            action = Action.CREATE,
            documentID = comment.uid,
            data = comment
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun removeComment(commentID: CommentID): Result<Unit, AmericaService.Error> {

        return store.readFirebaseID(commentID)
            .mapError(errorStoreHandler)
            .flatMap {
                val message = AmericaMessage(
                    collection = AmericaCollection.Comment,
                    action = Action.MARK_DELETED,
                    documentID = it
                )

                pubSubStore.sendMessage(message)
                    .mapError(errorPubSubHandler)
            }
    }

    override suspend fun createHiddenComment(
        profileID: ProfileID,
        commentID: CommentID
    ): Result<Unit, AmericaService.Error> {

        val profileFirebaseID = store.readFirebaseID(profileID).get()
        val commentFirebaseID = store.readFirebaseID(commentID).get()
        val postFirebaseID = store.readPostID(commentID).flatMap { store.readFirebaseID(it) }.get()

        val data = MexicoBlockedComment(
            commentID = commentFirebaseID,
            userID = profileFirebaseID,
            postID = postFirebaseID,
            timestamp = DateTime.now() // TODO is this correct?
        )

        val message = AmericaMessage(
            collection = AmericaCollection.BlockedComment,
            action = Action.CREATE,
            documentID = "${profileFirebaseID}_$commentFirebaseID",
            data = data
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun removeHiddenComment(
        profileID: ProfileID,
        commentID: CommentID
    ): Result<Unit, AmericaService.Error> {

        val profileFirebaseID = store.readFirebaseID(profileID).get()
        val commentFirebaseID = store.readFirebaseID(commentID).get()

        val message = AmericaMessage(
            collection = AmericaCollection.BlockedComment,
            action = Action.DELETE,
            documentID = "${profileFirebaseID}_$commentFirebaseID"
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun createClap(clapResponse: ClapResponse): Result<Unit, AmericaService.Error> {

        val postFirebaseID = store.readFirebaseID(clapResponse.postID).get()
        val profileFirebaseID = store.readFirebaseID(clapResponse.profileID).get()
        val challengeFirebaseID = store.readChallengeID(clapResponse.postID).flatMap { store.readFirebaseID(it) }.get()

        val data = MexicoClap(
            challengeID = challengeFirebaseID,
            postID = postFirebaseID,
            timestamp = DateTime.now(), // TODO is this correct?
            type = clapResponse.type,
            userID = profileFirebaseID
        )

        val message = AmericaMessage(
            collection = AmericaCollection.Clap,
            action = Action.CREATE,
            documentID = "${postFirebaseID}_$profileFirebaseID",
            data = data
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    override suspend fun removeClap(profileID: ProfileID, postID: PostID): Result<Unit, AmericaService.Error> {

        val postFirebaseID = store.readFirebaseID(postID).get()
        val profileFirebaseID = store.readFirebaseID(profileID).get()

        val message = AmericaMessage(
            collection = AmericaCollection.Clap,
            action = Action.DELETE,
            documentID = "${postFirebaseID}_$profileFirebaseID"
        )

        return pubSubStore.sendMessage(message)
            .mapError(errorPubSubHandler)
    }

    private val errorPubSubHandler: (PubSubStore.Error) -> AmericaService.Error = {
        when (it) {
            is PubSubStore.Error.Unknown ->
                AmericaService.Error.Unknown(it)
        }
    }

    private val errorStoreHandler: (AmericaStore.Error) -> AmericaService.Error = {
        AmericaService.Error.Unknown(it)
    }

    @SuppressWarnings("MagicNumber")
    private suspend fun ChallengeResponse.toAmericaChallenge(): MexicoChallenge {

        return MexicoChallenge(
            deleted = false,
            endDate = this.endDate ?: DateTime(3000, 1, 1, 0, 0), // TODO we must have endtime in firebase
            media = this.media.toMexicoChallengeMediaTypeContainer(),
            participations = this.toMexicoChallengeParticipations(),
            privacy = this.privacy.toMexicoChallengePrivacy(),
            startDate = this.beginDate,
            timestamp = this.createdDate,
            title = this.title,
            description = this.description,
            uid = this.firebaseID,
            userID = store.readFirebaseID(this.profileID).get()
        )
    }

    private fun ChallengeMedia.toMexicoChallengeMediaTypeContainer(): MexicoChallengeMediaTypeContainer {

        val type = when (this.type) {

            ChallengeMediaType.IMAGE -> MexicoChallengeMediaType.PHOTO

            ChallengeMediaType.VIDEO -> MexicoChallengeMediaType.VIDEO
        }

        return MexicoChallengeMediaTypeContainer(type)
    }

    private fun ChallengeResponse.toMexicoChallengeParticipations(): List<MexicoChallengeParticipations> {

        val list = mutableListOf<MexicoChallengeParticipations>()

        if (allowVideo) {
            list += MexicoChallengeParticipations.VIDEO
        }

        if (allowImage) {
            list += MexicoChallengeParticipations.PHOTO
        }

        if (allowText) {
            list += MexicoChallengeParticipations.TEXT
        }

        return list
    }

    private suspend fun Privacy.toMexicoChallengePrivacy(): MexicoChallengePrivacy {

        return when (this) {

            Privacy.Public -> MexicoChallengePrivacy.Public

            is Privacy.PrivateProfiles ->
                profileIDs.map {
                    store.readFirebaseID(it).get()
                }
                    .let {
                        MexicoChallengePrivacy.PrivateProfiles(it)
                    }

            is Privacy.PrivateGroups ->
                store.readFirebaseID(groupID).get()
                    .let {
                        MexicoChallengePrivacy.PrivateGroups(it)
                    }
        }
    }

    private suspend fun PostResponse.toAmericaPost(privacy: Privacy): MexicoPost {

        return MexicoPost(
            challengeID = store.readFirebaseID(this.challengeID).get(),
            deleted = false,
            media = this.media.toMexicoPostMedia(),
            timestamp = this.createdDate,
            uid = this.firebaseID,
            userID = store.readFirebaseID(this.profileID).get(),
            privacy = privacy.toMexicoChallengePrivacy()
        )
    }

    private fun PostMedia.toMexicoPostMedia(): MexicoPostMedia {
        return when (this) {

            is PostMedia.Image -> MexicoPostMedia.Photo

            is PostMedia.Video -> MexicoPostMedia.Video

            is PostMedia.Text -> MexicoPostMedia.Text(listOf(this.startColor, this.endColor), this.message)
        }
    }

    private suspend fun CommentResponse.toAmericaComment(): MexicoComment {

        return MexicoComment(
            deleted = false,
            postID = store.readFirebaseID(this.postID).get(),
            timestamp = this.createdDate,
            uid = this.firebaseID,
            userID = store.readFirebaseID(this.profileID).get(),
            value = this.message
        )
    }
}
