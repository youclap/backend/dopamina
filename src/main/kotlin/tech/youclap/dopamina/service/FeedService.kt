package tech.youclap.dopamina.service

import com.github.kittinunf.result.Result
import tech.youclap.dopamina.model.FeedItem
import tech.youclap.dopamina.model.response.FeedItemResponse

interface FeedService {

    suspend fun readFeed(feed: List<FeedItem>): Result<List<FeedItemResponse>, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
